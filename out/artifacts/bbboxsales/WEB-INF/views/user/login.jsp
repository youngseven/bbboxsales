<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>


<!DOCTYPE html>

<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>登录</title>
    <link rel="stylesheet" href="plugins/layui/css/layui.css" media="all" />
    <link rel="stylesheet" href="css/login.css" />
    <script src="assets/js/jquery.min.js"></script>
</head>

<body class="beg-login-bg">
<div class="beg-login-box">
    <header>
        <h1>代理后台登录</h1>
    </header>
    <div class="beg-login-main">
        <form id="formid" action="userLogin.html"  class="layui-form" method="post">
            <input name="__RequestVerificationToken" type="hidden" value="fkfh8D89BFqTdrE2iiSdG_L781RSRtdWOH411poVUWhxzA5MzI8es07g6KPYQh9Log-xf84pIR2RIAEkOokZL3Ee3UKmX0Jc8bW8jOdhqo81" />
            <div class="layui-form-item">
                <label class="beg-login-icon">
                    <i class="layui-icon">&#xe612;</i>
                </label>
                <input type="text"  id="cloginname" name="cloginname" value="" lay-verify="userName" autocomplete="off" placeholder="这里输入登录名" class="layui-input">
            </div>
            <div class="layui-form-item">
                <label class="beg-login-icon">
                    <i class="layui-icon">&#xe642;</i>
                </label>
                <input type="password" id="cpassword" name="cpassword" value="" lay-verify="password" autocomplete="off" placeholder="这里输入密码" class="layui-input">
            </div>
            <div class="layui-form-item">
                <input id="authCode" name="authCode" type="text" class="layui-input" maxlength="6" style="width: 50%; display: initial" lay-verify="codeImage" autocomplete="off" placeholder="这里输入验证码" />
                <!--这里img标签的src属性的值为后台实现图片验证码方法的请求地址-->
                <img type="image" src="authCode.html" id="codeImage" onclick="chageCode()" title="图片看不清？点击重新得到验证码" style="cursor:pointer;"/>
                <label><a onclick="chageCode()">换一张</a></label>
            </div>
            <div class="layui-form-item">
                <div class="beg-pull-left beg-login-remember">
                    <label>记住帐号？</label>
                    <input type="checkbox" name="rememberMe" value="true" lay-skin="switch" checked title="记住帐号">
                </div>
                <div class="beg-pull-right">
                    <button class="layui-btn layui-btn-primary" lay-submit lay-filter="login">
                        <i class="layui-icon">&#xe650;</i> 登录
                    </button>
                </div>
                <div class="beg-clear"></div>
            </div>
        </form>
    </div>
    <footer>
        <p>foreverPay</p>
    </footer>
</div>
<script type="text/javascript" src="plugins/layui/layui.js"></script>
<script>
    layui.use(['layer', 'form'], function() {
        var layer = layui.layer,
            $ = layui.jquery,
            form = layui.form();

        form.on('submit(login)',function(data){
            if(document.getElementById("cloginname").value==""){
                alert("请输入登录名！");
                return false;
            }
            if(document.getElementById("cpassword").value==""){
                alert("请输入密码！");
                return false;
            }
            if(document.getElementById("codeImage").value==""){
                alert("请输入验证码！");
                return false;
            }
        });
    });

    function chageCode(){
        $('#codeImage').attr('src','authCode.html?abc='+Math.random());//链接后添加Math.random，确保每次产生新的验证码，避免缓存问题。
    }
</script>
</body>

</html>