<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>fbPay管理后台</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <!-- App favicon -->
		<link rel="shortcut icon" href="assets/images/favicon.ico">
		
		<!-- DataTables -->
		<link href="plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
		<link href="plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
		<!-- Responsive datatable examples -->
		<link href="plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
		<!-- Plugins css -->
		<link href="plugins/bootstrap-timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
		<link href="plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css" rel="stylesheet">
		<link href="plugins/clockpicker/css/bootstrap-clockpicker.min.css" rel="stylesheet">
		<link href="plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
		<!-- App css -->
		<link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		<link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
		<link href="assets/css/metismenu.min.css" rel="stylesheet" type="text/css" />
		<link href="assets/css/style.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" type="text/css" href="css/pikaday.css" />
		<script type="text/javascript" src="js/pikaday.min.js"></script>
		<script src="assets/js/modernizr.min.js"></script>
        <script type="text/javascript">
			function checkinput() {
				if(document.getElementById("begintime").value==""||document.getElementById("endtime").value==""){
					alert("请选择时间！");
					return false;
				}else{
					if(document.getElementById("begintime").value==""&&document.getElementById("endtime").value!=""){
					alert("请选择开始时间！");
					return false;
					}
					if(document.getElementById("endtime").value==""&&document.getElementById("begintime").value!="") {
						alert("请选择结束时间！");
						return false;
					}
					if (document.getElementById("begintime").value!=""&&document.getElementById("endtime").value!="") {
						if(DateMinus(document.getElementById("begintime").value,document.getElementById("endtime").value)<0){
							alert("结束时间必须大于开始时间！");
							return false;
						} 
						if(DateMinus(document.getElementById("begintime").value,document.getElementById("endtime").value)>=8){
							alert("查询周期不能大于7天！");
							return false;
						}else{
							var form = document.getElementById('formid');
							//再次修改input内容
							form.submit();
						}	
					}
				}
			}
			function DateMinus(btime,etime){ 
			　　var begintime = new Date(btime.replace(/-/g, "/")); 
			　　var endtime = new Date(etime.replace(/-/g, "/")); 
			　　var days = endtime.getTime() - begintime.getTime(); 
			　　var day = parseInt(days / (1000 * 60 * 60 * 24)); 
			　　return day; 
			}
			
			</script>
    </head>


    <body>
		<div class="row">
			<div class="col-lg-12">
				<div class="card-box table-responsive">
					<h4 class="header-title m-t-0">查询订单明细</h4>

					<form id="formid"  action="qureyreport.html" method="post">
						<div class="form-group row">
							<label class="col-1 col-form-label">查询周期</label>
							<div class="col-2">
								 <input type="text" class="form-control" id="begintime" name="begintime" value="" placeholder="请选择开始时间" autocomplete="off">
							</div>
							<label class="col-1 col-form-label">-----</label>
							<div class="col-2">
								 <input type="text" class="form-control" id="endtime" name="endtime" value="" placeholder="请选择结束时间" autocomplete="off">
							</div>
							<label class="col-1 col-form-label"></label>
							<div class="col-2">

								<input type="button" class="redBt" value="查询" onclick="checkinput();"/>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>

			<div class="row">
				<div class="col-12">
					<div class="card-box table-responsive">
						<h4 class="m-t-0 header-title">
							<b>余额明细</b>
						</h4>
						<table id="datatable-buttons"
							class="table table-striped table-bordered" cellspacing="0"
							width="100%">
							<c:if test="${!empty reportlbeanList}">
								<thead>
									<tr>
										<th>日期</th>
										<th>总充值笔数</th>
										<th>今日充值总金额</th>
										<th>今日收入</th>
										<th>成功总笔数</th>
										<th>自动成功笔数</th>
										<th>加急确认笔数</th>
										<th>用户未支付笔数</th>
										<th>充值平均金额</th>
										<th>自动成功金额</th>
										<th>加急确认金额</th>

									</tr>
								</thead>
								<tbody>
								<c:forEach items="${reportlbeanList}" var="s">
									<tr>
										<td>${s.datetime}</td>
										<td>${s.sum}</td>
										<td>${s.receivableAmount/100}</td>
										<td>${s.handlingfee/100}</td>
										<td>${s.success}</td>
										<td>${s.oksuccess}</td>
										<td>${s.rsuccess}</td>
										<td>${s.fail}</td>
										<c:if test="${s.success>0}">
											<td><fmt:formatNumber type="number" value="${s.receivableAmount/s.success/100}" pattern="0.00" maxFractionDigits="2"/></td>
										</c:if>
										<c:if test="${s.success==0}">
											<td>0</td>
										</c:if>
										<td>${s.okmoney/100}</td>
										<td>${s.rmoney/100}</td>

									</tr>
								</c:forEach>
								</tbody>
							</c:if>
							<c:if test="${empty reportlbeanList}">暂时没有订单</c:if>
						</table>
					</div>
				</div>
			</div>
      	<!-- jQuery  -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/popper.min.js"></script>
			<script src="assets/js/bootstrap.min.js"></script>
			<script src="assets/js/metisMenu.min.js"></script>
			<script src="assets/js/waves.js"></script>
			<script src="assets/js/jquery.slimscroll.js"></script>
			<script src="plugins/waypoints/lib/jquery.waypoints.min.js"></script>
			<script src="plugins/counterup/jquery.counterup.min.js"></script>
			<!-- plugin js -->
			<script src="plugins/moment/moment.js"></script>
			<script src="plugins/bootstrap-timepicker/bootstrap-timepicker.js"></script>
			<script
				src="plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
			<script src="plugins/clockpicker/js/bootstrap-clockpicker.min.js"></script>
			<script src="plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
		
			<!-- Required datatable js -->
			<script src="plugins/datatables/jquery.dataTables.min.js"></script>
			<script src="plugins/datatables/dataTables.bootstrap4.min.js"></script>
			<!-- Buttons examples -->
			<script src="plugins/datatables/dataTables.buttons.min.js"></script>
			<script src="plugins/datatables/buttons.bootstrap4.min.js"></script>
			<script src="plugins/datatables/jszip.min.js"></script>
			<script src="plugins/datatables/pdfmake.min.js"></script>
			<script src="plugins/datatables/vfs_fonts.js"></script>
			<script src="plugins/datatables/buttons.html5.min.js"></script>
			<script src="plugins/datatables/buttons.print.min.js"></script>
			<!-- Responsive examples -->
			<script src="plugins/datatables/dataTables.responsive.min.js"></script>
			<script src="plugins/datatables/responsive.bootstrap4.min.js"></script>
		
			<!-- App js -->
			<script src="assets/js/jquery.core.js"></script>
			<script src="assets/js/jquery.app.js"></script>
			<!-- Init js -->
			<script src="assets/pages/jquery.form-pickers.init.js"></script>
			<script type="text/javascript">
				$(document).ready(function() {
					//$('#datatable').DataTable();
					//$('#datatable-buttons').DataTable();
				
					//Buttons examples
					var table = $('#datatable-buttons').DataTable({
						lengthChange : false,
						buttons : [ 'copy', 'excel', 'pdf' ]
					});
					table.buttons().container()
						.appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');
				});
				var begintime = new Pikaday(
					{
						field : document.getElementById('begintime'),
						firstDay : 1,
						minDate : new Date('2018-01-01'),
						maxDate : new Date(),
						yearRange : [ 2018, 2025 ]
					});
				var endtime = new Pikaday(
					{
						field : document.getElementById('endtime'),
						firstDay : 1,
						minDate : new Date('2018-01-01'),
						maxDate : new Date(),
						yearRange : [ 2018, 2025 ]
					});
			</script>
    </body>
</html>