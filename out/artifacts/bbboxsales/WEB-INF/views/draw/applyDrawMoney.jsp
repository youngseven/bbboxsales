<%@ page language="java" import="java.util.*"  pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>管理后台</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <!-- App favicon -->
        <link rel="shortcut icon" href="assets/images/favicon.ico">

        <!-- Plugins css-->
        <link href="plugins/bootstrap-tagsinput/css/bootstrap-tagsinput.css" rel="stylesheet" />
        <link href="plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" />
        <link href="plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="plugins/bootstrap-touchspin/css/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="plugins/switchery/switchery.min.css">

        <!-- App css -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/metismenu.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/style.css" rel="stylesheet" type="text/css" />

        <script src="assets/js/modernizr.min.js"></script>
        <script type="text/javascript">
			function checkinput() {
                if (document.getElementById("applymoney2").value=="") {
                    alert("请输入收款金额金额！");
                    return false;
                }else if (document.getElementById("applymoney2").value*100>document.getElementById("avbalance2").value*100) {
					alert("提款金额不能大于可用金额！");
					return false;
				} else if(document.getElementById("turninbank").value==""||document.getElementById("turnincardnumber").value==""||document.getElementById("turninname").value==""){
                    alert("请输入收款信息！");
                    return false;
                }else {
					document.getElementById("check").style.display="none";
					document.getElementById("reset").style.display="";
					document.getElementById("confirm").style.display="";
					document.getElementById("applymoney2").readOnly = true;
					document.getElementById("turninbank").readOnly = true;
					document.getElementById("turnincardnumber").readOnly = true;
					document.getElementById("turninname").readOnly = true;
					document.getElementById("turnoutbank").readOnly = true;
					document.getElementById("turnoutbcardnumber").readOnly = true;
					document.getElementById("turnoutname").readOnly = true;
					
					//document.getElementById("servicecharge2").value=Math.round(document.getElementById("applymoney2").value*100)/100
					// document.getElementById("actualmoney2").value=Math.round((document.getElementById("applymoney2").value-document.getElementById("servicecharge2").value)*100)/100;
					document.getElementById("applymoney").value=document.getElementById("applymoney2").value*100;
					document.getElementById("actualmoney").value=document.getElementById("actualmoney2").value*100;
					//document.getElementById("servicecharge").value=document.getElementById("servicecharge2").value*100;
					alert("确认提交吗？请在检查一次！！");
				}
			}
			function resetform(){
				document.getElementById("check").style.display="";
				document.getElementById("reset").style.display="none";
				document.getElementById("confirm").style.display="none";
				document.getElementById("applymoney2").value="";
				// document.getElementById("servicecharge2").value="0";
				document.getElementById("actualmoney2").value="";
				document.getElementById("applymoney2").readOnly = false;
				document.getElementById("turninbank").readOnly = false;
				document.getElementById("turnincardnumber").readOnly = false;
				document.getElementById("turninname").readOnly = false;
				document.getElementById("turnoutbank").readOnly = false;
				document.getElementById("turnoutbcardnumber").readOnly = false;
				document.getElementById("turnoutname").readOnly = false;
				
				document.getElementById("avbalance").value="${merchant.avbalance}";
				document.getElementById("applymoney").value="";
				document.getElementById("actualmoney").value="";
				//document.getElementById("servicecharge").value="";
			}
			function submitform(){

				var form = document.getElementById('formid');
				//再次修改input内容
				form.submit();
			}
		 </script>
    </head>


    <body>
         <div class="row">
            <div class="col-12">
                <div class="card-box">
                    <h4 class="m-t-0 header-title">代理余额信息</h4>
                    <div class="form-row">
                        <label class="col-form-label col-md-1.5">代&nbsp;&nbsp;理&nbsp;&nbsp;编&nbsp;&nbsp;号：</label>
                        <div class="form-group col-md-2">
                            <input type="text" class="form-control"  id="salesno2" name="salesno2" value="${sales.salesno}" readonly>
                        </div>
                        <label class="col-form-label col-md-1.5">&nbsp;代&nbsp;&nbsp;理&nbsp;&nbsp;名&nbsp;&nbsp;称：</label>
                         <div class="form-group col-md-2">
                            <input type="text" class="form-control"  id="salesname2" name="salesname2" value="${sales.salesname}" readonly/>
                        </div>
                        <label class="col-form-label col-md-1.5">当前总余额(元)：</label>
                        <div class="form-group col-md-2">
                            <input type="text" class="form-control"  id="balance" name="balance" value="${sales.balance/100 }" readonly/>
                        </div>
                    </div>
                    <div class="form-row">
                       <label class="col-form-label col-md-1.5">可用余额(元)：</label>
                       <div class="form-group col-md-2">
                            <input type="text" class="form-control"  id="avbalance2" name="avbalance2" value="${sales.avbalance/100 }" readonly>
                        </div>
                        <label class="col-form-label col-md-1.5">锁定余额(元)：</label>
                        <div class="form-group col-md-2">
                            <input type="text" class="form-control"  id="lockbalance" name="lockbalance" value="${sales.lockbalance/100 }" readonly>
                        </div>
                        <label class="col-form-label col-md-1.5">不可用余额(元)：</label>
                        <div class="form-group col-md-2">
                            <input type="text" class="form-control"  id="unbalance" name="unbalance" value="${sales.unbalance/100 }" readonly>
                        </div>
                    </div>
                    <div class="form-row">
                        <label class="col-form-label col-md-1.5">结款金额(元)：</label>
                        <div class="form-group col-md-2">
                            <input type="text" class="form-control"  id="applymoney2" name="applymoney2" value="">
                        </div>
                        <label class="col-form-label col-md-1.5">到账余额(元)：</label>
                        <div class="form-group col-md-2">
                            <input type="text" class="form-control"  id="actualmoney2" name="actualmoney2" value="" readonly>
                        </div>
                    </div>
                </div>
            </div>
        </div>

					<!-- end row -->
        <div class="row">
            <div class="col-12">
                <div class="card-box">
                    <h4 class="m-t-0 header-title"><b>商户结算申请表</b></h4>
                        <form id="formid" action="applyDrawMoney.html" method="post">
                           <div class="form-group row">
                               <label class="col-2 col-form-label">收款银行</label>
                               <div class="col-10">
                                   <input type="text" class="form-control" id="turninbank" name="turninbank" value="" placeholder="请输入收款银行" autocomplete="off">
                                <input type="hidden" id="salesno" name="salesno" value="${sales.salesno}">
                                <input type="hidden" id="salesname" name="salesname" value="${sales.salesname}">
                                <input type="hidden" id="settlementno" name="settlementno" value="${settlementno}">
                                <input type="hidden" id="avbalance" name="avbalance" value="${sales.avbalance}">
                                <input type="hidden" id="applymoney" name="applymoney" value="">
                                <input type="hidden" id="actualmoney" name="actualmoney" value="">

                               </div>
                           </div>
                           <div class="form-group row">
                               <label class="col-2 col-form-label" >收款卡号</label>
                               <div class="col-10">
                                   <input type="text" class="form-control" id="turnincardnumber" name="turnincardnumber" value="" placeholder="请输入收款卡号" autocomplete="off">
                               </div>
                           </div>
                           <div class="form-group row">
                               <label class="col-2 col-form-label" >收款人</label>
                               <div class="col-10">
                                   <input type="text" class="form-control" id="turninname" name="turninname" value="" placeholder="请输入收款人姓名" autocomplete="off">
                               </div>
                           </div>
                           <h4 class="m-t-0 header-title"><b>付款信息</b></h4>
                           <div class="form-group row">
                               <label class="col-2 col-form-label">付款银行</label>
                               <div class="col-10">
                                   <input type="text" class="form-control" id="turnoutbank" name="turnoutbank" value="" placeholder="请输入付款银行" autocomplete="off" readonly>
                               </div>
                           </div>
                           <div class="form-group row">
                               <label class="col-2 col-form-label" >付款卡号</label>
                               <div class="col-10">
                                   <input type="text" class="form-control" id="turnoutbcardnumber" name="turnoutbcardnumber" value="" placeholder="请输入付款卡号" autocomplete="off" readonly>
                               </div>
                           </div>
                           <div class="form-group row">
                               <label class="col-2 col-form-label" >付款人</label>
                               <div class="col-10">
                                   <input type="text" class="form-control" id="turnoutname" name="turnoutname" value="" placeholder="请输入付款人姓名" autocomplete="off" readonly>
                               </div>
                           </div>
                           <div class="form-group row">
                            <div class="col-10">
                                <input id="check" type="button"  value="确定" onclick="checkinput();"/>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <input id="reset" style="display: none" type="button"  value="取消" onclick="resetform();"/>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <input id="confirm" style="display: none" type="button"  value="提交" onclick="submitform();"/>

                            </div>
                           </div>
                         </form>
                       </div>
                   </div>
               </div>
            </div>
		</div>


        <!-- jQuery  -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/popper.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/metisMenu.min.js"></script>
        <script src="assets/js/waves.js"></script>
        <script src="assets/js/jquery.slimscroll.js"></script>

        <script src="plugins/switchery/switchery.min.js"></script>
        <script src="plugins/bootstrap-tagsinput/js/bootstrap-tagsinput.min.js"></script>
        <script src="plugins/select2/js/select2.min.js" type="text/javascript"></script>
        <script src="plugins/bootstrap-select/js/bootstrap-select.js" type="text/javascript"></script>
        <script src="plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js" type="text/javascript"></script>
        <script src="plugins/bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js" type="text/javascript"></script>
        <script src="plugins/bootstrap-maxlength/bootstrap-maxlength.js" type="text/javascript"></script>

        <script type="text/javascript" src="plugins/autocomplete/jquery.mockjax.js"></script>
        <script type="text/javascript" src="plugins/autocomplete/jquery.autocomplete.min.js"></script>
        <script type="text/javascript" src="plugins/autocomplete/countries.js"></script>
        <script type="text/javascript" src="assets/pages/jquery.autocomplete.init.js"></script>

        <!-- Init Js file -->
        <script type="text/javascript" src="assets/pages/jquery.form-advanced.init.js"></script>

        <!-- App js -->
        <script src="assets/js/jquery.core.js"></script>
        <script src="assets/js/jquery.app.js"></script>

    </body>
</html>