<%@ page language="java" import="java.util.*"  pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>管理后台</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
    <meta content="Coderthemes" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <!-- App favicon -->
    <link rel="shortcut icon" href="assets/images/favicon.ico">

    <!-- Plugins css-->
    <link href="plugins/bootstrap-tagsinput/css/bootstrap-tagsinput.css" rel="stylesheet" />
    <link href="plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" />
    <link href="plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
    <link href="plugins/bootstrap-touchspin/css/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="plugins/switchery/switchery.min.css">

    <!-- App css -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/metismenu.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/style.css" rel="stylesheet" type="text/css" />

    <script src="assets/js/modernizr.min.js"></script>

</head>


<body>
<div class="row">
    <div class="col-12">
        <div class="card-box">
            <h4 class="page-title float-left"><b>代理结算审批</b></h4>
            <div class="form-group row">
                <label class="col-1 col-form-label">申请单号</label>
                <div class="col-2">
                    <input type="text" class="form-control" id="drawmoneyno" name="drawmoneyno" value="${drawMoney.drawmoneyno }" readonly>
                </div>
                <label class="col-1 col-form-label">申请人</label>
                <div class="col-2">
                    <input type="text" class="form-control" id="applynname" name="applynname" value="${drawMoney.applynname }" readonly>
                </div>
                <label class="col-1 col-form-label">申请时间</label>
                <div class="col-2">
                    <input type="text" class="form-control" id="applytime" name="applytime" value="${drawMoney.applytime }" readonly>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-1 col-form-label">当前状态</label>
                <div class="col-2">
                    <c:if test="${drawMoney.state==0}">
                        <input type="text" class="form-control" id="state" name="state" value="发起申请" readonly>
                    </c:if>
                    <c:if test="${drawMoney.state==1}">
                        <input type="text" class="form-control" id="state" name="state" value="审核通过" readonly>
                    </c:if>
                    <c:if test="${drawMoney.state==2}">
                        <input type="text" class="form-control" id="state" name="state" value="审核不通过" readonly>
                    </c:if>
                    <c:if test="${drawMoney.state==3}">
                        <input type="text" class="form-control" id="state" name="state" value="结款完成" readonly>
                    </c:if>
                    <c:if test="${drawMoney.state==4}">
                        <input type="text" class="form-control" id="state" name="state" value="结款失败" readonly>
                    </c:if>
                </div>
                <label class="col-1 col-form-label">审批人</label>
                <div class="col-2">
                    <input type="text" class="form-control" id="examinename" name="examinename" value="${drawMoney.examinename }" readonly>
                </div>
                <label class="col-1 col-form-label">审批时间</label>
                <div class="col-2">
                    <input type="text" class="form-control" id="examinetime" name="examinetime" value="${drawMoney.examinetime }" readonly>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-1 col-form-label">实际转款</label>
                <div class="col-2">
                    <input type="text" class="form-control" Style="color: red;" id="actualamount2" name="actualamount2" value="${drawMoney.actualamount/100 }" readonly>
                </div>
                <label class="col-1 col-form-label">确认人</label>
                <div class="col-2">
                    <input type="text" class="form-control" id="endname" name="endname" value="${drawMoney.endname }" readonly>
                </div>
                <label class="col-1 col-form-label">确认时间</label>
                <div class="col-2">
                    <input type="text" class="form-control" id="endtime" name="endtime" value="${drawMoney.endtime }" readonly>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-1 col-form-label">代理号</label>
                <div class="col-2">
                    <input type="text" class="form-control" id="salesno" name="salesno" value="${drawMoney.salesno }" readonly>
                </div>
                <label class="col-1 col-form-label">代理名</label>
                <div class="col-2">
                    <input type="text" class="form-control" id="salesname" name="salesname" value="${drawMoney.salesname }" readonly>
                </div>
                <label class="col-1 col-form-label">费率</label>
                <div class="col-2">
                    <input type="text" class="form-control" id="rate" name="rate" value="${drawMoney.rate*100 }" readonly>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-1 col-form-label">申请金额</label>
                <div class="col-2">
                    <input type="text" class="form-control" id="applymoney2" name="applymoney2" value="${drawMoney.applymoney/100 }" readonly>
                </div>
                <label class="col-1 col-form-label">转款金额</label>
                <div class="col-2">
                    <input type="text" class="form-control" Style="color: red;" id="actualmoney" name="actualmoney" value="${drawMoney.actualmoney/100 }" readonly>
                </div>
                <label class="col-1 col-form-label">手续费</label>
                <div class="col-2">
                    <input type="text" class="form-control" id="servicecharge" name="servicecharge" value="${drawMoney.servicecharge/100 }" readonly>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-1 col-form-label">收款银行</label>
                <div class="col-2">
                    <input type="text" class="form-control" id="turninbank" name="turninbank" value="${drawMoney.turninbank }" readonly>
                </div>
                <label class="col-1 col-form-label">收款卡号</label>
                <div class="col-2">
                    <input type="text" class="form-control" id="turnincardnumber" name="turnincardnumber" value="${drawMoney.turnincardnumber }" readonly>
                </div>
                <label class="col-1 col-form-label">收款人</label>
                <div class="col-2">
                    <input type="text" class="form-control" id="turninname" name="turninname" value="${drawMoney.turninname }" readonly>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-1 col-form-label">付款银行</label>
                <div class="col-2">
                    <input type="text" class="form-control" id="turnoutbank" name="turnoutbank" value="${drawMoney.turnoutbank }" readonly>
                </div>
                <label class="col-1 col-form-label">付款卡号</label>
                <div class="col-2">
                    <input type="text" class="form-control" id="turnoutbcardnumber" name="turnoutbcardnumber" value="${drawMoney.turnoutbcardnumber }" readonly>
                </div>
                <label class="col-1 col-form-label">付款人</label>
                <div class="col-2">
                    <input type="text" class="form-control" id="turnoutname" name="turnoutname" value="${drawMoney.turnoutname }" readonly>
                </div>
            </div>
        </div>
    </div>
</div>




<!-- jQuery  -->
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/popper.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/metisMenu.min.js"></script>
<script src="assets/js/waves.js"></script>
<script src="assets/js/jquery.slimscroll.js"></script>

<script src="plugins/switchery/switchery.min.js"></script>
<script src="plugins/bootstrap-tagsinput/js/bootstrap-tagsinput.min.js"></script>
<script src="plugins/select2/js/select2.min.js" type="text/javascript"></script>
<script src="plugins/bootstrap-select/js/bootstrap-select.js" type="text/javascript"></script>
<script src="plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js" type="text/javascript"></script>
<script src="plugins/bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js" type="text/javascript"></script>
<script src="plugins/bootstrap-maxlength/bootstrap-maxlength.js" type="text/javascript"></script>

<script type="text/javascript" src="plugins/autocomplete/jquery.mockjax.js"></script>
<script type="text/javascript" src="plugins/autocomplete/jquery.autocomplete.min.js"></script>
<script type="text/javascript" src="plugins/autocomplete/countries.js"></script>
<script type="text/javascript" src="assets/pages/jquery.autocomplete.init.js"></script>

<!-- Init Js file -->
<script type="text/javascript" src="assets/pages/jquery.form-advanced.init.js"></script>

<!-- App js -->
<script src="assets/js/jquery.core.js"></script>
<script src="assets/js/jquery.app.js"></script>

</body>
</html>