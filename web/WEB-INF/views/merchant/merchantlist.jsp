<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>fbPay管理后台</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <!-- App favicon -->
		<link rel="shortcut icon" href="assets/images/favicon.ico">
		
		<!-- DataTables -->
		<link href="plugins/datatables/dataTables.bootstrap4.min.css"
			rel="stylesheet" type="text/css" />
		<link href="plugins/datatables/buttons.bootstrap4.min.css"
			rel="stylesheet" type="text/css" />
		<!-- Responsive datatable examples -->
		<link href="plugins/datatables/responsive.bootstrap4.min.css"
			rel="stylesheet" type="text/css" />
		<!-- Plugins css -->
		<link href="plugins/bootstrap-timepicker/bootstrap-timepicker.min.css"
			rel="stylesheet">
		<link
			href="plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css"
			rel="stylesheet">
		<link href="plugins/clockpicker/css/bootstrap-clockpicker.min.css"
			rel="stylesheet">
		<link href="plugins/bootstrap-daterangepicker/daterangepicker.css"
			rel="stylesheet">
		<!-- App css -->
		<link href="assets/css/bootstrap.min.css" rel="stylesheet"
			type="text/css" />
		<link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
		<link href="assets/css/metismenu.min.css" rel="stylesheet"
			type="text/css" />
		<link href="assets/css/style.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" type="text/css" href="css/pikaday.css" />
		<script type="text/javascript" src="js/pikaday.min.js"></script>
		<script src="assets/js/modernizr.min.js"></script>
        <script type="text/javascript">
			$(function() {
			 	if(${error!=null}){
			 	alert("${error}")
			 	}
			 });
		 </script>
    </head>


    <body>

		<div class="row">
			<div class="col-12">
				<div class="card-box table-responsive">
					<h4 class="m-t-0 header-title">
						<b>商户列表</b>
					</h4>
					<table id="datatable"
						class="table table-striped table-bordered" cellspacing="0"
						width="100%">
						<c:if test="${!empty merchantlist}">
							<thead>
								<tr>
									<th>序号</th>
									<th>代理编号</th>
									<th>商户编号</th>
									<th>商户名称</th>
									<th>所属行业</th>
									<th>入驻时间</th>
									<th>当前状态</th>
									<th>操作</th>
									<%--<th>用户管理</th>--%>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${merchantlist}" var="s" varStatus="st">
									<tr>
										<td>${st.index+1}</td>
										<td>${s.salesno }</td>
										<td><a href="detailsmerchant.html?merchantno=${s.merchantno }">${s.merchantno }</a></td>
										<td>${s.merchantnname }</td>
										<td>${s.industry }</td>
										<td>${s.regtime }</td>
										<c:if test="${s.merchanttype==0 }">
											<td style="color: #229c2a">启用</td>
										</c:if>
										<c:if test="${s.merchanttype==1 }">
											<td style="color: #ff0000">停用</td>
										</c:if>

										<td>
											<c:if test="${mui<4}">
											<a href="merchantInterFace.html?merchantno=${s.merchantno }&mid=${s.mid}&said=${s.said}&salesno=${s.salesno }">通道费率</a>
											&nbsp;&nbsp;&nbsp;&nbsp;
											</c:if>
											<a href="merchantuser.html?merchantno=${s.merchantno }&mid=${s.mid}">用户管理</a>
											&nbsp;&nbsp;&nbsp;&nbsp;
											<c:if test="${s.merchanttype==0 }" >
												<a style="color: #ff0000" href="modifyMerchant.html?merchantno=${s.merchantno }&merchanttype=1">停用</a>
											</c:if>
											<c:if test="${s.merchanttype==1 }">
												<a style="color: #229c2a" href="modifyMerchant.html?merchantno=${s.merchantno }&merchanttype=0">启用</a>
											</c:if>
										</td>
										<%--<td><a href="merchantuser.html?merchantno=${s.merchantno }&mid=${s.mid}">用户管理</a></td>--%>
										<%--<td><a href="updatemerchantturn.html?merchantno=${s.merchantno }">修改</a></td>--%>
									</tr>
								</c:forEach>
							</tbody>
						</c:if>
						<c:if test="${empty merchantlist}">暂时没有商户</c:if>
					</table>
				</div>
			</div>
		</div>
      	<!-- jQuery  -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/popper.min.js"></script>
			<script src="assets/js/bootstrap.min.js"></script>
			<script src="assets/js/metisMenu.min.js"></script>
			<script src="assets/js/waves.js"></script>
			<script src="assets/js/jquery.slimscroll.js"></script>
			<script src="plugins/waypoints/lib/jquery.waypoints.min.js"></script>
			<script src="plugins/counterup/jquery.counterup.min.js"></script>
			<!-- plugin js -->
			<script src="plugins/moment/moment.js"></script>
			<script src="plugins/bootstrap-timepicker/bootstrap-timepicker.js"></script>
			<script
				src="plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
			<script src="plugins/clockpicker/js/bootstrap-clockpicker.min.js"></script>
			<script src="plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
		
			<!-- Required datatable js -->
			<script src="plugins/datatables/jquery.dataTables.min.js"></script>
			<script src="plugins/datatables/dataTables.bootstrap4.min.js"></script>
			<!-- Buttons examples -->
			<script src="plugins/datatables/dataTables.buttons.min.js"></script>
			<script src="plugins/datatables/buttons.bootstrap4.min.js"></script>
			<script src="plugins/datatables/jszip.min.js"></script>
			<script src="plugins/datatables/pdfmake.min.js"></script>
			<script src="plugins/datatables/vfs_fonts.js"></script>
			<script src="plugins/datatables/buttons.html5.min.js"></script>
			<script src="plugins/datatables/buttons.print.min.js"></script>
			<!-- Responsive examples -->
			<script src="plugins/datatables/dataTables.responsive.min.js"></script>
			<script src="plugins/datatables/responsive.bootstrap4.min.js"></script>
		
			<!-- App js -->
			<script src="assets/js/jquery.core.js"></script>
			<script src="assets/js/jquery.app.js"></script>
			<!-- Init js -->
			<script src="assets/pages/jquery.form-pickers.init.js"></script>
			<script type="text/javascript">
		$(document).ready(function() {
			$('#datatable').DataTable({ 'iDisplayLength': 100});
			//$('#datatable-buttons').DataTable();
		
			//Buttons examples
			var table = $('#datatable-buttons').DataTable({
				lengthChange : false,
				buttons : [ 'copy', 'excel', 'pdf' ]
			});
		
			table.buttons().container()
				.appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');
		});
		</script>
    </body>
</html>