<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>fbPay管理后台</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
    <meta content="Coderthemes" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <!-- App favicon -->
    <link rel="shortcut icon" href="assets/images/favicon.ico">

    <!-- App css -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/metismenu.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/style.css" rel="stylesheet" type="text/css" />

    <script src="assets/js/modernizr.min.js"></script>
    <script type="text/javascript">
        $(function() {
            if(${error!=null}){
                alert("${error}")
            }
        });
        function copy(id){
            var Url2=document.getElementById(id).value;
            var oInput = document.createElement('input');
            oInput.value = Url2;
            document.body.appendChild(oInput);
            oInput.select(); // 选择对象
            document.execCommand("Copy"); // 执行浏览器复制命令
            oInput.className = 'oInput';
            oInput.style.display='none';
            alert('复制成功');
        }
    </script>
</head>


<body>
    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <h4 class="m-t-0 header-title">商户基本信息</h4>
                <div class="row">
                    <div class="col-12">
                        <div class="p-20">
                            <div class="form-group row">
                                <label class="col-2 col-form-label">商户编号</label>
                                <div class="col-10">
                                    <input type="text" class="form-control" id="merchantno" name="merchantno" value="${merchant.merchantno}" readonly="readonly">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-2 col-form-label" >商户主体</label>
                                <div class="col-10">
                                    <input type="text" id="merchantnname" name="merchantnname" class="form-control" value="${merchant.merchantnname}" readonly="readonly">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-2 col-form-label">行业类型</label>
                                <div class="col-10">
                                    <input type="text" id="industry" name="industry" class="form-control" value="${merchant.industry}" readonly="readonly">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-2 col-form-label">联系人</label>
                                <div class="col-10">
                                    <input type="text" id="contacts" name="contacts" class="form-control" value="${merchant.contacts}" readonly="readonly">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-2 col-form-label">商户性质</label>
                                <div class="col-10">
                                    <input type="hidden" class="form-control" name="state" id="state" value="${merchant.state}" >
                                    <c:if test="${merchant.state==0}">
                                        <input type="text"  class="form-control" value="正常商户" readonly="readonly"  autocomplete="off">
                                    </c:if>
                                    <c:if test="${merchant.state==1}">
                                        <input type="text"  class="form-control" value="预付费商户" readonly="readonly"  autocomplete="off">
                                    </c:if>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-2 col-form-label">商户资金</label>
                                <label class="col-2 col-form-label">商户余额&nbsp;&nbsp;${merchant.balance/100}</label>
                                <%--<input type="text" name="deposit" id="balance" value="${merchant.balance/100}" readonly="readonly" autocomplete="off">--%>
                                <label class="col-2 col-form-label">可用余额&nbsp;&nbsp;${merchant.avbalance/100}</label>
                                <%--<input type="text" name="deposit" id="avbalance" value="${merchant.avbalance/100}" readonly="readonly" autocomplete="off">--%>
                                <label class="col-2 col-form-label">锁定余额&nbsp;&nbsp;${merchant.lockbalance/100}</label>
                                <%--<input type="text" name="deposit" id="lockbalance" value="${merchant.lockbalance/100}" readonly="readonly" autocomplete="off">--%>
                                <label class="col-2 col-form-label">不可用余额&nbsp;&nbsp;${merchant.unbalance/100}</label>
                                <%--<input type="text" name="deposit" id="unbalance"  value="${merchant.unbalance/100}" readonly="readonly" autocomplete="off">--%>
                            </div>
                            <h4 class="m-t-0 header-title"><b>商户秘钥信息</b></h4>
                            <div class="form-group row">
                                <label class="col-2 col-form-label">私钥</label>
                                <div class="col-10">
                                    <input type="text" class="form-control" id="privateKey" name="privateKey" value="${merchant.privatekey}" readonly="readonly">
                                    <button value="复制" onclick="copy('privateKey');">复制</button>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-2 col-form-label" >公钥</label>
                                <div class="col-10">
                                    <input type="text" id="publicKey" name="publicKey" class="form-control" value="${merchant.publickey}" readonly="readonly">
                                    <button value="复制" onclick="copy('publicKey');">复制</button>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-2 col-form-label">token</label>
                                <div class="col-10">
                                    <input type="text" id="token" name="token" class="form-control" value="${merchant.token}" readonly="readonly">
                                    <button value="复制" onclick="copy('token');">复制</button>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-2 col-form-label">当前状态</label>
                                <div class="col-10">
                                    <c:if test="${merchant.merchanttype==0 }">
                                        <input type="text" class="form-control" name="merchanttype" id="merchanttype" value="启用" readonly >
                                    </c:if>
                                    <c:if test="${merchant.merchanttype==1 }">
                                        <input type="text" class="form-control" name="merchanttype" id="merchanttype" value="停用" readonly >
                                    </c:if>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-10">
                                    <input  type="button"  value="返回" onclick="location.href='merchantlist.html'"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!-- end card-box -->
        </div><!-- end col -->
    </div>



<!-- jQuery  -->
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/popper.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/metisMenu.min.js"></script>
<script src="assets/js/waves.js"></script>
<script src="assets/js/jquery.slimscroll.js"></script>
<script src="plugins/waypoints/lib/jquery.waypoints.min.js"></script>
<script src="plugins/counterup/jquery.counterup.min.js"></script>

<!-- Chart JS -->
<script src="plugins/chart.js/chart.bundle.js"></script>

<!-- init dashboard -->
<script src="assets/pages/jquery.dashboard.init.js"></script>

<!-- App js -->
<script src="assets/js/jquery.core.js"></script>
<script src="assets/js/jquery.app.js"></script>

</body>
</html>