<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<title>Layui</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">
	<link rel="stylesheet" href="css/layui.css" media="all" />
	<link rel="stylesheet" href="css/begtable.css" />
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<script src="assets/js/jquery.min.js"></script>
	<script type="text/javascript" src="assets/js/adddate.js" ></script>
	<script type="text/javascript">


		function checkinput() {
			if(document.getElementById("orderno").value!=""){
				var form = document.getElementById('formid');
				//再次修改input内容
				form.submit();
			}
			if(document.getElementById("sysorderno").value!=""){
				var form = document.getElementById('formid');
				//再次修改input内容
				form.submit();
			}
			if(document.getElementById("orderno").value==""&&document.getElementById("sysorderno").value==""){
				if(document.getElementById("begintime").value==""||document.getElementById("endtime").value==""){
					alert("请选择时间！");
					return false;
				}else{
					if(document.getElementById("begintime").value==""&&document.getElementById("endtime").value!=""){
						alert("请选择开始时间！");
						return false;
					}
					if(document.getElementById("endtime").value==""&&document.getElementById("begintime").value!="") {
						alert("请选择结束时间！");
						return false;
					}
					if (document.getElementById("begintime").value!=""&&document.getElementById("endtime").value!="") {
						if(DateMinus(document.getElementById("begintime").value,document.getElementById("endtime").value)<0){
							alert("结束时间必须大于开始时间！");
							return false;
						}
						if(DateMinus(document.getElementById("begintime").value,document.getElementById("endtime").value)>=31){
							alert("查询周期不能大于30天！");
							return false;
						}else{
							var form = document.getElementById('formid');
							//再次修改input内容
							form.submit();
						}
					}
				}
			}
		}
		function DateMinus(btime,etime){
			var begintime = new Date(btime.replace(/-/g, "/"));
			var endtime = new Date(etime.replace(/-/g, "/"));
			var days = endtime.getTime() - begintime.getTime();
			var day = parseInt(days / (1000 * 60 * 60 * 24));
			return day;
		}
	</script>

</head>

<body>

<div style="margin: 15px;">
	<fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
		<legend>查询订单</legend>
	</fieldset>
	<div>
		<form id="formid"  action="rechargeOrderlist.html" method="post">
			<div class="layui-form-item">
				<div class="layui-inline">
					<label class="layui-form-label">商户编号</label>
					<div class="layui-input-block">
						<input type="text" id="merchantno" name="merchantno" value="" lay-verify="title" autocomplete="off" placeholder="请输入商户编号" class="layui-input">
					</div>
				</div>
				<div class="layui-inline">
					<label class="layui-form-label">商户订单号</label>
					<div class="layui-input-block">
						<input type="text" id="orderno" name="orderno" value="" lay-verify="title" autocomplete="off" placeholder="请输入商户订单号" class="layui-input">
					</div>
				</div>
				<div class="layui-inline">
					<label class="layui-form-label">系统订单号</label>
					<div class="layui-input-block">
						<input type="text" id="sysorderno" name="sysorderno" value="" lay-verify="title" autocomplete="off" placeholder="请输入系统订单号" class="layui-input">
					</div>
				</div>

			</div>
			<div class="layui-form-item">

				<div class="layui-inline">
					<label class="layui-form-label">订单日期</label>
					<div class="layui-input-block">
						<input type="text" id="begintime" name="begintime" readonly="readonly"  placeholder="请选择开始时间" lay-verify="date" autocomplete="off" class="layui-input" onclick="SelectDate(this,'yyyy-MM-dd hh:mm:ss')">
					</div>
				</div>
				<div class="layui-inline">
					<label class="layui-form-label">-----</label>
					<div class="layui-input-block">
						<input type="text" id="endtime" name="endtime" readonly="readonly"  placeholder="请选择结束时间" lay-verify="date" autocomplete="off" class="layui-input" onclick="SelectDate(this,'yyyy-MM-dd hh:mm:ss')">
					</div>
				</div>
				<div class="layui-inline">
					<label class="layui-form-label">订单状态</label>
					<div class="layui-input-block">
						<select name="ordertype" id="ordertype">
							<option value="100" selected="">全部</option>
							<option value="0">订单生成</option>
							<option value="1">订单确认</option>
							<option value="2">跳转支付</option>
							<option value="3">等待支付</option>
							<option value="4">充值成功</option>
							<option value="5">链接超时</option>
							<option value="6">未支付</option>
							<option value="7">补单完成</option>
						</select>
					</div>
				</div>
				<div class="layui-inline">
					<div class="layui-input-block">
						<input type="button" class="layui-btn" value="查询" onclick="checkinput();"/>
					</div>
				</div>
			</div>
		</form>
	</div>
	<blockquote class="layui-elem-quote">
		订单列表
	</blockquote>
	<hr>
	<div style="width: 100%; height: 100%; border: 1px solid #009688;">
		<div class="beg-table-box">
			<div class="beg-table-body">
				<table class="beg-table beg-table-bordered beg-table-striped beg-table-hovered">
					<c:if test="${!empty rechargeOrderlist}">
						<thead>
						<tr>
							<th>序号</th>
							<th>商户编号</th>
							<th>代理编号</th>
							<th style="width: 50px;">商户订单号</th>
							<th>系统订单号</th>
							<th>订单时间</th>
							<th>确认时间</th>
							<th>等待时间</th>
							<th>完成时间</th>
							<th>订单金额</th>
							<th>应付金额</th>
							<th>实际金额</th>
							<th>通道</th>
							<th>费率(%)</th>
							<th>手续费</th>
							<th>代理利率(%)</th>
							<th>代理收入</th>
							<th>状态</th>
							<th>回调</th>
						</tr>
						</thead>
						<tbody>
						<c:forEach items="${rechargeOrderlist}" var="s" varStatus="st">
							<tr>
								<td>${st.index+1}</td>
								<td>${s.merchantno}</td>
								<td>${s.salesno}</td>
								<td>${s.orderno}</td>
								<td>${s.sysorderno}</td>
								<td>${s.sysordertime}</td>
								<td>${s.confirmtime}</td>
								<td>${s.waittime}</td>
								<td>${s.endtime}</td>
								<td><font style="color: red">${s.orderamount/100}</font></td>
								<td><font style="color: red">${s.saleamount/100}</font></td>
								<td><font style="color: red">${s.receivableamount/100}</font></td>
								<td><font style="color: #229c2a">${map[s.paychanneltype]}</font></td>
								<td>${s.rate}</td>
								<td>${s.handlingfee/100}</td>
								<td>${s.srate*100}</td>
								<td>${s.shandlingfee/100}</td>
								<c:if test="${s.ordertype==0}">
									<td>发起</td>
								</c:if>
								<c:if test="${s.ordertype==1}">
									<td>确认</td>
								</c:if>
								<c:if test="${s.ordertype==2}">
									<td>跳转</td>
								</c:if>
								<c:if test="${s.ordertype==3}">
									<td>等待</td>
								</c:if>
								<c:if test="${s.ordertype==4}">
									<td>充值成功</td>
								</c:if>
								<c:if test="${s.ordertype==5}">
									<td>链接超时</td>
								</c:if>
								<c:if test="${s.ordertype==6}">
									<td>未支付</td>
								</c:if>
								<c:if test="${s.ordertype==7}">
									<td>补单</td>
								</c:if>
								<c:if test="${s.ordertype==4||s.ordertype==7}">
									<c:if test="${s.callback==200}">
										<td>回调成功</td>
									</c:if>
									<c:if test="${s.callback!=200}">
										<td>回调失败</td>
									</c:if>
								</c:if>
								<c:if test="${s.ordertype!=4&&s.ordertype!=7}">
									<td>----</td>
								</c:if>
							</tr>
						</c:forEach>
						</tbody>
					</c:if>
					<c:if test="${empty rechargeOrderlist}">暂时没有订单</c:if>
				</table>
			</div>
			<div class="beg-table-paged"></div>
		</div>
	</div>
</div>
<input type="hidden" id="pageBeginTime" name="pageBeginTime" value="${begintime}"/>
<input type="hidden" id="pageEndTime" name="pageEndTime" value="${endtime}"/>
<input type="hidden" id="pageMerchantNo" name="pageMerchantNo" value="${merchantno}"/>
<input type="hidden" id="pageOrderType" name="pageOrderType" value="${ordertype}"/>
<input type="hidden" id="pageCardNum" name="pageCardNum" value="${begintime}"/>
<script type="text/javascript" src="plugins/layui/layui.js"></script>
<script>
	layui.config({
		base: 'js/'
	});
	var page = 1;
	if(${pageTimes>5}){
		page=5;
	}else{
		page=${pageTimes};
	}
	layui.use('begtable', function() {
		var begtable = layui.begtable(),
				layer = layui.layer,
				$ = layui.jquery,
				laypage = layui.laypage;

		laypage({
			cont: $('.beg-table-paged'),
			pages: "${pageTimes}" //总页数
			,curr:"${pageNum}"
			,
			groups: page //连续显示分页数
			,
			jump: function(obj, first) {
				//得到了当前页，用于向服务端请求对应数据
				var curr = obj.curr;
				if(!first) {
					// alert('第 '+ obj.curr +' 页');
					location.href="rechargeOrderPagelist.html?merchantno=${merchantno}&orderno=${orderno}&sysorderno=${sysorderno}&begintime=${begintime}&endtime=${endtime}&ordertype=${ordertype}&pageNum="+obj.curr;
					<%--$.ajax({--%>
					<%--type: "post",--%>
					<%--async: true,--%>
					<%--url: "rechargeOrderPagelist.html?merchantno=${merchantno}&orderno=${orderno}&sysorderno=${sysorderno}&begintime=${begintime}&endtime=${endtime}&ordertype=${ordertype}&cardNum=${cardNum}&pageNum="+obj.curr,--%>
					<%--data: null,--%>
					<%--dataType: "text",--%>
					<%--success: function (res) {--%>

					<%--}--%>
					<%--})--%>
				}
			}
		});
	});
</script>

</body>

</html>