﻿<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html>
<html>

    <head>
        <meta charset="utf-8" />
        <title>错误页面</title>
        <script src="js/jquery.min.js"></script>
        <style type="text/css">
			#bcid{
			height:500px;
			width:500px;
			top:50%;
			left:50%;
			position:absolute;
			margin-top:-250px;
			margin-left:-250px;
			}
		</style>
        
    </head>

    <body>
        <div id="bcid">
            <div style="font-size: 80px;">${msg }</div>
        </div>
    </body>

</html>