﻿<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>

<html>

<head>
    <meta charset="utf-8">
    <title>后台管理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">

    <link rel="stylesheet" href="plugins/layui/css/layui.css" media="all" />
    <link rel="stylesheet" href="css/global.css" media="all">
    <link rel="stylesheet" href="plugins/font-awesome/css/font-awesome.min.css">
    <script src="assets/js/jquery.min.js"></script>
    <script type="text/javascript">
        function changePWD() {
            var truthBeTold = window.confirm("真的修改密码？");
            if(truthBeTold) {
                var oldpwd = window.prompt("输入旧密码：", "");
                var newpwd = window.prompt("输入新密码：", "");

                if(oldpwd=="${mui.spassword}"){
                    $.ajax({
                        type: "post",
                        async: true,
                        url: "changePwd.html?loginname=${mui.sloginname}&oldpwd=" + oldpwd + "&newpwd=" + newpwd,
                        data: null,
                        dataType: "text",
                        success: function (res) {
                            //alert(res);
                            if (res == "ok") {
                                alert("修改成功！新密码："+newpwd+"请牢记！");
                                location.href="logout.html";
                            }
                            if (res == "error") {
                                alert("操作失败");
                            }
                            if (res == "oldpwderror") {
                                alert("旧密码错误");
                            }
                            if (res == "exception") {
                                alert("系统异常");
                            }

                        }
                    })
                }else{
                    alert("旧密码错误");
                }
            }
        }
    </script>
</head>

<body>
<div class="layui-layout layui-layout-admin" style="border-bottom: solid 5px #1aa094;">
    <div class="layui-header header header-demo">
        <div class="layui-main">
            <div class="admin-login-box">
                <a class="logo" style="left: 0;" href="#">
                    <span style="font-size: 22px;">代理后台</span>
                </a>
                <div class="admin-side-toggle">
                    <i class="fa fa-bars" aria-hidden="true"></i>
                </div>
                <div class="admin-side-full">
                    <i class="fa fa-life-bouy" aria-hidden="true"></i>
                </div>
            </div>
            <ul class="layui-nav admin-header-item">
                <li class="layui-nav-item">
                    <a href="#" onclick ="changePWD()">修改密码</a>
                </li>
                <li class="layui-nav-item">
                    <a href="javascript:;">清除缓存</a>
                </li>
                <li class="layui-nav-item">
                    <a href="javascript:;" class="admin-header-user">
                        <img src="images/0.jpg" />
                        <span>${mui.sloginname}</span>
                    </a>
                    <dl class="layui-nav-child">
                        <%--<dd>--%>
                            <%--<a href="javascript:;"><i class="fa fa-user-circle" aria-hidden="true"></i> 个人信息</a>--%>
                        <%--</dd>--%>
                        <%--<dd>--%>
                            <%--<a href="javascript:;"><i class="fa fa-gear" aria-hidden="true"></i> 修改密码</a>--%>
                        <%--</dd>--%>
                        <%--<dd id="lock">--%>
                            <%--<a href="javascript:;">--%>
                                <%--<i class="fa fa-lock" aria-hidden="true" style="padding-right: 3px;padding-left: 1px;"></i> 锁屏 (Alt+L)--%>
                            <%--</a>--%>
                        <%--</dd>--%>
                        <dd>
                            <a href="logout.html"><i class="fa fa-sign-out" aria-hidden="true"></i> 注销</a>
                        </dd>
                    </dl>
                </li>
            </ul>
            <ul class="layui-nav admin-header-item-mobile">
                <li class="layui-nav-item">
                    <a href="logout.html"><i class="fa fa-sign-out" aria-hidden="true"></i> 注销</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="layui-side layui-bg-black" id="admin-side">
        <div class="layui-side-scroll" id="admin-navbar-side" lay-filter="side"></div>
    </div>
    <div class="layui-body" style="bottom: 0;border-left: solid 2px #1AA094;" id="admin-body">
        <div class="layui-tab admin-nav-card layui-tab-brief" lay-filter="admin-tab">
            <ul class="layui-tab-title">
                <li class="layui-this">
                    <i class="fa fa-dashboard" aria-hidden="true"></i>
                    <cite>主页</cite>
                </li>
            </ul>
            <div class="layui-tab-content" style="min-height: 150px; padding: 5px 0 0 0;">
                <div class="layui-tab-item layui-show">
                    <iframe src="homeJump.html"></iframe>
                </div>
            </div>
        </div>
    </div>
    <div class="layui-footer footer footer-demo" id="admin-footer">
        <div class="layui-main">
            <p>2019 &copy;
                <a href="#">Pay</a>
            </p>
        </div>
    </div>
    <div class="site-tree-mobile layui-hide">
        <i class="layui-icon">&#xe602;</i>
    </div>
    <div class="site-mobile-shade"></div>

    <%--<!--锁屏模板 start-->--%>
    <%--<script type="text/template" id="lock-temp">--%>
        <%--<div class="admin-header-lock" id="lock-box">--%>
            <%--<div class="admin-header-lock-img">--%>
                <%--<img src="images/0.jpg"/>--%>
            <%--</div>--%>
            <%--<div class="admin-header-lock-name" id="lockUserName">${mui.mloginname}</div>--%>
            <%--<input type="text" class="admin-header-lock-input" value="输入密码解锁.." name="lockPwd" id="lockPwd" />--%>
            <%--<button class="layui-btn layui-btn-small" id="unlock">解锁</button>--%>
        <%--</div>--%>
    <%--</script>--%>
    <%--<!--锁屏模板 end -->--%>

    <script type="text/javascript" src="plugins/layui/layui.js"></script>
    <script type="text/javascript" src="datas/nav.js"></script>
    <script>
        navs=${menuJson};
    </script>
    <script src="js/index.js"></script>
    <%--<script>--%>
        <%--layui.use('layer', function() {--%>
            <%--var $ = layui.jquery,--%>
                <%--layer = layui.layer;--%>

            <%--$('#video1').on('click', function() {--%>
                <%--layer.open({--%>
                    <%--title: 'YouTube',--%>
                    <%--maxmin: true,--%>
                    <%--type: 2,--%>
                    <%--content: 'video.html',--%>
                    <%--area: ['800px', '500px']--%>
                <%--});--%>
            <%--});--%>
            <%--$('#pay').on('click', function () {--%>
                <%--layer.open({--%>
                    <%--title: false,--%>
                    <%--type: 1,--%>
                    <%--content: '<img src="images/xx.png" />',--%>
                    <%--area: ['500px', '250px'],--%>
                    <%--shadeClose: true--%>
                <%--});--%>
            <%--});--%>

        <%--});--%>
    <%--</script>--%>
</div>
</body>

</html>