<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>
<!DOCTYPE html>
<head>
    <meta charset="utf-8" />
    <title>管理后台</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
    <meta content="Coderthemes" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <!-- App favicon -->
    <link rel="shortcut icon" href="assets/images/favicon.ico">
    <!-- App css -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/metismenu.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/style.css" rel="stylesheet" type="text/css" />
</head>
<html>
<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-6 col-md-3 col-lg-3 col-xl-3">
            <div class="card-box tilebox-one">
                <i class="fi-layers float-right"></i>
                <h8 class="text-muted text-uppercase mb-3">今日总订单数</h8>
                <h6 class="mb-3">${merchantReport.countOrder}笔</h6>
            </div>
        </div>
        <div class="col-xs-6 col-md-3 col-lg-3 col-xl-3">
            <div class="card-box tilebox-one">
                <i class="dripicons-checkmark float-right"></i>
                <h8 class="text-muted text-uppercase mb-3">支付订单数</h8>
                <h6 class="mb-3">${merchantReport.success}笔</h6>
            </div>
        </div>
        <div class="col-xs-6 col-md-3 col-lg-3 col-xl-3">
            <div class="card-box tilebox-one">
                <i class="dripicons-warning float-right"></i>
                <h8 class="text-muted text-uppercase mb-3">未支付订单数</h8>
                <h6 class="mb-3">${merchantReport.fail}笔</h6>
            </div>
        </div>
        <div class="col-xs-6 col-md-3 col-lg-3 col-xl-3">
            <div class="card-box tilebox-one">
                <i class="dripicons-warning float-right"></i>
                <h8 class="text-muted text-uppercase mb-3">支付率/成功率</h8>
                <c:if test="${merchantReport.countOrder==0}">
                    <h6 class="mb-3">0.00%&nbsp;&nbsp;&nbsp;&nbsp;/&nbsp;&nbsp;&nbsp;&nbsp;0.00%</h6>
                </c:if>
                <c:if test="${merchantReport.countOrder>0}">
                    <h6 class="mb-3">
                        <fmt:formatNumber type="number" value="${merchantReport.success/merchantReport.countOrder*100}" pattern="0.00" maxFractionDigits="2"/>
                        %&nbsp;&nbsp;/&nbsp;&nbsp;
                        <c:if test="${merchantReport.success>0}">
                            <fmt:formatNumber type="number" value="${merchantReport.oksuccess/merchantReport.success*100}" pattern="0.00" maxFractionDigits="2"/>%
                        </c:if>
                        <c:if test="${merchantReport.success==0}">
                            0.00%
                        </c:if>
                    </h6>
                </c:if>
            </div>
        </div>

    </div>
    <div class="row">
        <div class="col-xs-6 col-md-3 col-lg-3 col-xl-3">
            <div class="card-box tilebox-one">
                <i class="fa fa-cny float-right"></i>
                <h8 class="text-muted text-uppercase mb-3">所有订单总额</h8>
                <h6 class="mb-3">${merchantReport.orderAmount/100}元</h6>
            </div>
        </div>
        <div class="col-xs-6 col-md-3 col-lg-3 col-xl-3">
            <div class="card-box tilebox-one">
                <i class="fa fa-cny float-right"></i>
                <h8 class="text-muted text-uppercase mb-3">成功订单总额</h8>
                <h6 class="mb-3">${merchantReport.okOrderAmount/100}元</h6>
            </div>
        </div>
        <div class="col-xs-6 col-md-3 col-lg-3 col-xl-3">
            <div class="card-box tilebox-one">
                <i class="fa fa-cny float-right"></i>
                <h8 class="text-muted text-uppercase mb-3">实际充值总额</h8>
                <h6 class="mb-3">${merchantReport.receivableAmount/100}元</h6>
            </div>
        </div>
        <div class="col-xs-6 col-md-3 col-lg-3 col-xl-3">
            <div class="card-box tilebox-one">
                <i class="fa fa-cny float-right"></i>
                <h8 class="text-muted text-uppercase mb-3">充值平均金额</h8>
                <c:if test="${merchantReport.success>0}">
                    <h6 class="mb-3"><fmt:formatNumber type="number" value="${merchantReport.receivableAmount/merchantReport.success/100}" pattern="0.00" maxFractionDigits="2"/>元</h6>
                </c:if>
                <c:if test="${merchantReport.success==0}">
                    <h6 class="mb-3">0元</h6>
                </c:if>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-6 col-md-3 col-lg-3 col-xl-3">
            <div class="card-box tilebox-one">
                <i class="dripicons-checkmark float-right"></i>
                <h8 class="text-muted text-uppercase mb-3">正常笔数</h8>
                <h6 class="mb-3">${merchantReport.oksuccess}</h6>
            </div>
        </div>
        <div class="col-xs-6 col-md-3 col-lg-3 col-xl-3">
            <div class="card-box tilebox-one">
                <i class="dripicons-checkmark float-right"></i>
                <h8 class="text-muted text-uppercase mb-3">补单笔数</h8>
                <h6 class="mb-3">${merchantReport.rsuccess}</h6>
            </div>
        </div>
        <div class="col-xs-6 col-md-3 col-lg-3 col-xl-3">
            <div class="card-box tilebox-one">
                <i class="fa fa-cny float-right"></i>
                <h8 class="text-muted text-uppercase mb-3">正常金额</h8>
                <h6 class="mb-3">${merchantReport.okmoney/100}元</h6>
            </div>
        </div>
        <div class="col-xs-6 col-md-3 col-lg-3 col-xl-3">
            <div class="card-box tilebox-one">
                <i class="fa fa-cny float-right"></i>
                <h8 class="text-muted text-uppercase mb-3">补单金额</h8>
                <h6 class="mb-3">${merchantReport.rmoney/100}元</h6>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-6 col-md-3 col-lg-3 col-xl-3">
            <div class="card-box tilebox-one">
                <i class="fa fa-cny float-right"></i>
                <h8 class="text-muted text-uppercase mb-3">商户手续费</h8>
                <h6 class="mb-3">${merchantReport.handlingfee/100}元</h6>
            </div>
        </div>
        <div class="col-xs-6 col-md-3 col-lg-3 col-xl-3">
            <div class="card-box tilebox-one">
                <i class="fa fa-cny float-right"></i>
                <h8 class="text-muted text-uppercase mb-3">代理手续费</h8>
                <h6 class="mb-3">${merchantReport.shandlingfee/100}元</h6>
            </div>
        </div>
        <div class="col-xs-6 col-md-3 col-lg-3 col-xl-3">
            <div class="card-box tilebox-one">
                <i class="fa fa-cny float-right"></i>
                <h8 class="text-muted text-uppercase mb-3">代理收入</h8>
                <h6 class="mb-3">${merchantReport.income/100}元</h6>
            </div>
        </div>
    </div>

</div>
</body>
</html>