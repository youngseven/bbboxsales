<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>管理后台</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <!-- DataTables -->
		<link href="plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
		<link href="plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
		<!-- Responsive datatable examples -->
		<link href="plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
		<!-- Plugins css -->
		<link href="plugins/bootstrap-timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
		<link href="plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css" rel="stylesheet">
		<link href="plugins/clockpicker/css/bootstrap-clockpicker.min.css" rel="stylesheet">
		<link href="plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
		<!-- App css -->
		<link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		<link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
		<link href="assets/css/metismenu.min.css" rel="stylesheet" type="text/css" />
		<link href="assets/css/style.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" type="text/css" href="css/pikaday.css" />
		<script type="text/javascript" src="js/pikaday.min.js"></script>
		<script src="assets/js/modernizr.min.js"></script>
        <script type="text/javascript">
			function addSalesJump() {
				location.href="addSalesJump.html";
			}

		</script>
    </head>


    <body>
		<div class="row">
			<div class="col-12">
				<div class="card-box table-responsive">
					<h4 class="page-title float-left"><b>代理列表</b></h4>
					<input type="button" value="新增代理" onclick="addSalesJump();"/>
					<table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
						<c:if test="${!empty salesList}">
							<thead>
								<tr>
									<th>序号</th>
									<th>代理编号</th>
									<th>代理名称</th>
									<th>登录名</th>
									<th>密码</th>
									<th>代理余额</th>
									<th>可用余额</th>
									<th>锁定余额</th>
									<th>不可用余额</th>
									<th>登记时间</th>
									<th>通道管理</th>
									<th>商户管理</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${salesList}" var="s" varStatus="st">
									<tr>
										<td>${st.index+1}</td>
										<td>${s.salesno }</td>
										<td>${s.salesname}</td>
										<td>${s.sloginname}</td>
										<td>${s.spassword}</td>
										<td>${s.balance/100}</td>
										<td>${s.avbalance/100}</td>
										<td>${s.lockbalance/100}</td>
										<td>${s.unbalance/100}</td>
										<td>${s.regtime}</td>
										<td><a href="addSalesInterFaceJump.html?said=${s.said}&salesno=${s.salesno }">新增通道</a>
											&nbsp;&nbsp;&nbsp;&nbsp;
											<a href="salesInterFaceList.html?said=${s.said}&salesno=${s.salesno }">查看通道</a>
										</td>
										<td><a href="salesMerchantJump.html?said=${s.said}&salesno=${s.salesno }">新增商户</a>
											&nbsp;&nbsp;&nbsp;&nbsp;
											<a href="salesMerchantlist.html?said=${s.said}&salesno=${s.salesno }">查看商户</a>
										</td>

									</tr>
								</c:forEach>
							</tbody>
						</c:if>
						<c:if test="${empty salesList}">
							<div>目前没有代理</div>
						</c:if>
					</table>
				</div>
			</div>
		</div>
 


        <!-- jQuery  -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/popper.min.js"></script>
			<script src="assets/js/bootstrap.min.js"></script>
			<script src="assets/js/metisMenu.min.js"></script>
			<script src="assets/js/waves.js"></script>
			<script src="assets/js/jquery.slimscroll.js"></script>
			<script src="plugins/waypoints/lib/jquery.waypoints.min.js"></script>
			<script src="plugins/counterup/jquery.counterup.min.js"></script>
			<!-- plugin js -->
			<script src="plugins/moment/moment.js"></script>
			<script src="plugins/bootstrap-timepicker/bootstrap-timepicker.js"></script>
			<script
				src="plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
			<script src="plugins/clockpicker/js/bootstrap-clockpicker.min.js"></script>
			<script src="plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
		
			<!-- Required datatable js -->
			<script src="plugins/datatables/jquery.dataTables.min.js"></script>
			<script src="plugins/datatables/dataTables.bootstrap4.min.js"></script>
			<!-- Buttons examples -->
			<script src="plugins/datatables/dataTables.buttons.min.js"></script>
			<script src="plugins/datatables/buttons.bootstrap4.min.js"></script>
			<script src="plugins/datatables/jszip.min.js"></script>
			<script src="plugins/datatables/pdfmake.min.js"></script>
			<script src="plugins/datatables/vfs_fonts.js"></script>
			<script src="plugins/datatables/buttons.html5.min.js"></script>
			<script src="plugins/datatables/buttons.print.min.js"></script>
			<!-- Responsive examples -->
			<script src="plugins/datatables/dataTables.responsive.min.js"></script>
			<script src="plugins/datatables/responsive.bootstrap4.min.js"></script>
		
			<!-- App js -->
			<script src="assets/js/jquery.core.js"></script>
			<script src="assets/js/jquery.app.js"></script>
			<!-- Init js -->
			<script src="assets/pages/jquery.form-pickers.init.js"></script>
       
    </body>
</html>