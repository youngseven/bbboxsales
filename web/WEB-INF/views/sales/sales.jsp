<%@ page language="java" import="java.util.*"  pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>管理后台</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <!-- App favicon -->
        <link rel="shortcut icon" href="assets/images/favicon.ico">

        <!-- Plugins css-->
        <link href="plugins/bootstrap-tagsinput/css/bootstrap-tagsinput.css" rel="stylesheet" />
        <link href="plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" />
        <link href="plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="plugins/bootstrap-touchspin/css/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="plugins/switchery/switchery.min.css">

        <!-- App css -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/metismenu.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/style.css" rel="stylesheet" type="text/css" />

        <script src="assets/js/modernizr.min.js"></script>
        <script type="text/javascript">

		 </script>
    </head>


    <body>
        <div class="row">
            <div class="col-12">
                <div class="card-box">
                    <h4 class="m-t-0 header-title"><b>商户代理</b></h4>
                    <form id="formid"  action="addSalses.html" method="post">
                    <div class="row">
                        <div class="col-12">
                            <div class="p-20">
                                <div class="form-group row">
                                    <label class="col-1 col-form-label">代理编号</label>
                                    <div class="col-2">
                                        <input type="text" class="form-control" value="${sales.salesno}" name="salesno" id="salesno" readonly autocomplete="off"/>
                                    </div>
                                    <label class="col-1 col-form-label">代理名称</label>
                                    <div class="col-2">
                                        <input type="text" class="form-control" value="${sales.salesname}" name="salesname" id="salesname" readonly autocomplete="off"/>
                                    </div>
                                    <label class="col-1 col-form-label">操作</label>
                                    <div class="col-2">
                                        <a type="button" style="width: 200px; text-align: center;height: 30px; line-height: 30px" href="DrawMoneyJump.html?salesno=${sales.salesno }">结款申请</a>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-1 col-form-label">登录名称</label>
                                    <div class="col-2">
                                        <input type="text" class="form-control" value="${sales.sloginname}" name="sloginname" id="sloginname" readonly autocomplete="off"/>
                                    </div>
                                    <label class="col-1 col-form-label">登录密码</label>
                                    <div class="col-2">
                                        <input type="text" class="form-control" value="${sales.spassword}" name="spassword" id="spassword" readonly autocomplete="off"/>
                                    </div>
                                    <label class="col-1 col-form-label">注册时间</label>
                                    <div class="col-2">
                                        <input type="text" class="form-control" value="${sales.regtime}" name="regtime" id="regtime" readonly autocomplete="off"/>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-1 col-form-label">代理余额</label>
                                    <div class="col-2">
                                    <input type="text" class="form-control" value="${sales.balance/100}" name="balance" id="balance" readonly autocomplete="off"/>
                                    </div>
                                    <label class="col-1 col-form-label">可用余额</label>
                                    <div class="col-2">
                                        <input type="text" class="form-control" value="${sales.avbalance/100}" name="avbalance" id="avbalance" readonly autocomplete="off"/>
                                    </div>
                                    <label class="col-1 col-form-label">锁定余额</label>
                                    <div class="col-2">
                                        <input type="text" class="form-control" value="${sales.lockbalance/100}" name="lockbalance" id="lockbalance" readonly autocomplete="off"/>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                        <c:if test="${!empty salesInterFaceList}">
                                            <tr>
                                                <th>序号</th>
                                                <th>通道</th>
                                                <th>费率%</th>
                                                <th>状态</th>
                                            </tr>
                                            <c:forEach items="${salesInterFaceList}" var="s" varStatus="st">
                                                <tr>
                                                    <td>${st.index+1}</td>
                                                    <td>${map[s.pid]}</td>
                                                    <td><fmt:formatNumber type="float" value="${s.rate*100}" pattern="#.##" minFractionDigits="2"/></td>
                                                    <c:if test="${s.state==0}">
                                                        <td>启用</td>
                                                    </c:if>
                                                    <c:if test="${s.state==1}">
                                                        <td>停用</td>
                                                    </c:if>
                                                    <c:if test="${s.state==2}">
                                                        <td>弃用</td>
                                                    </c:if>
                                                </tr>
                                            </c:forEach>
                                        </c:if>
                                        <c:if test="${empty salesInterFaceList}">该代理没有开通通道</c:if>
                                    </table>
                                </div>
                                <div class="form-group row">
                                    <table id="datatable"
                                           class="table table-striped table-bordered" cellspacing="0"
                                           width="100%">
                                        <c:if test="${!empty merchantList}">
                                            <thead>
                                            <tr>
                                                <th>序号</th>
                                                <th>商户编号</th>
                                                <th>商户名称</th>
                                                <th>所属行业</th>
                                                <th>入驻时间</th>
                                                <th>当前状态</th>
                                                <th>操作</th>
                                                    <%--<th>用户管理</th>--%>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <c:forEach items="${merchantList}" var="s" varStatus="st">
                                                <tr>
                                                    <td>${st.index+1}</td>
                                                    <td><a href="detailsmerchant.html?merchantno=${s.merchantno }">${s.merchantno }</a></td>
                                                    <td>${s.merchantnname }</td>
                                                    <td>${s.industry }</td>
                                                    <td>${s.regtime }</td>
                                                    <c:if test="${s.merchanttype==0 }">
                                                        <td style="color: #229c2a">启用</td>
                                                    </c:if>
                                                    <c:if test="${s.merchanttype==1 }">
                                                        <td style="color: #ff0000">停用</td>
                                                    </c:if>
                                                    <td>
                                                        <a href="merchantInterFace.html?merchantno=${s.merchantno }&mid=${s.mid}&said=${s.said}&salesno=${s.salesno }">通道费率</a>
                                                    </td>
                                                </tr>
                                            </c:forEach>
                                            </tbody>
                                        </c:if>
                                        <c:if test="${empty merchantList}">暂时没有商户</c:if>
                                    </table>
                                </div>


                            </div>
                        </div>
                    </div>
                    </form>
                    <!-- end row -->

                </div> <!-- end card-box -->
            </div><!-- end col -->
        </div>

 


        <!-- jQuery  -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/popper.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/metisMenu.min.js"></script>
        <script src="assets/js/waves.js"></script>
        <script src="assets/js/jquery.slimscroll.js"></script>

        <script src="plugins/switchery/switchery.min.js"></script>
        <script src="plugins/bootstrap-tagsinput/js/bootstrap-tagsinput.min.js"></script>
        <script src="plugins/select2/js/select2.min.js" type="text/javascript"></script>
        <script src="plugins/bootstrap-select/js/bootstrap-select.js" type="text/javascript"></script>
        <script src="plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js" type="text/javascript"></script>
        <script src="plugins/bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js" type="text/javascript"></script>
        <script src="plugins/bootstrap-maxlength/bootstrap-maxlength.js" type="text/javascript"></script>

        <script type="text/javascript" src="plugins/autocomplete/jquery.mockjax.js"></script>
        <script type="text/javascript" src="plugins/autocomplete/jquery.autocomplete.min.js"></script>
        <script type="text/javascript" src="plugins/autocomplete/countries.js"></script>
        <script type="text/javascript" src="assets/pages/jquery.autocomplete.init.js"></script>

        <!-- Init Js file -->
        <script type="text/javascript" src="assets/pages/jquery.form-advanced.init.js"></script>

        <!-- App js -->
        <script src="assets/js/jquery.core.js"></script>
        <script src="assets/js/jquery.app.js"></script>

    </body>
</html>