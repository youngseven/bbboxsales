<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>管理后台</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <!-- DataTables -->
		<link href="plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
		<link href="plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
		<!-- Responsive datatable examples -->
		<link href="plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
		<!-- Plugins css -->
		<link href="plugins/bootstrap-timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
		<link href="plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css" rel="stylesheet">
		<link href="plugins/clockpicker/css/bootstrap-clockpicker.min.css" rel="stylesheet">
		<link href="plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
		<!-- App css -->
		<link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		<link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
		<link href="assets/css/metismenu.min.css" rel="stylesheet" type="text/css" />
		<link href="assets/css/style.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" type="text/css" href="css/pikaday.css" />
		<script type="text/javascript" src="js/pikaday.min.js"></script>
		<script src="assets/js/modernizr.min.js"></script>
        <script type="text/javascript">

		</script>
    </head>


    <body>
		<div class="row">
			<div class="col-12">

				<div class="card-box table-responsive">
					<div class="col-10">
						<h4 class="m-t-0 header-title">代理(${salesno})--通道列表</h4>
						<c:if test="${mui<3}">
						&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="button" value="新增通道" onclick="location.href='addSalesInterFaceJump.html?said=${said}&salesno=${salesno }'"/>
						</c:if>
						&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="button" value="返回代理列表" onclick="location.href='salesList.html'"/>
					</div>
					<table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
						<c:if test="${!empty salesInterFaceList}">
							<tr>
								<th>序号</th>
								<th>通道</th>
								<th>费率%</th>
								<th>状态</th>
								<c:if test="${mui<3}">
								<th>操作</th>
								</c:if>
							</tr>
							<c:forEach items="${salesInterFaceList}" var="s" varStatus="st">
								<tr>
								<input type="hidden" value="${s.sid}" name="sid" id="sid${st.index}"/>
								<input type="hidden" value="${s.said}" name="mid" id="mid${st.index}"/>
								<input type="hidden" value="${s.salesno}" name="merchantno" id="merchantno${st.index}"/>
								<input type="hidden" value="${s.pid}" name="pid" id="pid${st.index}"/>
								<input type="hidden" value="${s.rate*100}" name="rate" id="rate${st.index}"/>
								<input type="hidden" value="${s.state}" name="state" id="state${st.index}"/>

									<td>${st.index+1}</td>
									<td>${map[s.pid]}</td>
									<td  id="tdrate${st.index}"><fmt:formatNumber type="float" value="${s.rate*100}" pattern="#.##" minFractionDigits="2"/></td>
									<c:if test="${s.state==0}">
										<td id="tdstate${st.index}">启用</td>
									</c:if>
									<c:if test="${s.state==1}">
										<td id="tdstate${st.index}">停用</td>
									</c:if>
									<c:if test="${s.state==2}">
										<td id="tdstate${st.index}">弃用</td>
									</c:if>
									<c:if test="${mui<4}">
									<td><input type="button" id="update${st.index}" value="修改" style="display: inline;" onclick="updaterow(${st.index});"/>
									<input type="button" id="save${st.index}" style="display: none;" value="提交" onclick="saverow(${st.index});"/>
									&nbsp;&nbsp;&nbsp;&nbsp;
									<input type="button" id="cancel${st.index}" style="display: none;" value="取消" onclick="cancelrow(${st.index});"/>
									</td>
									</c:if>
								</tr>
							</c:forEach>
						</c:if>
						<c:if test="${empty salesInterFaceList}">该代理没有开通通道</c:if>
					</table>
				</div>
			</div>
		</div>

        <!-- jQuery  -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/popper.min.js"></script>
			<script src="assets/js/bootstrap.min.js"></script>
			<script src="assets/js/metisMenu.min.js"></script>
			<script src="assets/js/waves.js"></script>
			<script src="assets/js/jquery.slimscroll.js"></script>
			<script src="plugins/waypoints/lib/jquery.waypoints.min.js"></script>
			<script src="plugins/counterup/jquery.counterup.min.js"></script>
			<!-- plugin js -->
			<script src="plugins/moment/moment.js"></script>
			<script src="plugins/bootstrap-timepicker/bootstrap-timepicker.js"></script>
			<script
				src="plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
			<script src="plugins/clockpicker/js/bootstrap-clockpicker.min.js"></script>
			<script src="plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
		
			<!-- Required datatable js -->
			<script src="plugins/datatables/jquery.dataTables.min.js"></script>
			<script src="plugins/datatables/dataTables.bootstrap4.min.js"></script>
			<!-- Buttons examples -->
			<script src="plugins/datatables/dataTables.buttons.min.js"></script>
			<script src="plugins/datatables/buttons.bootstrap4.min.js"></script>
			<script src="plugins/datatables/jszip.min.js"></script>
			<script src="plugins/datatables/pdfmake.min.js"></script>
			<script src="plugins/datatables/vfs_fonts.js"></script>
			<script src="plugins/datatables/buttons.html5.min.js"></script>
			<script src="plugins/datatables/buttons.print.min.js"></script>
			<!-- Responsive examples -->
			<script src="plugins/datatables/dataTables.responsive.min.js"></script>
			<script src="plugins/datatables/responsive.bootstrap4.min.js"></script>
		
			<!-- App js -->
			<script src="assets/js/jquery.core.js"></script>
			<script src="assets/js/jquery.app.js"></script>
			<!-- Init js -->
			<script src="assets/pages/jquery.form-pickers.init.js"></script>
       
    </body>
</html>