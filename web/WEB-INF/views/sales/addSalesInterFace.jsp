<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>管理后台</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <!-- DataTables -->
		<link href="plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
		<link href="plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
		<!-- Responsive datatable examples -->
		<link href="plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
		<!-- Plugins css -->
		<link href="plugins/bootstrap-timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
		<link href="plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css" rel="stylesheet">
		<link href="plugins/clockpicker/css/bootstrap-clockpicker.min.css" rel="stylesheet">
		<link href="plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
		<!-- App css -->
		<link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		<link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
		<link href="assets/css/metismenu.min.css" rel="stylesheet" type="text/css" />
		<link href="assets/css/style.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" type="text/css" href="css/pikaday.css" />
		<script type="text/javascript" src="js/pikaday.min.js"></script>
		<script src="assets/js/modernizr.min.js"></script>
        <script type="text/javascript">
			function checkinput() {
				if (document.getElementById("rate").value==""||document.getElementById("rate").value*1<0) {
					alert("请填写正确填写费率！");
					return false;
				}else{
					var jsonStr={};
					jsonStr.said= document.getElementById("said").value;
					jsonStr.salesno= document.getElementById("salesno").value;
					jsonStr.pid= document.getElementById("pid").value;
					jsonStr.rate= document.getElementById("rate").value;
					var jsonArrayFinal = JSON.stringify(jsonStr);//string类型
					$.ajax({
		  	     		type:"post",
		  	     		async:true,
		  	     		url:"addSalesInterFace.html",
		  	     		data:{mydata:jsonArrayFinal},
		  	     		dataType:"text",
		  	     		success:function(res) {
		  	     			if(res=="have") {
		  	     				alert("不要重复添加！");
		  	     			}else if(res=="yes") {
		  	     			    alert("添加成功！");
		  	     			    location.href="salesInterFaceList.html?said="+document.getElementById("said").value+"&salesno="+document.getElementById("salesno").value;
		  	     			}else{
		  	     				alert("请稍后再试！");
		  	     			}
		  	     		}
		  	     	})
				}
			}
		 </script>
    </head>


    <body>
        <div class="row">
            <div class="col-12">
                <h4 class="m-t-0 header-title">代理--${salesno}--新增通道</h4>
                <div class="p-20">
                    <div class="form-group row">
                        <label class="col-2 col-form-label">代理编号</label>
                        <div class="col-10">
                            <input type="hidden" value="${said}" name="said" id="said"/>
                            <input type="text" class="form-control" value="${salesno}" name="salesno" id="salesno" readonly="readonly"  autocomplete="off"/>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-2 col-form-label">通道选择</label>
                        <div class="col-10">
                            <select name="pid" id="pid"  class="selectpicker" data-style="btn-light btn-rounded">
                                <c:forEach items="${map}" var="s" varStatus="st">
                                    <option id="card" value="${s.key}" >${s.value}</option>
                                </c:forEach>
                             </select>
                        </div>
                    </div>
                    <div class="form-group row">
                         <label class="col-2 col-form-label">费率（百分之）</label>
                        <div class="col-10">
                            <input type="text" class="form-control" name="rate" id="rate" value="" placeholder="请输入代理费率" autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-5">

                        </div>
                        <div class="col-5">
                            <input  type="button"  value="确定" onclick="checkinput();"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <input type="button" value="返回" onclick="location.href='salesList.html'"/>
                        </div>

                    </div>
                </div>
            </div>
        </div>


        <!-- jQuery  -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/popper.min.js"></script>
			<script src="assets/js/bootstrap.min.js"></script>
			<script src="assets/js/metisMenu.min.js"></script>
			<script src="assets/js/waves.js"></script>
			<script src="assets/js/jquery.slimscroll.js"></script>
			<script src="plugins/waypoints/lib/jquery.waypoints.min.js"></script>
			<script src="plugins/counterup/jquery.counterup.min.js"></script>
			<!-- plugin js -->
			<script src="plugins/moment/moment.js"></script>
			<script src="plugins/bootstrap-timepicker/bootstrap-timepicker.js"></script>
			<script
				src="plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
			<script src="plugins/clockpicker/js/bootstrap-clockpicker.min.js"></script>
			<script src="plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
		
			<!-- Required datatable js -->
			<script src="plugins/datatables/jquery.dataTables.min.js"></script>
			<script src="plugins/datatables/dataTables.bootstrap4.min.js"></script>
			<!-- Buttons examples -->
			<script src="plugins/datatables/dataTables.buttons.min.js"></script>
			<script src="plugins/datatables/buttons.bootstrap4.min.js"></script>
			<script src="plugins/datatables/jszip.min.js"></script>
			<script src="plugins/datatables/pdfmake.min.js"></script>
			<script src="plugins/datatables/vfs_fonts.js"></script>
			<script src="plugins/datatables/buttons.html5.min.js"></script>
			<script src="plugins/datatables/buttons.print.min.js"></script>
			<!-- Responsive examples -->
			<script src="plugins/datatables/dataTables.responsive.min.js"></script>
			<script src="plugins/datatables/responsive.bootstrap4.min.js"></script>
		
			<!-- App js -->
			<script src="assets/js/jquery.core.js"></script>
			<script src="assets/js/jquery.app.js"></script>
			<!-- Init js -->
			<script src="assets/pages/jquery.form-pickers.init.js"></script>
       
    </body>
</html>