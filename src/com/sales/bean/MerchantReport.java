package com.sales.bean;

public class MerchantReport {

    private String cardNum;
    private String bank;
    private String displayname;
    private int countOrder=0;
    private int success=0;
    private int fail=0;
    private int orderAmount=0;
    private int okOrderAmount=0;
    private int receivableAmount=0;
    private int oksuccess=0;
    private int okmoney=0;
    private int rsuccess=0;
    private int rmoney=0;
    private int handlingfee=0;
    private String orderDay;
    private String merchantNo;
    private int card=0;
    private int cardmoney=0;
    private int okcard=0;
    private int okcardmoney=0;
    private int failcard=0;
    private int ocard=0;
    private int ocardmoney=0;
    private int rkcard=0;
    private int rkcardmoney=0;
    private int zfb=0;
    private int zfbmoney=0;
    private int okzfb=0;
    private int okzfbmoney=0;
    private int failzfb=0;
    private int ozfb=0;
    private int ozfbmoney=0;
    private int rkzfb=0;
    private int rkzfbmoney=0;
    private int wx=0;
    private int wxmoney=0;
    private int okwx=0;
    private int okwxmoney=0;
    private int failwx=0;
    private int owx=0;
    private int owxmoney=0;
    private int rkwx=0;
    private int rkwxmoney=0;
    private int paychanneltype=0;
    private int shandlingfee=0;
    private int income=0;

    public int getCard() {
        return card;
    }

    public void setCard(int card) {
        this.card = card;
    }

    public int getCardmoney() {
        return cardmoney;
    }

    public void setCardmoney(int cardmoney) {
        this.cardmoney = cardmoney;
    }

    public int getOkcard() {
        return okcard;
    }

    public void setOkcard(int okcard) {
        this.okcard = okcard;
    }

    public int getOkcardmoney() {
        return okcardmoney;
    }

    public void setOkcardmoney(int okcardmoney) {
        this.okcardmoney = okcardmoney;
    }

    public int getFailcard() {
        return failcard;
    }

    public void setFailcard(int failcard) {
        this.failcard = failcard;
    }

    public int getOcard() {
        return ocard;
    }

    public void setOcard(int ocard) {
        this.ocard = ocard;
    }

    public int getOcardmoney() {
        return ocardmoney;
    }

    public void setOcardmoney(int ocardmoney) {
        this.ocardmoney = ocardmoney;
    }

    public int getRkcard() {
        return rkcard;
    }

    public void setRkcard(int rkcard) {
        this.rkcard = rkcard;
    }

    public int getRkcardmoney() {
        return rkcardmoney;
    }

    public void setRkcardmoney(int rkcardmoney) {
        this.rkcardmoney = rkcardmoney;
    }

    public int getZfb() {
        return zfb;
    }

    public void setZfb(int zfb) {
        this.zfb = zfb;
    }

    public int getZfbmoney() {
        return zfbmoney;
    }

    public void setZfbmoney(int zfbmoney) {
        this.zfbmoney = zfbmoney;
    }

    public int getOkzfb() {
        return okzfb;
    }

    public void setOkzfb(int okzfb) {
        this.okzfb = okzfb;
    }

    public int getOkzfbmoney() {
        return okzfbmoney;
    }

    public void setOkzfbmoney(int okzfbmoney) {
        this.okzfbmoney = okzfbmoney;
    }

    public int getFailzfb() {
        return failzfb;
    }

    public void setFailzfb(int failzfb) {
        this.failzfb = failzfb;
    }

    public int getOzfb() {
        return ozfb;
    }

    public void setOzfb(int ozfb) {
        this.ozfb = ozfb;
    }

    public int getOzfbmoney() {
        return ozfbmoney;
    }

    public void setOzfbmoney(int ozfbmoney) {
        this.ozfbmoney = ozfbmoney;
    }

    public int getRkzfb() {
        return rkzfb;
    }

    public void setRkzfb(int rkzfb) {
        this.rkzfb = rkzfb;
    }

    public int getRkzfbmoney() {
        return rkzfbmoney;
    }

    public void setRkzfbmoney(int rkzfbmoney) {
        this.rkzfbmoney = rkzfbmoney;
    }

    public int getWx() {
        return wx;
    }

    public void setWx(int wx) {
        this.wx = wx;
    }

    public int getWxmoney() {
        return wxmoney;
    }

    public void setWxmoney(int wxmoney) {
        this.wxmoney = wxmoney;
    }

    public int getOkwx() {
        return okwx;
    }

    public void setOkwx(int okwx) {
        this.okwx = okwx;
    }

    public int getOkwxmoney() {
        return okwxmoney;
    }

    public void setOkwxmoney(int okwxmoney) {
        this.okwxmoney = okwxmoney;
    }

    public int getFailwx() {
        return failwx;
    }

    public void setFailwx(int failwx) {
        this.failwx = failwx;
    }

    public int getOwx() {
        return owx;
    }

    public void setOwx(int owx) {
        this.owx = owx;
    }

    public int getOwxmoney() {
        return owxmoney;
    }

    public void setOwxmoney(int owxmoney) {
        this.owxmoney = owxmoney;
    }

    public int getRkwx() {
        return rkwx;
    }

    public void setRkwx(int rkwx) {
        this.rkwx = rkwx;
    }

    public int getRkwxmoney() {
        return rkwxmoney;
    }

    public void setRkwxmoney(int rkwxmoney) {
        this.rkwxmoney = rkwxmoney;
    }



    public int getCountOrder() {
        return countOrder;
    }

    public void setCountOrder(int countOrder) {
        this.countOrder = countOrder;
    }

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public int getFail() {
        return fail;
    }

    public void setFail(int fail) {
        this.fail = fail;
    }

    public int getOrderAmount() {
        return orderAmount;
    }

    public void setOrderAmount(int orderAmount) {
        this.orderAmount = orderAmount;
    }

    public int getOkOrderAmount() {
        return okOrderAmount;
    }

    public void setOkOrderAmount(int okOrderAmount) {
        this.okOrderAmount = okOrderAmount;
    }

    public int getReceivableAmount() {
        return receivableAmount;
    }

    public void setReceivableAmount(int receivableAmount) {
        this.receivableAmount = receivableAmount;
    }

    public int getOksuccess() {
        return oksuccess;
    }

    public void setOksuccess(int oksuccess) {
        this.oksuccess = oksuccess;
    }

    public int getOkmoney() {
        return okmoney;
    }

    public void setOkmoney(int okmoney) {
        this.okmoney = okmoney;
    }

    public int getRsuccess() {
        return rsuccess;
    }

    public void setRsuccess(int rsuccess) {
        this.rsuccess = rsuccess;
    }

    public int getRmoney() {
        return rmoney;
    }

    public void setRmoney(int rmoney) {
        this.rmoney = rmoney;
    }

    public int getHandlingfee() {
        return handlingfee;
    }

    public void setHandlingfee(int handlingfee) {
        this.handlingfee = handlingfee;
    }



    public String getMerchantNo() {
        return merchantNo;
    }

    public void setMerchantNo(String merchantNo) {
        this.merchantNo = merchantNo;
    }

    public String getOrderDay() {
        return orderDay;
    }

    public void setOrderDay(String orderDay) {
        this.orderDay = orderDay;
    }

    public int getPaychanneltype() {
        return paychanneltype;
    }

    public void setPaychanneltype(int paychanneltype) {
        this.paychanneltype = paychanneltype;
    }

    public int getShandlingfee() {
        return shandlingfee;
    }

    public void setShandlingfee(int shandlingfee) {
        this.shandlingfee = shandlingfee;
    }

    public int getIncome() {
        return income;
    }

    public void setIncome(int income) {
        this.income = income;
    }

    public String getCardNum() {
        return cardNum;
    }

    public void setCardNum(String cardNum) {
        this.cardNum = cardNum;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getDisplayname() {
        return displayname;
    }

    public void setDisplayname(String displayname) {
        this.displayname = displayname;
    }
}

