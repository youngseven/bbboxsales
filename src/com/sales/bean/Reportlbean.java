package com.sales.bean;

public class Reportlbean {
	
 	private int sum=0;

    private int orderAmount=0;

    private int handlingfee=0;

    private int success=0;

    private int oksuccess=0;

	private int rsuccess=0;

    private int fail=0;

    private int okmoney=0;

    private int rmoney=0;

    private String datetime;

	public int getSum() {
		return sum;
	}

	public void setSum(int sum) {
		this.sum = sum;
	}

	

	public int getOrderAmount() {
		return orderAmount;
	}

	public void setOrderAmount(int orderAmount) {
		this.orderAmount = orderAmount;
	}

	public int getHandlingfee() {
		return handlingfee;
	}

	public void setHandlingfee(int handlingfee) {
		this.handlingfee = handlingfee;
	}

	public int getSuccess() {
		return success;
	}

	public void setSuccess(int success) {
		this.success = success;
	}

	public int getOksuccess() {
		return oksuccess;
	}

	public void setOksuccess(int oksuccess) {
		this.oksuccess = oksuccess;
	}

	public int getRsuccess() {
		return rsuccess;
	}

	public void setRsuccess(int rsuccess) {
		this.rsuccess = rsuccess;
	}

	public int getFail() {
		return fail;
	}

	public void setFail(int fail) {
		this.fail = fail;
	}

	public int getOkmoney() {
		return okmoney;
	}

	public void setOkmoney(int okmoney) {
		this.okmoney = okmoney;
	}

	public int getRmoney() {
		return rmoney;
	}

	public void setRmoney(int rmoney) {
		this.rmoney = rmoney;
	}

	public String getDatetime() {
		return datetime;
	}

	public void setDatetime(String datetime) {
		this.datetime = datetime;
	}

	

}
