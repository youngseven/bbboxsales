package com.sales.service;

import com.alibaba.fastjson.JSONObject;
import com.sales.bean.Constant;

import com.sales.dao.MerchantMapper;
import com.sales.dao.SalesInterFaceMapper;
import com.sales.dao.SalesMapper;
import com.sales.model.Merchant;
import com.sales.model.Sales;
import com.sales.model.SalesInterFace;
import com.sales.util.AESUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;


@Service
@Transactional
public class SalesService {

	/*
	 * 约定大于配置
	 */
	@Resource
	private SalesMapper salesMapper;
	@Resource
	private SalesInterFaceMapper salesInterFaceMapper;
	@Resource
	private MerchantMapper merchantMapper;
	String aesKey = "de66bdf43eb24f9f8d4823e87aa4b49b";



	public Sales login(String cloginname, String cpassword) {
		// TODO Auto-generated method stub
		Sales sales = salesMapper.login(cloginname,cpassword);
		JSONObject.toJSONString(sales);
		if(sales.getSalesname()!=null){
			sales =debase(sales);
		}
		return sales;
	}

	public Sales debase(Sales sales) {
		// TODO Auto-generated method stub
		try {

			sales.setSloginname(new String(AESUtil.AESDncode2(sales.getSloginname(), Constant.aesKey)));
			sales.setSalesname(new String(AESUtil.AESDncode2(sales.getSalesname(),  Constant.aesKey)));
			sales.setSpassword(new String(AESUtil.AESDncode2(sales.getSpassword(),  Constant.aesKey)));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return sales;
	}

    public int changePwd(String loginname, String oldpwd, String newpwd) {
		int i = salesMapper.changePwd(loginname,oldpwd,newpwd);
		return i;
    }






	public List<SalesInterFace> querysalesInterFaceList(String salesno) {
		List<SalesInterFace> salesInterFaceList = salesInterFaceMapper.querySalesInterFaceList(salesno);
		return salesInterFaceList;
	}

    public Sales querySales(String salesno) {
		Sales sales = salesMapper.querySales(salesno);
		sales.setSalesname(AESUtil.AESDncode2(sales.getSalesname(), Constant.aesKey));
		sales.setSloginname(AESUtil.AESDncode2(sales.getSloginname(), Constant.aesKey));
		sales.setSpassword(AESUtil.AESDncode2(sales.getSpassword(), Constant.aesKey));
		return sales;
    }

	public List<Merchant> queryMerchantList(String salesno) {
		List<Merchant> merchantList = merchantMapper.queryMerchantList(salesno);
		return merchantList;
	}

	public int modifybalance(String salesno, Integer applymoney) {
		int i = salesMapper.modifybalance(salesno,applymoney);
		return i;
	}
}
