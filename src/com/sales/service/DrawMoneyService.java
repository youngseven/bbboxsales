package com.sales.service;


import com.sales.dao.DrawMoneyMapper;
import com.sales.model.DrawMoney;
import com.sales.util.AESUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;


@Service
@Transactional
public class DrawMoneyService {

	/*
	 * 约定大于配置
	 */
	@Resource
	private DrawMoneyMapper drawMoneyMapper;

	String aesKey = "de66bdf43eb24f9f8d4823e87aa4b49b";


	public int insert(DrawMoney drawMoney) {
		// TODO Auto-generated method stub
		int i =drawMoneyMapper.insert(drawMoney);
		return i;
	}



	
	public List<DrawMoney> debase(List<DrawMoney> drawMoneyList) {
		// TODO Auto-generated method stub
		for(int i = 0; i<drawMoneyList.size();i++){
			DrawMoney drawMoney = drawMoneyList.get(i);
			try {
				
				drawMoney.setTurninbank(new String(AESUtil.AESDncode2(drawMoney.getTurninbank(), aesKey)));
				drawMoney.setTurnincardnumber(new String(AESUtil.AESDncode2(drawMoney.getTurnincardnumber(), aesKey)));
				drawMoney.setTurninname(new String(AESUtil.AESDncode2(drawMoney.getTurninname(), aesKey)));
				drawMoney.setTurnoutbank(new String(AESUtil.AESDncode2(drawMoney.getTurnoutbank(), aesKey)));
				drawMoney.setTurnoutbcardnumber(new String(AESUtil.AESDncode2(drawMoney.getTurnoutbcardnumber(), aesKey)));
				drawMoney.setTurnoutname(new String(AESUtil.AESDncode2(drawMoney.getTurnoutname(), aesKey)));
				if(drawMoney.getApplynname()!=null){
					drawMoney.setApplynname(new String(AESUtil.AESDncode2(drawMoney.getApplynname(), aesKey)));
				}
				if(drawMoney.getExaminename()!=null){
					drawMoney.setExaminename(new String(AESUtil.AESDncode2(drawMoney.getExaminename(), aesKey)));
				}
				if(drawMoney.getEndname()!=null){
					drawMoney.setEndname(new String(AESUtil.AESDncode2(drawMoney.getEndname(), aesKey)));
				}
				
				drawMoneyList.remove(i);
				drawMoneyList.add(i, drawMoney);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return drawMoneyList;
	}
	
	public DrawMoney debase2(DrawMoney drawMoney) {
		// TODO Auto-generated method stub
		try {
			
			drawMoney.setTurninbank(new String(AESUtil.AESDncode2(drawMoney.getTurninbank(), aesKey)));
			drawMoney.setTurnincardnumber(new String(AESUtil.AESDncode2(drawMoney.getTurnincardnumber(), aesKey)));
			drawMoney.setTurninname(new String(AESUtil.AESDncode2(drawMoney.getTurninname(), aesKey)));
			drawMoney.setTurnoutbank(new String(AESUtil.AESDncode2(drawMoney.getTurnoutbank(), aesKey)));
			drawMoney.setTurnoutbcardnumber(new String(AESUtil.AESDncode2(drawMoney.getTurnoutbcardnumber(), aesKey)));
			drawMoney.setTurnoutname(new String(AESUtil.AESDncode2(drawMoney.getTurnoutname(), aesKey)));
			if(drawMoney.getApplynname()!=null){
				drawMoney.setApplynname(new String(AESUtil.AESDncode2(drawMoney.getApplynname(), aesKey)));
			}
			if(drawMoney.getExaminename()!=null){
				drawMoney.setExaminename(new String(AESUtil.AESDncode2(drawMoney.getExaminename(), aesKey)));
			}
			if(drawMoney.getEndname()!=null){
				drawMoney.setEndname(new String(AESUtil.AESDncode2(drawMoney.getEndname(), aesKey)));
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return drawMoney;
	}





	public List<DrawMoney> queryRecordListTime(String begintime, String endtime, String salesno) {
		// TODO Auto-generated method stub
		List<DrawMoney> drawMoneyList =drawMoneyMapper.queryRecordListTime(begintime,endtime,salesno);
		List<DrawMoney> findAllList2=debase(drawMoneyList);
		return findAllList2;
	}









	public List<DrawMoney> queryRecordListDrawmoneyno(String drawMoneyno, String salesno) {
		// TODO Auto-generated method stub
		List<DrawMoney> drawMoneyList =drawMoneyMapper.queryRecordListDrawmoneyno(drawMoneyno,salesno);
		List<DrawMoney> findAllList2=debase(drawMoneyList);
		return findAllList2;
	}


	public List<DrawMoney> queryRecordListstateTime(int state, String begintime, String endtime, String salesno) {
		// TODO Auto-generated method stub
		List<DrawMoney> drawMoneyList =drawMoneyMapper.queryRecordListstateTime(state,begintime,endtime,salesno);
		List<DrawMoney> findAllList2=debase(drawMoneyList);
		return findAllList2;
	}



	public DrawMoney queryDrawMoney(int dmid) {
		// TODO Auto-generated method stub
		DrawMoney drawMoney = drawMoneyMapper.queryDrawMoney(dmid);
		DrawMoney drawMoney2=debase2(drawMoney);
		return drawMoney2;
	}




}
