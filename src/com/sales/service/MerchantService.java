package com.sales.service;

import com.sales.bean.Constant;
import com.sales.dao.InterFaceMapper;
import com.sales.dao.MerchantMapper;
import com.sales.model.InterFace;
import com.sales.model.Merchant;
import com.sales.util.AESUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;


@Service
@Transactional
public class MerchantService {

	/*
	 * 约定大于配置
	 */
	@Resource
	private MerchantMapper merchantMapper;

	@Resource
	private InterFaceMapper interFaceMapper;





	

//	public Merchant detailsmerchant(String merchantno) {
//		// TODO Auto-generated method stub
//		Merchant merchant = merchantMapper.detailsmerchant(merchantno);
//		return merchant;
//	}
//
//
//	public List<InterFace> queryInterFace(String merchantno) {
//		// TODO Auto-generated method stub
//		List<InterFace> interFaceList = interFaceMapper.queryInterFace(merchantno);
//		return interFaceList;
//	}





	public Merchant detailsmerchant(String merchantno) {
		Merchant merchant = merchantMapper.detailsmerchant(merchantno);
		return merchant;
	}




	public List<InterFace> queryInterFace(String merchantno) {
		// TODO Auto-generated method stub
		List<InterFace> interFaceList = interFaceMapper.queryInterFace(merchantno);
		return interFaceList;
	}


	public Merchant queryMerchant(String merchantno) {
		// TODO Auto-generated method stub
		Merchant merchant = merchantMapper.detailsmerchant(merchantno);
		//List<Merchant> merchantList2=debase(merchantList);
		return merchant;
	}


	public List<Merchant> queryMerchantList(String salesno) {
		List<Merchant> merchantList = merchantMapper.queryMerchantList(salesno);
		return merchantList;
	}
}
