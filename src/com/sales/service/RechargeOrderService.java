package com.sales.service;

import com.sales.bean.MerchantReport;
import com.sales.dao.SalesMapper;
import com.sales.dao.RechargeOrderMapper;
import com.sales.model.RechargeOrder;
import com.sales.util.AESUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.text.DecimalFormat;
import java.util.List;


@Service
@Transactional
public class RechargeOrderService {

	/*
	 * 约定大于配置
	 */
	@Resource
	private RechargeOrderMapper rechargeOrderMapper;
	@Resource
	private SalesMapper salesMapper;


	DecimalFormat df = new DecimalFormat("#.00");
	String aesKey = "de66bdf43eb24f9f8d4823e87aa4b49b";

	public List<RechargeOrder> queryRechargeOrder(String orderno) {
		// TODO Auto-generated method stub
		List<RechargeOrder> rechargeOrderlist = rechargeOrderMapper.queryRechargeOrder(orderno);
		List<RechargeOrder> findAllList2 = debase(rechargeOrderlist);
		return findAllList2;
	}

	public List<RechargeOrder> queryRechargeOrderSys(String sysorderno) {
		// TODO Auto-generated method stub
		List<RechargeOrder> rechargeOrderlist = rechargeOrderMapper.queryRechargeOrderSys(sysorderno);
		List<RechargeOrder> findAllList2 = debase(rechargeOrderlist);
		return findAllList2;
	}


	public List<RechargeOrder> debase(List<RechargeOrder> findAllList) {
		// TODO Auto-generated method stub
		for (int i = 0; i < findAllList.size(); i++) {
			RechargeOrder RechargeOrder = findAllList.get(i);
			try {
				//RechargeOrder.setPayee(new String(AESUtil.AESDncode2(RechargeOrder.getPayee(),aesKey)));
				if (RechargeOrder.getDisplayname() != null) {
					RechargeOrder.setDisplayname(AESUtil.AESDncode2(RechargeOrder.getDisplayname(), aesKey));
				}
				RechargeOrder.setRate(Double.parseDouble(df.format(RechargeOrder.getRate() * 100)));
				findAllList.remove(i);
				findAllList.add(i, RechargeOrder);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return findAllList;
	}





	public List<RechargeOrder> queryRechargeOrderThere2(String begintime, String endtime,String merchantno, String salesno,  int startRow) {
		// TODO Auto-generated method stub
		List<RechargeOrder> aliOrderlist = rechargeOrderMapper.queryRechargeOrderThere2(begintime, endtime, merchantno,salesno,startRow);
		List<RechargeOrder> findAllList2 = debase(aliOrderlist);
		return findAllList2;
	}





	public MerchantReport queryRechargeOrderReport(String begintime, String endtime, String salesno) {
		MerchantReport merchantReport = rechargeOrderMapper.queryRechargeOrderReport(begintime, endtime, salesno);
		return merchantReport;
	}

	public List<MerchantReport> qureyreport(String begintime, String endtime, String salesno) {
		List<MerchantReport> merchantReportList = rechargeOrderMapper.qureyreport(begintime,endtime,salesno);
//		List<CardReport> findAllList2=debase2(merchantReportList);
		return merchantReportList;
	}


	public int queryRechargeOrderCount(String begintime, String endtime,String salesno) {
		int count = rechargeOrderMapper.queryRechargeOrderCount(begintime,endtime,salesno);
		return count;
	}
	public List<RechargeOrder> queryRechargeOrderTwo(String begintime, String endtime, String salesno,int startRow){
		// TODO Auto-generated method stub
		List<RechargeOrder> rechargeOrderlist = rechargeOrderMapper.queryRechargeOrderTwo(begintime,endtime,salesno,startRow);
		List<RechargeOrder> findAllList2=debase(rechargeOrderlist);
		return findAllList2;
	}


	public int queryRechargeOrderThereCount2(String begintime, String endtime, String merchantno, String salesno) {
		int count = rechargeOrderMapper.queryRechargeOrderThereCount2(begintime,endtime,merchantno);
		return count;
	}

	public int queryRechargeOrderThereCount(String begintime, String endtime,  Integer ordertype,String salesno) {
		int count = rechargeOrderMapper.queryRechargeOrderThereCount(begintime,endtime,ordertype,salesno);
		return count;
	}
	public List<RechargeOrder> queryRechargeOrderThere(String begintime, String endtime,int ordertype, String salesno,int startRow) {
		// TODO Auto-generated method stub
		List<RechargeOrder> rechargeOrderlist = rechargeOrderMapper.queryRechargeOrderThere(begintime,endtime,salesno,ordertype,startRow);
		List<RechargeOrder> findAllList2=debase(rechargeOrderlist);
		return findAllList2;
	}
	public int queryRechargeOrderFourCount(String begintime, String endtime, Integer ordertype, String merchantno, String salesno) {
		int count = rechargeOrderMapper.queryRechargeOrderFourCount(begintime,endtime,ordertype,merchantno);
		return count;
	}
	public List<RechargeOrder> queryRechargeOrderFour(String begintime, String endtime, Integer ordertype,String merchantno, String salesno, int startRow) {
		// TODO Auto-generated method stub
		List<RechargeOrder> aliOrderlist = rechargeOrderMapper.queryRechargeOrderFour(begintime, endtime, ordertype, merchantno,salesno,startRow);
		List<RechargeOrder> findAllList2 = debase(aliOrderlist);
		return findAllList2;
	}


	public List<MerchantReport> merchantReport(String begintime, String endtime, String salesno) {
		List<MerchantReport> merchantReportList = rechargeOrderMapper.merchantReport(begintime,endtime,salesno);
		return merchantReportList;
	}

    public List<MerchantReport> transferCardReport(String begintime, String endtime, String salesno) {
		List<MerchantReport> merchantReportList = rechargeOrderMapper.transferCardReport(begintime,endtime,salesno);
		return merchantReportList;
    }

	public List<MerchantReport> zfbReport(String begintime, String endtime, String salesno) {
		List<MerchantReport> merchantReportList = rechargeOrderMapper.zfbReport(begintime,endtime,salesno);
		return merchantReportList;
	}

	public List<MerchantReport> wxReport(String begintime, String endtime, String salesno) {
		List<MerchantReport> merchantReportList = rechargeOrderMapper.wxReport(begintime,endtime,salesno);
		return merchantReportList;
	}

	public List<MerchantReport> modeReport(String begintime, String endtime, String salesno) {
		List<MerchantReport> merchantReportList = rechargeOrderMapper.modeReport(begintime,endtime,salesno);
		return merchantReportList;
	}
}
