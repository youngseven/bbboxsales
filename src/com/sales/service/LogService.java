package com.sales.service;


import com.sales.dao.LogTableMapper;
import com.sales.model.LogTable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


@Service
@Transactional
public class LogService {

	/*
	 * 约定大于配置
	 */
	@Resource
	private LogTableMapper logTableMapper;


	public void insert(LogTable logTable) {
		logTableMapper.insert(logTable);
	}
}
