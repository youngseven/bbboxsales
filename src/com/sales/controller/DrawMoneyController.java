package com.sales.controller;


import com.sales.bean.Constant;
import com.sales.model.*;
import com.sales.service.DrawMoneyService;
import com.sales.service.LogService;
import com.sales.service.SalesService;
import com.sales.util.AESUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;


/**
 * 二维码
 */
@Controller
public class DrawMoneyController {


	@Autowired
	private DrawMoneyService drawMoneyService;
	@Autowired
	private LogService logService;
	@Autowired
	private SalesService salesService;
	
	String aesKey = "de66bdf43eb24f9f8d4823e87aa4b49b";
	
	

	
	

	/**
	 * 页面跳转
	 */
	@RequestMapping(value="DrawMoneyJump.html",produces="application/json;charset=utf-8")
	@ResponseBody
	public ModelAndView  DrawMoneyJump(String salesno,HttpServletRequest request) {
		ModelAndView mv = new ModelAndView();
		LogTable logTable =  new LogTable();
		Sales mui = (Sales)request.getSession().getAttribute("mui");
		logTable.setUsername(mui.getSloginname());
		Sales sales = salesService.querySales(mui.getSalesno());
		mv.addObject("sales",sales);
		logTable.setActiontime(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
		logTable.setActionevent("进入结款界面");
		logService.insert(logTable);
		mv.setViewName("draw/applyDrawMoney");
        return mv;
	}
	/**
	 * 页面跳转
	 */
	@RequestMapping(value="applyDrawMoney.html",produces="application/json;charset=utf-8")
	@ResponseBody
	public ModelAndView  addBankCard(DrawMoney drawMoney,int avbalance,HttpServletRequest request) {
		ModelAndView mv = new ModelAndView();
		LogTable logTable =  new LogTable();
		Sales mui = (Sales)request.getSession().getAttribute("mui");
		logTable.setUsername(mui.getSloginname());
		logTable.setActiontime(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));

		try{
			if(drawMoney.getApplymoney()>avbalance){
				mv.addObject("error","提款金额不能大于可用金额！！！");
			}else{
				drawMoney.setRate(0.0);
				drawMoney.setDrawmoneyno("sl"+LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmssSSS")));
				drawMoney.setServicecharge(0);
				drawMoney.setActualmoney(drawMoney.getApplymoney()-drawMoney.getServicecharge());
				drawMoney.setTurninbank(AESUtil.AESEncode2(drawMoney.getTurninbank(), aesKey));
				drawMoney.setTurnincardnumber(AESUtil.AESEncode2(drawMoney.getTurnincardnumber(), aesKey));
				drawMoney.setTurninname(AESUtil.AESEncode2(drawMoney.getTurninname(), aesKey));
				drawMoney.setTurnoutbank(AESUtil.AESEncode2(drawMoney.getTurnoutbank(), aesKey));
				drawMoney.setTurnoutbcardnumber(AESUtil.AESEncode2(drawMoney.getTurnoutbcardnumber(), aesKey));
				drawMoney.setTurnoutname(AESUtil.AESEncode2(drawMoney.getTurnoutname(), aesKey));
				drawMoney.setApplytime(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
				drawMoney.setApplynname(AESUtil.AESEncode2(mui.getSloginname(), aesKey));
				drawMoney.setActualamount(drawMoney.getActualmoney());
				drawMoney.setState(0);
				int  i = drawMoneyService.insert(drawMoney);
				if(i==1) {
					int  j = salesService.modifybalance(drawMoney.getSalesno(),drawMoney.getApplymoney());
					logTable.setActionevent("结款提交成功");
					logService.insert(logTable);
					mv.addObject("error","代理结款提交成功！！");
				} else {
					logTable.setActionevent("结款提交失败");
					logService.insert(logTable);
					mv.addObject("error","代理结款提交失败！！");
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		Sales sales = salesService.querySales(mui.getSalesno());
		List<SalesInterFace> salesInterFaceList = salesService.querysalesInterFaceList(mui.getSalesno());
		List<Merchant> merchantList = salesService.queryMerchantList(mui.getSalesno());
		mv.addObject("sales", sales);
		mv.addObject("salesInterFaceList", salesInterFaceList);
		mv.addObject("merchantList", merchantList);
		mv.addObject("map", Constant.map);
		mv.setViewName("sales/sales");
		return mv;
	}
	

	/**
	 * 结款查询跳转
	 */
	@RequestMapping(value="drawMoneyListJump.html",produces="application/json;charset=utf-8")
	@ResponseBody
	public ModelAndView drawMoneyListJump(HttpServletRequest request) {
		ModelAndView mv = new ModelAndView();
		LogTable logTable =  new LogTable();
		Sales mui = (Sales)request.getSession().getAttribute("mui");
		logTable.setUsername(mui.getSloginname());
		logTable.setActiontime(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
		logTable.setActionevent("查看代理结算信息");
		logService.insert(logTable);
		mv.setViewName("draw/drawMoneyList");
        return mv;
	}
	
	/**
	 * 结款查询
	 */
	@RequestMapping(value="drawMoneyList.html",produces="application/json;charset=utf-8")
	@ResponseBody
	public ModelAndView drawMoneyList(String begintime, String endtime, String drawMoneyno, int state, HttpServletRequest request) {
		ModelAndView mv = new ModelAndView();
		LogTable logTable =  new LogTable();
		Sales mui = (Sales)request.getSession().getAttribute("mui");
		logTable.setUsername(mui.getSloginname());
		String salesno=mui.getSalesno();
		List<DrawMoney> drawMoneyList  = null;
		if(!begintime.equals("")&&!endtime.equals("")){
			begintime = begintime+" 00:00:00";
			endtime = endtime+" 23:59:59";
		}
		if(!drawMoneyno.equals("")){
			drawMoneyList = drawMoneyService.queryRecordListDrawmoneyno(drawMoneyno,salesno);
		}else{
			if(state==100){
				drawMoneyList =drawMoneyService.queryRecordListTime(begintime,endtime,salesno);
			}else{
				drawMoneyList = drawMoneyService.queryRecordListstateTime(state,begintime,endtime,salesno);
			}
		}


		logTable.setActiontime(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
		logTable.setActionevent("查询"+begintime+"至"+endtime+"代理结算信息");
		logService.insert(logTable);
		mv.addObject("drawMoneyList",drawMoneyList);
		mv.setViewName("draw/drawMoneyList");
        return mv;
	}
	

	/**
	 * 详情
	 */
	@RequestMapping(value="drawMoney.html",produces="application/json;charset=utf-8")
	@ResponseBody
	public ModelAndView drawMoney(int dmid, HttpServletRequest request) {
		ModelAndView mv = new ModelAndView();
		DrawMoney drawMoney = drawMoneyService.queryDrawMoney(dmid);
		mv.addObject("drawMoney",drawMoney);
		LogTable logTable =  new LogTable();
		Sales mui = (Sales)request.getSession().getAttribute("mui");
		logTable.setUsername(mui.getSloginname());
		logTable.setActiontime(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
		logTable.setActionevent("查询"+drawMoney.getDrawmoneyno()+"代理结算信息");
		logService.insert(logTable);
		mv.setViewName("draw/drawMoney");
        return mv;
	}
	
	
	
}
