package com.sales.controller;


import com.sales.bean.Constant;
import com.sales.model.*;
import com.sales.service.LogService;
import com.sales.service.MerchantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;


@Controller
public class MerchantController {

	@Autowired
	private MerchantService merchantService;

	@Autowired
	private LogService logService;
	
	DecimalFormat df = new DecimalFormat("#.00");

	String aesKey = "de66bdf43eb24f9f8d4823e87aa4b49b";


	/**
	 * 商户列表
	 */
	@RequestMapping(value="salesMerchantlist.html",produces="application/json;charset=utf-8")
	@ResponseBody
	public ModelAndView  querySalesMerchantlist(HttpServletRequest request,int said, String salesno) {
		ModelAndView mv = new ModelAndView();

		LogTable logTable =  new LogTable();
		Sales mui = (Sales)request.getSession().getAttribute("mui");
		List<Merchant> merchantlist = merchantService.queryMerchantList(mui.getSalesno());
		logTable.setUsername(mui.getSloginname());
		logTable.setActiontime(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
		logTable.setActionevent("打开商户列表");
		logService.insert(logTable);
		mv.addObject("merchantlist",merchantlist);
		mv.setViewName("merchant/merchantlist");
		return mv;
	}
	


	
	/**
	 * 商户详情
	 */
	@RequestMapping(value="detailsmerchant.html",produces="application/json;charset=utf-8")
	@ResponseBody
	public ModelAndView detailsmerchant(HttpServletRequest request,String merchantno) {
		Sales mui = (Sales)request.getSession().getAttribute("mui");
		ModelAndView mv = new ModelAndView();
		Merchant merchant = merchantService.detailsmerchant(merchantno);
//		merchant.setBalance(merchant.getBalance()/100);
//		merchant.setAvbalance(merchant.getAvbalance()/100);
//		merchant.setLockbalance(merchant.getLockbalance()/100);
//		merchant.setUnbalance(merchant.getUnbalance()/100);
		merchant.setMquota(merchant.getMquota()/100);
		LogTable logTable =  new LogTable();
		logTable.setUsername(mui.getSloginname());
		logTable.setActiontime(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
		logTable.setActionevent("查看商户详情"+merchant.getMerchantno());
		logService.insert(logTable);
		mv.addObject("merchant",merchant);
        mv.setViewName("merchant/detailsmerchant");
        return mv;
	}




	/**
	 * 商户余额修改跳转
	 */
	@RequestMapping(value="balanceJump.html",produces="application/json;charset=utf-8")
	@ResponseBody
	public ModelAndView balanceJump(HttpServletRequest request) {
		ModelAndView mv = new ModelAndView();
		Sales mui = (Sales)request.getSession().getAttribute("mui");
		Merchant merchant = merchantService.queryMerchant(mui.getSloginname());
		mv.addObject("merchant",merchant);
		LogTable logTable =  new LogTable();
		logTable.setUsername(mui.getSloginname());
		logTable.setActiontime(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
		logTable.setActionevent("商户余额跳转");
		logService.insert(logTable);

		mv.setViewName("merchant/balancelist");
		return mv;
	}



	
	/**
	 * 商户通道跳转
	 */
	@RequestMapping(value="merchantInterFace.html",produces="application/json;charset=utf-8")
	@ResponseBody
	public ModelAndView merchantInterFace(String merchantno, int mid, HttpServletRequest request) {
		ModelAndView mv = new ModelAndView();
		List<InterFace> interFaceList = merchantService.queryInterFace(merchantno);;
		mv.addObject("interFaceList", interFaceList);
		mv.addObject("merchantno", merchantno);
		mv.addObject("mid",mid);
		mv.addObject("map", Constant.map);
		LogTable logTable =  new LogTable();
		Sales mui = (Sales)request.getSession().getAttribute("mui");
		logTable.setUsername(mui.getSloginname());
		logTable.setActiontime(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
		logTable.setActionevent("查看"+merchantno+"商户通道");
		logService.insert(logTable);
        mv.setViewName("merchant/interFaceList");
		
        return mv;
	}


}
