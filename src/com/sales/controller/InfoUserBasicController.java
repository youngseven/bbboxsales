package com.sales.controller;

import com.google.gson.Gson;
import com.sales.bean.MerchantReport;
import com.sales.method.ApiMethod;
import com.sales.method.InitDataListener;
import com.sales.model.*;
import com.sales.service.LogService;
import com.sales.service.MerchantService;
import com.sales.service.RechargeOrderService;
import com.sales.service.SalesService;
import com.sales.util.AESUtil;
import net.sf.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.*;


@Controller
@SessionAttributes({"mui"})
public class InfoUserBasicController {

	@Autowired
	private RechargeOrderService rechargeOrderService;
	@Autowired
	private MerchantService merchantService;
	@Autowired
	private SalesService salesService;
	@Autowired
	private LogService logService;
	String aesKey = "de66bdf43eb24f9f8d4823e87aa4b49b";



	/**
	 * 用户登录
	 *  使用ajax，所以要用String
	 * @throws UnsupportedEncodingException
	 */
	@RequestMapping(value="userLogin.html",produces="application/json;charset=utf-8")
	@ResponseBody
	public ModelAndView userLogin(ModelMap map, String cloginname, String cpassword , String authCode , HttpServletRequest request) {
		ModelAndView mv = new ModelAndView();
		ApiMethod tm = new ApiMethod();
		String ip = tm.giveip(request);
//		String ip = "aa";
//		String mac = tm.getLocalMac(ip);
//		System.out.println(ip+"+++"+mac);
		Sales mui = new Sales();
		boolean loginState=false;
		if(cloginname.equals("")||cpassword.equals("")){
			loginState=false;
		}else{
			String valueCode=(String)request.getSession().getAttribute("strCode");
//			Map map=new HashMap();
			if(valueCode!=null){
				if(valueCode.equals(authCode)) {
					cloginname = AESUtil.AESEncode2(cloginname, aesKey);
					cpassword = AESUtil.AESEncode2(cpassword, aesKey);
					mui = salesService.login(cloginname, cpassword);
					if (mui != null) {
						loginState = true;
					} else {
						loginState = false;
					}
				}else{
					loginState=false;
				}
			}else{
				loginState=false;
			}
		}
		if(loginState==true){
//			Mangement mui =mangement;
			List menuList1 = new ArrayList();
			for(int i = 0 ;i<InitDataListener.menuList.size();i++){
				Map menuMap = new LinkedHashMap();
				menuMap.put("icon","fa-cubes");
				menuMap.put("title",InitDataListener.menuList.get(i).getMenu());
				if(!InitDataListener.menuList.get(i).getMenuurl().equals("#")) {
					menuMap.put("href", InitDataListener.menuList.get(i).getMenuurl());
				}
				menuMap.put("spread",false);
				List menuList2 = new ArrayList();
				for (int j=0;j<InitDataListener.menu2List.size();j++) {
					if (InitDataListener.menu2List.get(j).getUpid() == InitDataListener.menuList.get(i).getSuid()) {
						Map menuMap2 = new LinkedHashMap();
						menuMap2.put("icon", "&#xe63c;");
						menuMap2.put("title", InitDataListener.menu2List.get(j).getMenu());
						if(!InitDataListener.menu2List.get(j).getMenuurl().equals("#")) {
							menuMap2.put("href", InitDataListener.menu2List.get(j).getMenuurl());
						}
						menuList2.add(menuMap2);
					}
				}
				if(menuList2.size()>0){
					menuMap.put("children",menuList2);
				}
				menuList1.add(menuMap);
			}

			JSONArray json = JSONArray.fromObject(menuList1);
			System.out.println(json);
			mv.addObject("menuJson", json);
			map.addAttribute("mui",mui);
	        mv.setViewName("user/index");
			LogTable logTable =  new LogTable();
			logTable.setUsername(mui.getSloginname());
			logTable.setActiontime(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
			logTable.setActionevent("登录");
			logService.insert(logTable);
		}else{
			mv.addObject("error","登录失败");
			mv.setViewName("user/login");
		}
		return mv;
	}



	/**
	 * 登录页面跳转
	 */
	@RequestMapping(value="homeJump.html",produces="application/json;charset=utf-8")
	@ResponseBody
	public ModelAndView userLoginJump(HttpSession httpSession) {
		ModelAndView mv = new ModelAndView();
		String datetime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		String begintime = datetime+" 00:00:00";
		String endtime = datetime+" 23:59:59";
		Sales mui = new Sales();
		mui = (Sales)httpSession.getAttribute("mui");
		MerchantReport merchantReport =rechargeOrderService.queryRechargeOrderReport(begintime,endtime,mui.getSalesno());
		mv.addObject("merchantReport",merchantReport);
		mv.setViewName("user/home");
		LogTable logTable =  new LogTable();

		logTable.setUsername(mui.getSloginname());
		logTable.setActiontime(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
		logTable.setActionevent("打开主页");
		logService.insert(logTable);
        return mv;
	}
	/**
	 * 页面跳转
	 */
	@RequestMapping(value="welcome.html",produces="application/json;charset=utf-8")
	@ResponseBody
	public ModelAndView welcome(HttpServletRequest request) {

		ModelAndView mv = new ModelAndView();
		ApiMethod tm = new ApiMethod();
		String ip = tm.giveip(request);
//		if(ip.equals("223.104.3.25")){
			mv.addObject("","");
	        mv.setViewName("user/login");
//		}else{
//			mv.addObject("error","非法登录");
//	        mv.setViewName("user/error");
//		}
        return mv;
	}

	/**
	 * 页面跳转
	 */
	@RequestMapping(value="notOpened.html",produces="application/json;charset=utf-8")
	@ResponseBody
	public ModelAndView notOpened(HttpServletRequest request) {

		ModelAndView mv = new ModelAndView();

//		if(ip.equals("223.104.3.25")){
		mv.addObject("","");
		mv.setViewName("user/notOpened");
//		}else{
//			mv.addObject("error","非法登录");
//	        mv.setViewName("user/error");
//		}
		return mv;
	}


	/**
	 * 退出登录
	 */
	@RequestMapping(value="logout.html",produces="application/json;charset=utf-8")
	@ResponseBody
	public ModelAndView logout(HttpServletRequest request){
		// 清除session
		Enumeration<String> em = request.getSession().getAttributeNames();
		while (em.hasMoreElements()) {
			request.getSession().removeAttribute(em.nextElement().toString());
		}
		request.getSession().removeAttribute("mui");
		request.getSession().invalidate();
		String path = request.getContextPath();
		// 拼接跳转页面路径
		//String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
		System.out.println(request.getSession().getAttribute("mui"));
		ModelAndView mv = new ModelAndView();
		mv.addObject("","");
        mv.setViewName("user/login");
        return mv;
	}

	/**
	 * 查看代理
	 *
	 */
	@RequestMapping(value="authCode.html",produces="application/json;charset=utf-8")
	@ResponseBody

	public void getAuthCode(HttpServletRequest request, HttpServletResponse response, HttpSession session)
			throws IOException {
		int width = 63;
		int height = 37;
		Random random = new Random();
		//设置response头信息
		//禁止缓存
		response.setHeader("Pragma", "No-cache");
		response.setHeader("Cache-Control", "no-cache");
		response.setDateHeader("Expires", 0);

		//生成缓冲区image类
		BufferedImage image = new BufferedImage(width, height, 1);
		//产生image类的Graphics用于绘制操作
		Graphics g = image.getGraphics();
		//Graphics类的样式
		g.setColor(this.getRandColor(200, 250));
		g.setFont(new Font("Times New Roman",0,28));
		g.fillRect(0, 0, width, height);
		//绘制干扰线
		for(int i=0;i<40;i++){
			g.setColor(this.getRandColor(130, 200));
			int x = random.nextInt(width);
			int y = random.nextInt(height);
			int x1 = random.nextInt(12);
			int y1 = random.nextInt(12);
			g.drawLine(x, y, x + x1, y + y1);
		}

		//绘制字符
		String strCode = "";
		for(int i=0;i<4;i++){
			String rand = String.valueOf(random.nextInt(10));
			strCode = strCode + rand;
			g.setColor(new Color(20+random.nextInt(110),20+random.nextInt(110),20+random.nextInt(110)));
			g.drawString(rand, 13*i+6, 28);
		}
		//将字符保存到session中用于前端的验证
		session.setAttribute("strCode", strCode);

		g.dispose();

		ImageIO.write(image, "JPEG", response.getOutputStream());
		response.getOutputStream().flush();
	}

	//创建颜色
	Color getRandColor(int fc,int bc){
		Random random = new Random();
		if(fc>255)
			fc = 255;
		if(bc>255)
			bc = 255;
		int r = fc + random.nextInt(bc - fc);
		int g = fc + random.nextInt(bc - fc);
		int b = fc + random.nextInt(bc - fc);
		return new Color(r,g,b);
	}
	/**
	 * 进行验证用户的验证码是否正确
	 * @param value 用户输入的验证码
	 * @param request HttpServletRequest对象
	 * @return 一个String类型的字符串。格式为：<br/>
	 *                     {"res",boolean},<br/>
	 *                 如果为{"res",true}，表示验证成功<br/>
	 *                 如果为{"res",false}，表示验证失败
	 */

	@RequestMapping(value="validate.html",produces="application/json;charset=utf-8")
	@ResponseBody
	public String validate(String value, HttpServletRequest request){
		String valueCode=(String)request.getSession().getAttribute("strCode");
		Map map=new HashMap();
		if(valueCode!=null){
			if(valueCode.equals(value)){
				map.put("res", true);
				return new Gson().toJson(map);
			}
		}
		map.put("res", false);
		return new Gson().toJson(map);
	}

	@RequestMapping(value="changePwd.html",produces="application/json;charset=utf-8")
	@ResponseBody
	public String changePwd(String loginname, String oldpwd, String newpwd, HttpServletRequest request) {
		LogTable logTable =  new LogTable();
		Sales mui = (Sales)request.getSession().getAttribute("mui");
		logTable.setUsername(mui.getSloginname());
		logTable.setActiontime(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
		String result="error";
		try{
			if(oldpwd.equals(mui.getSpassword())){
				String cloginname = AESUtil.AESEncode2(loginname, aesKey);
				String opassword = AESUtil.AESEncode2(oldpwd, aesKey);
				String npassword = AESUtil.AESEncode2(newpwd, aesKey);
				int i = salesService.changePwd(cloginname,opassword,npassword);
				if(i==1){
					logTable.setActionevent(mui.getSloginname()+"修改密码成功");
					logService.insert(logTable);
					result= "ok";
				}else if(i==0){
					logTable.setActionevent(mui.getSloginname() +"修改密码失败");
					logService.insert(logTable);
					result= "error";
				}
			}else{
				logTable.setActionevent(mui.getSloginname() +"旧密码错误修改密码失败");
				logService.insert(logTable);
				result= "oldpwderror";
			}
		}catch (Exception e){
			e.printStackTrace();
			logTable.setActionevent(mui.getSloginname() +"修改密码失败");
			logService.insert(logTable);
			result= "exception";
		}
		return result;
	}
}
