package com.sales.controller;

import com.sales.bean.Constant;
import com.sales.model.*;
import com.sales.service.LogService;
import com.sales.service.SalesService;
import com.sales.util.AESUtil;
import com.sales.bean.Constant;
import com.sales.model.LogTable;
import com.sales.model.Sales;
import com.sales.model.SalesInterFace;
import com.sales.service.LogService;
import com.sales.service.SalesService;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;


/**
 * 二维码
 */
@Controller
public class SalesController {

	@Autowired
	private SalesService salesService;
	@Autowired
	private LogService logService;





	/**
	 * 查看代理
	 *
	 */
	@RequestMapping(value="detailsSales.html",produces="application/json;charset=utf-8")
	@ResponseBody

	public ModelAndView detailsSales(HttpServletRequest request) {
		ModelAndView mv = new ModelAndView();

		LogTable logTable =  new LogTable();
		Sales mui = (Sales)request.getSession().getAttribute("mui");
		logTable.setUsername(mui.getSloginname());

		Sales sales = salesService.querySales(mui.getSalesno());
		List<SalesInterFace> salesInterFaceList = salesService.querysalesInterFaceList(mui.getSalesno());
		List<Merchant> merchantList = salesService.queryMerchantList(mui.getSalesno());
		mv.addObject("sales", sales);
		mv.addObject("salesInterFaceList", salesInterFaceList);
		mv.addObject("merchantList", merchantList);
		mv.addObject("map",Constant.map);
		mv.setViewName("sales/sales");
		logTable.setActiontime(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
		logTable.setActionevent("查看代理");
		logService.insert(logTable);
		return mv;
	}





	/**
	 * 商户通道跳转
	 */
	@RequestMapping(value="salesInterFaceList.html",produces="application/json;charset=utf-8")
	@ResponseBody
	public ModelAndView salesInterFaceList(int said, String salesno, HttpServletRequest request) {
		ModelAndView mv = new ModelAndView();
		List<SalesInterFace> salesInterFaceList = salesService.querysalesInterFaceList(salesno);
		mv.addObject("salesInterFaceList", salesInterFaceList);
		mv.addObject("salesno", salesno);
		mv.addObject("said",said);
		mv.addObject("map",Constant.map);
		LogTable logTable =  new LogTable();
		Sales mui = (Sales)request.getSession().getAttribute("mui");
		logTable.setUsername(mui.getSloginname());
		logTable.setActiontime(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
		logTable.setActionevent("查看"+salesno+"代理通道");
		logService.insert(logTable);
		mv.setViewName("sales/salesInterFaceList");

		return mv;
	}



	
}
