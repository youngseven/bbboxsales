package com.sales.controller;

import com.sales.bean.Constant;
import com.sales.bean.MerchantReport;

import com.sales.model.*;
import com.sales.service.LogService;
import com.sales.service.MerchantService;
import com.sales.service.RechargeOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;


@Controller
public class RechargeOrderController {

	@Autowired
	private RechargeOrderService rechargeOrderService;

	@Autowired
	private MerchantService merchantService;
	@Autowired
	private LogService logService;
	DecimalFormat df = new DecimalFormat("#.00");


	/**
	 * 商户列表
	 */
	@RequestMapping(value="merchantOrderJump.html",produces="application/json;charset=utf-8")
	@ResponseBody
	public ModelAndView merchantOrderJump(HttpServletRequest request) {
		ModelAndView mv = new ModelAndView();
		//List<TransferCard> transferCardList = transferCardService.queryTransferCard();
		LogTable logTable =  new LogTable();
		Sales mui = (Sales)request.getSession().getAttribute("mui");
		logTable.setUsername(mui.getSloginname());
		String begintime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))+" 00:00:00";
		String endtime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))+" 23:59:59";
		List<RechargeOrder> rechargeOrderlist = null;
		rechargeOrderlist = rechargeOrderService.queryRechargeOrderTwo(begintime,endtime,mui.getSalesno(),0);
		mv.addObject("rechargeOrderlist",rechargeOrderlist);
		mv.addObject("map",Constant.map);
		mv.setViewName("order/orderlist");
		logTable.setActiontime(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
		logTable.setActionevent("打开"+mui.getSalesno()+"支付订单");
		logService.insert(logTable);
        return mv;
	}
	
	/**
	 * 订单列表
	 */
	@RequestMapping(value="rechargeOrderlist.html",produces="application/json;charset=utf-8")
	@ResponseBody
	public ModelAndView rechargeOrderlist(String begintime, String endtime, String orderno,String merchantno, String sysorderno, Integer ordertype, HttpServletRequest request) {
		ModelAndView mv = new ModelAndView();;
		List<RechargeOrder> rechargeOrderlist = null;
		LogTable logTable =  new LogTable();
		Sales mui = (Sales)request.getSession().getAttribute("mui");
		logTable.setUsername(mui.getSloginname());
		int count = 0;
		int pageTimes;
		if(orderno!=""||!orderno.equals("")){
			rechargeOrderlist = rechargeOrderService.queryRechargeOrder(orderno);
		}else if(sysorderno!=""||!sysorderno.equals("")){
			rechargeOrderlist = rechargeOrderService.queryRechargeOrderSys(sysorderno);
		}else{
			if(ordertype==100){
				if(merchantno.equals("")){
					count=rechargeOrderService.queryRechargeOrderCount(begintime,endtime,mui.getSalesno());
					rechargeOrderlist = rechargeOrderService.queryRechargeOrderTwo(begintime,endtime,mui.getSalesno(),0);
				}else{
					count=rechargeOrderService.queryRechargeOrderThereCount2(begintime,endtime,merchantno,mui.getSalesno());
					rechargeOrderlist = rechargeOrderService.queryRechargeOrderThere2(begintime,endtime,merchantno,mui.getSalesno(),0);
				}
			}else{
				if(merchantno.equals("")){
					count=rechargeOrderService.queryRechargeOrderThereCount(begintime,endtime,ordertype,mui.getSalesno());
					rechargeOrderlist = rechargeOrderService.queryRechargeOrderThere(begintime,endtime,ordertype,mui.getSalesno(),0);
				}else{
					count=rechargeOrderService.queryRechargeOrderFourCount(begintime,endtime,ordertype,merchantno,mui.getSalesno());
					rechargeOrderlist = rechargeOrderService.queryRechargeOrderFour(begintime,endtime,ordertype,merchantno,mui.getSalesno(),0);
				}
			}
		}
		if(count%20 == 0)
		{
			pageTimes = count/20;
		}else {
			pageTimes = count / 20 + 1;
		}

		if(ordertype==null||ordertype.equals("")){
			ordertype=100;
		}
		mv.addObject("begintime",begintime);
		mv.addObject("endtime",endtime);
		mv.addObject("merchantno",merchantno);
		mv.addObject("ordertype",ordertype);
		mv.addObject("pageTimes",pageTimes);
		mv.addObject("pageNum","1");
		mv.addObject("count",count);
		mv.addObject("rechargeOrderlist",rechargeOrderlist);
		mv.addObject("map", Constant.map);
		mv.setViewName("order/orderlist");

		logTable.setActiontime(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
		logTable.setActionevent("查询支付订单");
		logService.insert(logTable);
        return mv;
	}

	/**
	 * 今日报表
	 */
	@RequestMapping(value="reportForm.html",produces="application/json;charset=utf-8")
	@ResponseBody
	public ModelAndView reportForm(HttpServletRequest request) {
		ModelAndView mv = new ModelAndView();
		mv.addObject("","");
        mv.setViewName("order/report");
		LogTable logTable =  new LogTable();
		Sales mui = (Sales)request.getSession().getAttribute("mui");
		logTable.setUsername(mui.getSloginname());
		logTable.setActiontime(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
		logTable.setActionevent("查看今日报表");
		logService.insert(logTable);
        return mv;
	
	}

	/**
	 * 订单列表
	 */
	@RequestMapping(value="rechargeOrderPagelist.html",produces="application/json;charset=utf-8")
	@ResponseBody
	public ModelAndView rechargeOrderlist(String begintime,String endtime,String merchantno,Integer ordertype,String cardNum,String pageNum,HttpServletRequest request) {

		ModelAndView mv = new ModelAndView();
		LogTable logTable =  new LogTable();
		Sales mui = (Sales)request.getSession().getAttribute("mui");
		logTable.setUsername(mui.getSloginname());
		List<RechargeOrder> rechargeOrderlist = null;
		int count =0;
		int pageTimes;
		if(ordertype==100){
			if(merchantno.equals("")){
				count=rechargeOrderService.queryRechargeOrderCount(begintime,endtime,mui.getSalesno());
			}else{
				count=rechargeOrderService.queryRechargeOrderThereCount2(begintime,endtime,merchantno,mui.getSalesno());
			}
		}else{
			if(merchantno.equals("")){
				count=rechargeOrderService.queryRechargeOrderThereCount(begintime,endtime,ordertype,mui.getSalesno());
			}else{
				count=rechargeOrderService.queryRechargeOrderFourCount(begintime,endtime,ordertype,merchantno,mui.getSalesno());
			}
		}
		//总页数
		if(count%20 == 0)
		{
			pageTimes = count/20;
		}else {
			pageTimes = count / 20 + 1;
		}
		//当前页
//		if(null == pageNum)
//		{
//			pageNum = "1";
//		}
		//每页开始的第几条记录
		int startRow = (Integer.parseInt(pageNum)-1) * 20;
		if(ordertype==100){
			if(merchantno.equals("")){
				rechargeOrderlist = rechargeOrderService.queryRechargeOrderTwo(begintime,endtime,mui.getSalesno(),startRow);
			}else{
				rechargeOrderlist = rechargeOrderService.queryRechargeOrderThere2(begintime,endtime,merchantno,mui.getSalesno(),startRow);
			}
		}else{
			if(merchantno.equals("")){
				rechargeOrderlist = rechargeOrderService.queryRechargeOrderThere(begintime,endtime,ordertype,mui.getSalesno(),startRow);
			}else{
				rechargeOrderlist = rechargeOrderService.queryRechargeOrderFour(begintime,endtime,ordertype,merchantno,mui.getSalesno(),startRow);
			}
		}


		//页面初始的时候page没有值

		if(ordertype==null||ordertype.equals("")){
			ordertype=100;
		}
		mv.addObject("begintime",begintime);
		mv.addObject("endtime",endtime);
		mv.addObject("merchantno",merchantno);
		mv.addObject("ordertype",ordertype);
		mv.addObject("cardNum",cardNum);
		mv.addObject("pageTimes",pageTimes);
		mv.addObject("pageNum",pageNum);
		mv.addObject("count",count);
		mv.addObject("rechargeOrderlist",rechargeOrderlist);
		mv.addObject("map",Constant.map);
		mv.setViewName("order/orderlist");


		logTable.setActiontime(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
		logTable.setActionevent("查询支付订单");
		logService.insert(logTable);
		return mv;
	}

	/**
	 * 订单实时列表
	 */
	@RequestMapping(value="qureyreport.html",produces="application/json;charset=utf-8")
	@ResponseBody
	public ModelAndView qureyreport(String begintime, String endtime, HttpServletRequest request) {
		ModelAndView mv = new ModelAndView();;
		//String datetime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		begintime = begintime+" 00:00:00";
		endtime = endtime+" 23:59:59";
		LogTable logTable =  new LogTable();
		Sales mui = (Sales)request.getSession().getAttribute("mui");
		logTable.setUsername(mui.getSloginname());
		List<MerchantReport> merchantReportList = rechargeOrderService.qureyreport(begintime,endtime,mui.getSalesno());

		mv.addObject("merchantReportList",merchantReportList);
        mv.setViewName("order/report");
		logTable.setActiontime(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
		logTable.setActionevent("查看实时报表");
		logService.insert(logTable);
        return mv;
	
	}

	@RequestMapping(value="merchantReportJump.html",produces="application/json;charset=utf-8")
	@ResponseBody
	public ModelAndView  merchantReportJump(HttpServletRequest request) {
		ModelAndView mv = new ModelAndView();
		mv.setViewName("order/merchantReport");
//		mv.setViewName("order/newOrderlist");
		LogTable logTable =  new LogTable();
		Sales mui = (Sales)request.getSession().getAttribute("mui");
		logTable.setUsername(mui.getSloginname());
		logTable.setActiontime(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
		logTable.setActionevent("打开商户统计");
		logService.insert(logTable);
		return mv;
	}



	@RequestMapping(value="merchantReport.html",produces="application/json;charset=utf-8")
	@ResponseBody
	public ModelAndView merchantReport(String begintime,String endtime,HttpServletRequest request) {
		ModelAndView mv = new ModelAndView();
		LogTable logTable =  new LogTable();
		Sales mui = (Sales)request.getSession().getAttribute("mui");
		logTable.setUsername(mui.getSloginname());
		logTable.setActiontime(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
//        begintime = begintime+" 00:00:00";
//        endtime = endtime+" 23:59:59";
		try{
			List<MerchantReport> merchantReportList = new ArrayList();
			merchantReportList=rechargeOrderService.merchantReport(begintime,endtime,mui.getSalesno());
			mv.addObject("merchantReportList",merchantReportList);
			mv.setViewName("order/merchantReport");
			logTable.setActionevent("查看商户"+begintime+"---"+endtime+"报表");
			logService.insert(logTable);
		}catch(Exception e){
			e.printStackTrace();
		}
		return mv;

	}



	@RequestMapping(value="transferCardReportJump.html",produces="application/json;charset=utf-8")
	@ResponseBody
	public ModelAndView  transferCardReportJump(HttpServletRequest request) {
		ModelAndView mv = new ModelAndView();
		mv.setViewName("order/transferCardReport");
//		mv.setViewName("order/newOrderlist");
		LogTable logTable =  new LogTable();
		Sales mui = (Sales)request.getSession().getAttribute("mui");
		logTable.setUsername(mui.getSloginname());
		logTable.setActiontime(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
		logTable.setActionevent("打开卡转卡统计");
		logService.insert(logTable);
		return mv;
	}



	@RequestMapping(value="transferCardReport.html",produces="application/json;charset=utf-8")
	@ResponseBody
	public ModelAndView transferCardReport(String begintime,String endtime,HttpServletRequest request) {
		ModelAndView mv = new ModelAndView();
		LogTable logTable =  new LogTable();
		Sales mui = (Sales)request.getSession().getAttribute("mui");
		logTable.setUsername(mui.getSloginname());
		logTable.setActiontime(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
//		begintime = begintime+" 00:00:00";
//		endtime = endtime+" 23:59:59";
		try{
			List<MerchantReport> merchantReportList = new ArrayList();
			merchantReportList=rechargeOrderService.transferCardReport(begintime,endtime,mui.getSalesno());
			mv.addObject("dateTime",begintime+"---"+endtime);
			mv.addObject("merchantReportList",merchantReportList);
			mv.setViewName("order/transferCardReport");
			logTable.setActionevent("查看宝复制"+begintime+"---"+endtime+"报表");
			logService.insert(logTable);
		}catch(Exception e){
			e.printStackTrace();
		}
		return mv;

	}




	@RequestMapping(value="zfbReportJump.html",produces="application/json;charset=utf-8")
	@ResponseBody
	public ModelAndView  zfbReportJump(HttpServletRequest request) {
		ModelAndView mv = new ModelAndView();
		mv.setViewName("order/zfbReport");
//		mv.setViewName("order/newOrderlist");
		LogTable logTable =  new LogTable();
		Sales mui = (Sales)request.getSession().getAttribute("mui");
		logTable.setUsername(mui.getSloginname());
		logTable.setActiontime(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
		logTable.setActionevent("打开宝转卡统计");
		logService.insert(logTable);
		return mv;
	}



	@RequestMapping(value="zfbReport.html",produces="application/json;charset=utf-8")
	@ResponseBody
	public ModelAndView zfbReport(String begintime,String endtime,HttpServletRequest request) {
		ModelAndView mv = new ModelAndView();
		LogTable logTable =  new LogTable();
		Sales mui = (Sales)request.getSession().getAttribute("mui");
		logTable.setUsername(mui.getSloginname());
		logTable.setActiontime(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
//		begintime = begintime+" 00:00:00";
//		endtime = endtime+" 23:59:59";
		try{
			List<MerchantReport> merchantReportList = new ArrayList();
			merchantReportList=rechargeOrderService.zfbReport(begintime,endtime,mui.getSalesno());
			mv.addObject("dateTime",begintime+"---"+endtime);
			mv.addObject("merchantReportList",merchantReportList);
			mv.setViewName("order/zfbReport");
			logTable.setActionevent("查看宝扫码"+begintime+"---"+endtime+"报表");
			logService.insert(logTable);
		}catch(Exception e){
			e.printStackTrace();
		}
		return mv;

	}

	@RequestMapping(value="wxReportJump.html",produces="application/json;charset=utf-8")
	@ResponseBody
	public ModelAndView  wxReportJump(HttpServletRequest request) {
		ModelAndView mv = new ModelAndView();
		mv.setViewName("order/wxReport");
//		mv.setViewName("order/newOrderlist");
		LogTable logTable =  new LogTable();
		Sales mui = (Sales)request.getSession().getAttribute("mui");
		logTable.setUsername(mui.getSloginname());
		logTable.setActiontime(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
		logTable.setActionevent("打开微信统计");
		logService.insert(logTable);
		return mv;
	}



	@RequestMapping(value="wxReport.html",produces="application/json;charset=utf-8")
	@ResponseBody
	public ModelAndView wxReport(String begintime,String endtime,HttpServletRequest request) {
		ModelAndView mv = new ModelAndView();
		LogTable logTable =  new LogTable();
		Sales mui = (Sales)request.getSession().getAttribute("mui");
		logTable.setUsername(mui.getSloginname());
		logTable.setActiontime(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
//		begintime = begintime+" 00:00:00";
//		endtime = endtime+" 23:59:59";
		try{
			List<MerchantReport> merchantReportList = new ArrayList();
			merchantReportList=rechargeOrderService.wxReport(begintime,endtime,mui.getSalesno());
			mv.addObject("dateTime",begintime+"---"+endtime);
			mv.addObject("merchantReportList",merchantReportList);
			mv.setViewName("order/wxReport");
			logTable.setActionevent("查看微信"+begintime+"---"+endtime+"报表");
			logService.insert(logTable);
		}catch(Exception e){
			e.printStackTrace();
		}
		return mv;

	}

	@RequestMapping(value="modeReportJump.html",produces="application/json;charset=utf-8")
	@ResponseBody
	public ModelAndView  modeReportJump(HttpServletRequest request) {
		ModelAndView mv = new ModelAndView();
		mv.setViewName("order/modeReport");
//		mv.setViewName("order/newOrderlist");
		LogTable logTable =  new LogTable();
		Sales mui = (Sales)request.getSession().getAttribute("mui");
		logTable.setUsername(mui.getSloginname());
		logTable.setActiontime(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
		logTable.setActionevent("打开通道统计");
		logService.insert(logTable);
		return mv;
	}



	@RequestMapping(value="modeReport.html",produces="application/json;charset=utf-8")
	@ResponseBody
	public ModelAndView modeReport(String begintime,String endtime,HttpServletRequest request) {
		ModelAndView mv = new ModelAndView();
		LogTable logTable =  new LogTable();
		Sales mui = (Sales)request.getSession().getAttribute("mui");
		logTable.setUsername(mui.getSloginname());
		logTable.setActiontime(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
//		begintime = begintime+" 00:00:00";
//		endtime = endtime+" 23:59:59";
		try{
			List<MerchantReport> merchantReportList = new ArrayList();
			merchantReportList=rechargeOrderService.modeReport(begintime,endtime,mui.getSalesno());
			mv.addObject("dateTime",begintime+"---"+endtime);
			mv.addObject("map",Constant.map);
			mv.addObject("merchantReportList",merchantReportList);
			mv.setViewName("order/modeReport");
			logTable.setActionevent("查看通道"+begintime+"---"+endtime+"报表");
			logService.insert(logTable);
		}catch(Exception e){
			e.printStackTrace();
		}
		return mv;

	}

}
