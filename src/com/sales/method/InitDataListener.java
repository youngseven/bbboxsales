package com.sales.method;

import com.sales.bean.Constant;
import com.sales.dao.PayChannelMapper;
import com.sales.dao.SalesMenuMapper;
import com.sales.model.PayChannel;
import com.sales.model.SalesMenu;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;




@Service("initDataListener")
public class InitDataListener implements InitializingBean {
	
	@Resource
	private SalesMenuMapper salesMenuMapper;
	@Resource
	private PayChannelMapper payChannelMapper;


	public static List<SalesMenu> menuList;
	public static List<SalesMenu> menu2List;
	public static List<PayChannel> payChannelList;
	public static SalesMenu menuindex;
//	public static int a = 0;
	@Override
	public void afterPropertiesSet() throws Exception {
		// TODO Auto-generated method stub
		getData();

	}

	
	public void getData(){
		if(menuList==null){
			menuList = salesMenuMapper.getmenuList();
		}
		if(menu2List==null){
			menu2List = salesMenuMapper.getmenu2List();
		}
		if(payChannelList==null){
			payChannelList = payChannelMapper.qureyAll();
			for(int i=0;i<payChannelList.size();i++){
				Constant.map.put(payChannelList.get(i).getPid(),payChannelList.get(i).getPaychannelname());
			}

		}
	}
}
