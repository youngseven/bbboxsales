package com.sales.method;


import net.sf.json.JSONObject;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.util.Iterator;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.UUID;

public class TransferAliPayMethod {

	public String giveip(HttpServletRequest request) {
		// TODO Auto-generated method stub
		String ip = "";
		try {
//			String token = request.getHeader("Authorization");
//             System.out.println("-----------------"+token);            
             //ip
             ip = request.getHeader("x-forwarded-for");   
             if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {   
                 ip = request.getHeader("Proxy-Client-IP");   
             }   
             if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {   
                 ip = request.getHeader("WL-Proxy-Client-IP");   
             }   
             if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {   
                 ip = request.getRemoteAddr();   
                 if(ip.equals("127.0.0.1")){     
                     //根据网卡取本机配置的IP     
                     InetAddress inet=null;     
                     try {     
                         inet = InetAddress.getLocalHost();     
                     } catch (Exception e) {     
                         e.printStackTrace();     
                     }     
                     ip= inet.getHostAddress();     
                 }  
             }   
             // 多个代理的情况，第一个IP为客户端真实IP,多个IP按照','分割  
             if(ip != null && ip.length() > 15){    
                 if(ip.indexOf(",")>0){     
                     ip = ip.substring(0,ip.indexOf(","));     
                 }     
             }       
		}catch (Exception ex) {
		   ex.printStackTrace();
		}
		return ip;
	}

	public JSONObject getboj(HttpServletRequest request) {
		// TODO Auto-generated method stub
		try{
			StringBuffer str = new StringBuffer();
			InputStream is = request.getInputStream(); 
			InputStreamReader isr = new InputStreamReader(is, "utf-8"); 
			BufferedReader br = new BufferedReader(isr); 
	        //BufferedInputStream in = new BufferedInputStream(isr);
	        int i;
	        char c;
	        while ((i=br.read())!=-1) {
	        	c=(char)i;
	        	str.append(c);
	        }
	        //Gson gson = new Gson();
	        //JSONObject obj = gson.(str.toString());
	        JSONObject obj= JSONObject.fromObject(str.toString());
	        JSONObject obj2=getSortJson(obj);
	        return obj2;
		}catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
		
	}
	
	
	/**
	 * jeon排序
	 * @param json 排序前
	 * @return json 排序后
	 */
	
	public JSONObject getSortJson(JSONObject json){
		Iterator<String> iteratorKeys = json.keys();
	    SortedMap map = new TreeMap();  
	    while (iteratorKeys.hasNext()) {  
	            String key = iteratorKeys.next().toString();  
	            String vlaue = json.optString(key);  
	            map.put(key, vlaue);  
	    }  
		JSONObject json2 = JSONObject.fromObject(map);
		return json2;
	}
	
	
	/**
	 * jeon排序
	 * @param json 排序前
	 * @return json 排序后
	 */
	
	public String removeSymbols(JSONObject json){
		try{
			
			String removesign= json.toString().replace("\":\"", "=");
			removesign= removesign.replace("\",\"", "&");
			removesign= removesign.replace("{\"", "");
			removesign= removesign.replace("\"}", "");
			return removesign;
		}catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
		
	}
	

	
	public String getOrderIdByUUId() {
        int machineId = 1;//最大支持1-9个集群机器部署
        int hashCodeV = UUID.randomUUID().toString().hashCode();
        if(hashCodeV < 0) {//有可能是负数
            hashCodeV = - hashCodeV;
        }
        // 0 代表前面补充0     
        // 4 代表长度为4     
        // d 代表参数为正数型
        return machineId + String.format("%015d", hashCodeV);
    }

}
