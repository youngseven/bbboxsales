package com.sales.method;

import javax.servlet.http.HttpServletRequest;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;

public class ApiMethod {

	public String giveip(HttpServletRequest request) {
		// TODO Auto-generated method stub
		String ip = "";
		try {
//			String token = request.getHeader("Authorization");
//             System.out.println("-----------------"+token);            
             //ip
             ip = request.getHeader("X-Real-IP");
             if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)){
                 ip = request.getHeader("X-Forwarded-For");
				}	
             if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)){
                 ip = request.getHeader("Proxy-Client-IP");
             }
             if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)){
                 ip = request.getHeader("WL-Proxy-Client-IP");
             }
             if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)){
                 ip = request.getRemoteAddr();
                 if(ip.equals("127.0.0.1")){     
                     //根据网卡取本机配置的IP     
                     InetAddress inet=null;     
                     try {     
                         inet = InetAddress.getLocalHost();     
                     } catch (Exception e) {     
                         e.printStackTrace();     
                     }     
                     ip= inet.getHostAddress();     
                 }  
             }   
             // 多个代理的情况，第一个IP为客户端真实IP,多个IP按照','分割  
             if(ip != null && ip.length() > 15){    
                 if(ip.indexOf(",")>0){     
                     ip = ip.substring(0,ip.indexOf(","));     
                 }     
             }       
		}catch (Exception ex) {
		   ex.printStackTrace();
		}
		return ip;
	}
	
	
	/**
     * 根据IP地址获取mac地址
     * @param ipAddress 127.0.0.1
     * @return
     * @throws SocketException
     * @throws UnknownHostException
     */
	public String getLocalMac(String ipAddress) {
    // TODO Auto-generated method stub
        NetworkInterface ne= null;
        StringBuffer sb = new StringBuffer("");
        try {
            ne = NetworkInterface.getByInetAddress(InetAddress.getByName(ipAddress));
            byte[]mac=ne.getHardwareAddress();
            for(int i=0; i<mac.length; i++) {
                if(i!=0) {
                    sb.append("-");
                }
                //字节转换为整数
                int temp = mac[i]&0xff;
                String str = Integer.toHexString(temp);
                System.out.println("每8位:"+str);
                if(str.length()==1) {
                    sb.append("0"+str);
                }else {
                    sb.append(str);
                }
            }
        } catch (SocketException e) {
        e.printStackTrace();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        return sb.toString().toUpperCase();
	}
}
