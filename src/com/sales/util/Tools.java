package com.sales.util;

import com.google.gson.*;
import com.sales.bean.Constant;
import net.sf.json.JSONObject;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.lang.StringUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Tools {
	
	//使用正则表达式过滤
	private static String reg = "(?:')|(?:--)|(/\\*(?:.|[\\n\\r])*?\\*/)|"  
            + "(\\b(select|update|and|or|delete|insert|trancate|char|into|substr|ascii|declare|exec|count|master|into|drop|execute)\\b)";  
  
	private static Pattern sqlPattern = Pattern.compile(reg, Pattern.CASE_INSENSITIVE);  
	
	
	public static final Gson gson = new Gson();
	private static Gson gsonDisableHtml;
	//订单传递移位数
	private static final int LENGTH = 6;
	
	/**
	 * 根据正则表达式来过滤sql关键字
	 * @param str
	 * @return
	 */
	public static boolean dbSqlIsValid(String str) {  
	    if (sqlPattern.matcher(str).find()) {  
	        return false;  
	    }  
	    return true;  
	}
	
	public static Gson getGsonDisableHtml() {
		if(null == gsonDisableHtml){
			GsonBuilder gsonBuilder = new GsonBuilder();
			gsonDisableHtml = gsonBuilder.disableHtmlEscaping().create();
		}
		return gsonDisableHtml;
	}
	
	/**
	 * gson包 json转map 修正int自动转double的bug
	 * @param jsonStr
	 * @return
	 */
	public static Map<String,String> JsonToStrMap(String jsonStr){
		Map<String,Object> rs = JsonToMap(jsonStr);
		if(rs.isEmpty())
			return null;
		Map<String,String> returnMap = new HashMap<>();
		for (String key : rs.keySet()) {
			returnMap.put(key, rs.get(key)+"");
		}
		return returnMap;
	}
	
	/**
	 * gson包 json转map 修正int自动转double的bug
	 * @param jsonStr
	 * @return
	 */
	public static Map<String, Object> JsonToMap(String jsonStr) {
	    return JsonToMap(jsonStr, null);
	}
	
	/**
	 * gson包 json转map 修正int自动转double的bug
	 * @param jsonStr
	 * @param result
	 * @return
	 */
	public static Map<String, Object> JsonToMap(String jsonStr, Map<String, Object> result) {
	    if (isEmpty(jsonStr)) {
	        return null;
	    }
	    if (result == null) {
	        result = new HashMap<String, Object>();
	    }
	    JsonParser jsonParser = new JsonParser();
	    JsonElement jsonElement = jsonParser.parse(jsonStr);
	    return JsonToMap(result, "xxx", jsonElement);
	}
	
	/**
	 * gson包 json转map 修正int自动转double的bug
	 * @param result
	 * @param key
	 * @param value
	 * @return
	 */
	private static Map<String, Object> JsonToMap(Map<String, Object> result, String key, JsonElement value) {
	    // 如果key为null 直接报错
	    if (key == null) {
	        throw new RuntimeException("key值不能为null");
	    }
	    // 如果value为null,则直接put到map中
	    if (value == null) {
	        result.put(key, value);
	    } else {
	        // 如果value为基本数据类型，则放入到map中
	        if (value.isJsonPrimitive()) {
	            result.put(key, value.getAsString());
	        } else if (value.isJsonObject()) {
	            // 如果value为JsonObject数据类型，则遍历此JSONObject，进行递归调用本方法
	            JsonObject jsonObject = value.getAsJsonObject();
	            Iterator<Entry<String, JsonElement>> iterator = jsonObject.entrySet().iterator();
	            while (iterator.hasNext()) {
	                Entry<String, JsonElement> next = iterator.next();
	                result = JsonToMap(result, next.getKey(), next.getValue());
	            }
	        } else if (value.isJsonArray()) {
	            // 如果value为JsonArray数据类型，则遍历此JsonArray，进行递归调用本方法
	            JsonArray jsonArray = value.getAsJsonArray();
	            List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
	            for (int i = 0, len = jsonArray.size(); i < len; i++) {
	                Map<String, Object> tempMap = new HashMap<String, Object>();
	                JsonObject jsonObject = jsonArray.get(i).getAsJsonObject();
	                Iterator<Entry<String, JsonElement>> iterator = jsonObject.entrySet().iterator();
	                while (iterator.hasNext()) {
	                    Entry<String, JsonElement> next = iterator.next();
	                    tempMap = JsonToMap(tempMap, next.getKey(), next.getValue());
	                }
	                list.add(tempMap);
	            }
	            result.put(key, list);
	        }
	    }
	    // 返回最终结果
	    return result;
	}
	
	
	/**
	 * 将key =value &key =value 转换成map形式
	 * 
	 * @Title: keyValue2Map
	 * @Description: TODO
	 * @param str
	 * @return Map<String,String> 返回类型
	 * @throws:
	 */
	public static Map<String, String> keyValue2Map(String str) {
		// checkcode=3000&realinfunds=200&message=成功
		Map<String, String> map = new HashMap<>();

		String keys = "";
		String values = "";
		String[] strs = str.split("&");
		for (String st : strs) {
			if (Tools.notEmpty(st)) {
				String[] keyvalue = st.split("=");
				if (keyvalue.length == 2) {
					keys = keyvalue[0];
					values = keyvalue[1];
					if (Tools.notEmpty(keys)) {
						map.put(Tools.obj2str(keys).trim(), Tools.obj2str(values).trim());
					}
				}else{
					if (Tools.notEmpty(keys)) {
						map.put(Tools.obj2str(keys).trim(), null);
					}
				}
			}

		}
		return map;

	}
	

	public static String displacementEnString(String str){
		return transpositionString(displacementEnString(str,LENGTH));
	}
	
	public static String displacementDeString(String str){
		return displacementDeString(transpositionString(str),LENGTH);
	}
	
	/**
	 * 订单号换位
	 * @param strtranspositionString
	 * @return
	 */
	public static String transpositionString(String str){
		str = Tools.obj2str(str);
		if(str.length() < 1)
			return str;
		char[] strs = str.toCharArray();
		StringBuffer buffer = new StringBuffer();
		int charLength = strs.length / 2;
		for (int i = 0; i<charLength; i++) {
			buffer.append(strs[strs.length-i-1]);
		}
		if(strs.length % 2 != 0){
			buffer.append(strs[charLength]);
		}
		for (int i = charLength - 1; i >= 0; i--) {
			buffer.append(strs[i]);
		}
		return buffer.toString();
	}
	
	/**
	 * 订单号移位加
	 * @param str
	 * @param len
	 * @return
	 */
	public static String displacementEnString(String str,int len){
		str = Tools.obj2str(str);
		if(str.length() < 1)
			return str;
		char[] strs = str.toCharArray();
		StringBuffer buffer = new StringBuffer();
		int charLength = strs.length;
		for (int i = 0; i<strs.length; i++) {
			int x = i;
			x = i + len;
			if(x > charLength - 1){
				x = x - charLength;
			}
			buffer.append(strs[x]);
		}
		return buffer.toString();
	}
	
	/**
	 * 订单号移位解
	 * @param str
	 * @param len
	 * @return
	 */
	public static String displacementDeString(String str,int len){
		str = Tools.obj2str(str);
		if(str.length() < 1)
			return str;
		char[] strs = str.toCharArray();
		StringBuffer buffer = new StringBuffer();
		int charLength = strs.length;
		for (int i = 0; i<strs.length; i++) {
			int x = i;
			x = i - len;
			if(x < 0){
				x = x + charLength;
			}
			buffer.append(strs[x]);
		}
		return buffer.toString();
	}

	/**
	 * 格式化Exception的堆栈信息,转换成String,便于记录日志
	 * 
	 * @Title: exceptionToString
	 * @Description: TODO
	 * @param e
	 * @return
	 */
	public static String exceptionToString(Exception e) {
		StringWriter sw = null;
		PrintWriter pw = null;
		try {
			sw = new StringWriter();
			pw = new PrintWriter(sw);
			// 将出错的栈信息输出到printWriter中
			e.printStackTrace(pw);
			pw.flush();
			sw.flush();
		} finally {
			if (sw != null) {
				try {
					sw.close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
			if (pw != null) {
				pw.close();
			}
		}
		return sw.toString();
	}
	
	/**
	 * 判断字符数组是否为空
	 */
	public static boolean areNotEmpty(String... values) {
		boolean result = true;
		if (values == null || values.length == 0) {
			result = false;
		} else {
			for (String value : values) {
				result &= !isEmpty(value);
			}
		}
		return result;
	}
	
	/**
	 * 格式化数字并保留两位小数
	 * @Title: numFormat
	 * @Description: TODO
	 * @param num
	 * @return
	 */
	public static String numFormat(String num){
		DecimalFormat format = new DecimalFormat("#.##");
		Double numd = Double.valueOf(num);
		return format.format(numd);
	}
	
	/**
	 * String to int
	 * */
	public static Integer string2Int(String str){
		str = Tools.obj2str(str);
		return Tools.notEmpty(str)?Integer.valueOf(str):0;
	}
	public static Integer string2Int(Object str){
		String strs = Tools.obj2str(str);
		return Tools.notEmpty(strs)?Integer.valueOf(strs):0;
	}
	
	/**
	 * 在字段前面添加内容
	 * @Title: stringJoin
	 * @Description: TODO
	 * @param str
	 * @param len
	 * @param joinStr
	 * @return
	 * String 返回类型
	 * @throws:
	 */
	public static String stringJoin(String str,int len,String joinStr){
		int strLength = Tools.obj2str(str).length();
		StringBuffer buffer= new StringBuffer();
		if(strLength<len){
			len = len -strLength ;
			for(int i=0;i<len;i++){
				buffer.append(joinStr);
			}
		}
		buffer.append(str);
		return buffer.toString();
		
	}
	
	/**
	 * 将数组转换为带连接符链接的内容
	* @Title: stringJion 
	* @Description: TODO(这里用一句话描述这个方法的作用) 
	*  @param jion 拼接符号
	*  @param strAry 处理数组
	*  @return    设定文件 
	* @throws 
	* @date 2016年6月2日上午9:30:38
	* @user pengxi
	*/
	public static String stringJion(String jion,String[] strAry){
			StringBuffer sb=new StringBuffer();
			for(int i = 0;i<strAry.length;i++){
				if(i==(strAry.length-1)){
					sb.append(strAry[i]);
				}else{
					sb.append(strAry[i]).append(jion);
				}
			}
			return sb.toString();
	}
	/**
	 * 将数组转换为带连接符链接的内容
	* @Title: stringJion 
	* @Description: TODO(这里用一句话描述这个方法的作用) 
	*  @param jion 拼接符号
	*  @param strAry 处理数组
	*  @return    设定文件 
	* @throws 
	* @date 2016年6月2日上午9:30:38
	* @user pengxi
	*/
	public static String stringJion(String jion,Object[] strAry){
		StringBuffer sb=new StringBuffer();
		for(int i = 0;i<strAry.length;i++){
			if(i==(strAry.length-1)){
				sb.append(strAry[i]);
			}else{
				sb.append(strAry[i]).append(jion);
			}
		}
		return sb.toString();
	}
	
	/**
	 * obj转字符串
	 * @param s
	 * @return
	 */
	public static String obj2str(Object s){
		if(s==null){
			return "";
		}else if(Tools.isEmpty(s.toString())){
			return "";
		}else{
			return s.toString();
		}
	}
	public static long obj2Long(Object s){
		if(s==null){
			return 0l;
		}else if(Tools.isEmpty(s.toString())){
			return 0;
		}else{
			return Long.valueOf(s.toString().trim());
		}
	}
	
	public static boolean isEmpty(Object s){
		if(s==null){
			return true;
		}else if(Tools.isEmpty(s.toString())){
			return true;
		}else{
			return false;
		}
	}
	
	/**
	 * 随机生成六位数验证码 
	 * @return
	 */
	public static int getRandomNum(){
		 Random r = new Random();
		 return r.nextInt(900000)+100000;//(Math.random()*(999999-100000)+100000)
	}
	
	
	
	/**
	 * 检测字符串是否不为空(null,"","null")
	 * @param s
	 * @return 不为空则返回true，否则返回false
	 */
	public static boolean notEmpty(String s){
		return s!=null && !"".equals(s) && !"null".equals(s);
	}
	
	/**
	 * 检测字符串是否为空(null,"","null")
	 * @param s
	 * @return 为空则返回true，不否则返回false
	 */
	public static boolean isEmpty(String s){
		return s==null || "".equals(s) || "null".equals(s);
	}
	
	/**
	 * 字符串转换为字符串数组
	 * @param str 字符串
	 * @param splitRegex 分隔符
	 * @return 不包含末尾为空的字符
	 */
	public static String[] str2StrArray(String str,String splitRegex){
		return str2StrArrayTool(str,splitRegex,0);
	}
	/**
	 * 字符串转换为字符串数组
	 * @param str
	 * @param splitRegex
	 * @return 所有的截取字段，包含末尾空的字段
	 */
	public static String[] str2StrArrayAll(String str,String splitRegex){
		return str2StrArrayTool(str,splitRegex,-1);
	}
	
	/**
	 * 字符截取工具
	 * @param str
	 * @param splitRegex
	 * @param length
	 * @return
	 */
	public static String[] str2StrArrayTool(String str,String splitRegex,int length){
		if(isEmpty(str)){
			return null;
		}
		if(length ==0 ){
			return str.split(splitRegex);
		}else{
			return str.split(splitRegex,length);
		}
	}
	
	/**
	 * 用默认的分隔符(,)将字符串转换为字符串数组
	 * @param str	字符串
	 * @return
	 */
	public static String[] str2StrArray(String str){
		return str2StrArray(str,",\\s*");
	}
	
	/**
	 * 按照yyyy-MM-dd HH:mm:ss的格式，日期转字符串
	 * @param date
	 * @return yyyy-MM-dd HH:mm:ss
	 */
	public static String date2Str(Date date){
		return date2Str(date,"yyyy-MM-dd HH:mm:ss");
	}
	
	/**
	 * 按照yyyy-MM-dd HH:mm:ss的格式，字符串转日期
	 * @param date
	 * @return
	 */
	public static Date str2Date(String date){
		if(notEmpty(date)){
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			try {
				return sdf.parse(date);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			return new Date();
		}else{
			return null;
		}
	}
	
	/**
	 * 按照参数format的格式，日期转字符串
	 * @param date
	 * @param format
	 * @return
	 */
	public static String date2Str(Date date,String format){
		if(date!=null){
			SimpleDateFormat sdf = new SimpleDateFormat(format);
			return sdf.format(date);
		}else{
			return "";
		}
	}
	
	/**
	 * 把时间根据时、分、秒转换为时间段
	 * @param StrDate
	 */
	public static String getTimes(String StrDate){
		String resultTimes = "";
		
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    Date now;

	    try {
	    	now = new Date();
	    	Date date=df.parse(StrDate);
	    	long times = now.getTime()-date.getTime();
	    	long day  =  times/(24*60*60*1000);
	    	long hour = (times/(60*60*1000)-day*24);
	    	long min  = ((times/(60*1000))-day*24*60-hour*60);
	    	long sec  = (times/1000-day*24*60*60-hour*60*60-min*60);
	        
	    	StringBuffer sb = new StringBuffer();
	    	//sb.append("发表于：");
	    	if(hour>0 ){
	    		sb.append(hour+"小时前");
	    	} else if(min>0){
	    		sb.append(min+"分钟前");
	    	} else{
	    		sb.append(sec+"秒前");
	    	}
	    		
	    	resultTimes = sb.toString();
	    } catch (ParseException e) {
	    	e.printStackTrace();
	    }
	    
	    return resultTimes;
	}
	
	
	
	/**
	  * 验证邮箱
	  * @param email
	  * @return
	  */
	 public static boolean checkEmail(String email){
	  boolean flag = false;
	  try{
	    String check = "^([a-z0-9A-Z]+[-|_|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";
	    Pattern regex = Pattern.compile(check);
	    Matcher matcher = regex.matcher(email);
	    flag = matcher.matches();
	   }catch(Exception e){
	    flag = false;
	   }
	  return flag;
	 }
	
	 /**
	  * 验证手机号码
	  * @param mobiles
	  * @return
	  */
	 public static boolean checkMobileNumber(String mobileNumber){
	  boolean flag = false;
	  try{
	    Pattern regex = Pattern.compile("^(((13[0-9])|(15([0-3]|[5-9]))|(17([0-3]|[5-9]))|(18([0-3]|5-9])))\\d{8})|(0\\d{2}-\\d{8})|(0\\d{3}-\\d{7})$");
	    Matcher matcher = regex.matcher(mobileNumber);
	    flag = matcher.matches();
	   }catch(Exception e){
	    flag = false;
	   }
	  return flag;
	 }
	 
	 
	/**
	 * 根据长度截取字符串
	 * @param str 被截取的字符串
	 * @param cutLength 保留长度
	 * @return 你好... ...
	 */
	public static String strCutOut(String str,int cutLength){
		return strCutOut(str, cutLength,"......");
	}
	/**
	 * 根据长度截取字符串
	 * @param str 被截取的字符串
	 * @param cutLength 保留长度
	 * @param cutChar 截取后末尾添加符号；默认“...”
	 * @return
	 */
	public static String strCutOut(String str,int cutLength,String cutChar){
		cutChar = Tools.isEmpty(cutChar)?"...":cutChar;
		if(Tools.isEmpty(str)){
			return str;
		}
		int strLength=str.length();
		if(strLength>cutLength){
			return str.substring(0,cutLength)+cutChar;
		}
		
		return str;
	}
	
	/** 
     * 除去数组中的空值和签名参数
     * @param sArray 签名参数组
     * @return 去掉空值与签名参数后的新签名参数组
     */
    public static Map<String, String> paraFilter(Map<String, String> sArray) {

        Map<String, String> result = new HashMap<String, String>();

        if (sArray == null || sArray.size() <= 0) {
            return result;
        }

        for (String key : sArray.keySet()) {
            String value = sArray.get(key);
            if (value == null || value.equals("") || key.equalsIgnoreCase("sign")
                || key.equalsIgnoreCase("signType")) {
                continue;
            }
            result.put(key, value);
        }
       
        return result;
    }
    
    /** 
     * 除去数组中的空值和签名参数
     * @param sArray 签名参数组
     * @return 去掉空值与签名参数后的新签名参数组
     */
    public static Map<String, Object> paraFilterO(Map<String, Object> sArray) {

        Map<String, Object> result = new HashMap<String, Object>();

        if (sArray == null || sArray.size() <= 0) {
            return result;
        }

        for (String key : sArray.keySet()) {
            Object value = sArray.get(key);
            if (value == null || value.toString().equals("") || key.toString().equalsIgnoreCase("sign")
                || key.toString().equalsIgnoreCase("signType")) {
                continue;
            }
            result.put(key, value);
        }
       
        return result;
    }
	
	public static String createUUID() {
		UUID id = UUID.randomUUID();
		return id.toString().replace("-", "");
	}//method

	public static String[] BANK_NAME_KEY_ARRAY = { "邮储银行", "工商银行", "农业银行", "中国银行", "建设银行", "中信银行", "光大银行", "华夏银行",
			"民生银行", "广发银行", "招商银行", "兴业银行", "浦发银行", "平安银行", "北京银行", "上海银行", "交通银行" };
	public static String[] BANK_CODE = { "PSBC", "ICBC", "ABC", "BOC", "CCB", "CITIC", "CEB", "HXBANK", "CMBC", "GDB",
			"CMB", "CIB", "SPDB", "SPABANK", "BJBANK", "SHBANK", "COMM" };

	public static String getBankCode(String name) {
		for (int i = 0; i < BANK_NAME_KEY_ARRAY.length; i++) {
			if (name.contains(BANK_NAME_KEY_ARRAY[i])) {
				return BANK_CODE[i];
			}
		}
		return "";
	}
	
	
	/**
	 * 验证ip是否在允许范围之内
	 * @Title: checkIp
	 * @Description: TODO
	 * @param ip
	 * @param ips
	 * @return fasle 在时间范围之内
	 * Boolean 返回类型
	 * @throws:
	 */
	public static Boolean checkIp(String ip,String ips){
		if(Tools.isEmpty(ips)){
			return true;
		}
		if(ip==null||ip.equals("")){
			return false;
		}else{
			ips= ips+",";
			return ips.contains(ip+",");
		}
	}
	/**
	 * 结算时间 验证
	 * @Title: checkSettleTime 
	 * @Description: TODO
	 * @param times  时间格式07:09-13:50,20:18-23:10
	 * @return
	 * Boolean 返回类型
	 * @throws:
	 */
	public static Boolean checkSettleTime(String times){
		if(Tools.isEmpty(times)){
			return true;
		}
		boolean flag = false;
		String nowTime = date2Str(new Date(), "HH:mm");
		String [] timeArray = times.split(",");
		String [] timecheck ;
		String start="";
		String end="";
		SimpleDateFormat sd = new SimpleDateFormat("HH:mm");
		Date date1 ;
		Date date2 ;
		Date date3;
		try {
			date3 = sd.parse(nowTime);
			if(timeArray.length==0){
				return true;
				
			}
			for(String time:timeArray){
				if(isEmpty(time)){
					continue;
				}
				timecheck = time.split("-");
				start =  timecheck[0];
				if(isEmpty(start)){
					continue;
				}
				end =  timecheck[1];
				if(isEmpty(end)){
					continue;
				}
				date1 = sd.parse(start.trim());
				date2 = sd.parse(end.trim());
//				System.out.println("-date1:"+date1.getTime());
//				System.out.println("-date2:"+date2.getTime());
//				System.out.println("-date3:"+date3.getTime());
				if(date1.getTime()<=date3.getTime()&&date2.getTime()>=date3.getTime()){
					return true;
				}else{
					flag = false;
				}
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return flag;
	}
	
	/**
	 * 时间添加小时
	 * @param strDate
	 * @param hours
	 * @return
	 */
	public static String addDate(String strDate, int hours) {
		String newDate = "";
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			Date date = sdf.parse(strDate);
			Calendar canlendar = Calendar.getInstance();
			canlendar.setTime(date);
			canlendar.add(Calendar.HOUR, hours);
			date = canlendar.getTime();
			newDate = sdf.format(date);
		} catch (Exception e) {
			newDate = "0000-00-00";
		}
		return newDate;

	}
	/**
	 * 时间转换
	 * @param date
	 * @param hours
	 * @return
	 */
	public static Date addDate(Date date, int hours) {
		try {
			Calendar canlendar = Calendar.getInstance();
			canlendar.setTime(date);
			canlendar.add(Calendar.HOUR, hours);
			date = canlendar.getTime();
			return date;
		} catch (Exception e) {
			return null;
		}
	}
	/**
	 * 计算时间差
	 * @param start 开始时间
	 * @param end 结束时间
	 * @return 结束时间-开始时间
	 */
	public static long jisuanshijian(String start,String end){
		Date startTime = Tools.str2Date(start);
		Date endTime = Tools.str2Date(end);
		return endTime.getTime()-startTime.getTime();
	}
	
	/**
	 * jeon排序
	 * @param json 排序前
	 * @return json 排序后
	 */
	
	public JSONObject getSortJson(JSONObject json){
		Iterator<String> iteratorKeys = json.keys();
	    SortedMap map = new TreeMap();  
	    while (iteratorKeys.hasNext()) {  
	            String key = iteratorKeys.next().toString();  
	            String vlaue = json.optString(key);  
	            map.put(key, vlaue);  
	    }  
		JSONObject json2 = JSONObject.fromObject(map);
		return json2;
	}
	
	
	public static void main(String[] args) throws UnsupportedEncodingException, IOException {
		//long i =  new Date().getTime()-addDate(new Date(),2).getTime();
		
		System.out.println(displacementEnString("泰富恒通",4));
	}

	public static String debase(String variable) {
		// TODO Auto-generated method stub
		String result = AESUtil.AESDncode2(variable,Constant.aesKey);
		return result;
	}
	
	
	 public String weChatUrl(String polymerizationUrl){
		  String html = new String();
		  HttpGet httpget = new HttpGet("http://vip.wxticket.com/");
		  // 模拟浏览器，避免被服务器拒绝，返回返回403 forbidden的错误信息
		  httpget.setHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.84 Safari/537.36");
		  CloseableHttpResponse webResponse = null;
		  CloseableHttpResponse pzResponse = null;
		  CloseableHttpClient httpclient = HttpClients.createDefault();   // 使用默认的HttpClient
		  try {
			  webResponse = httpclient.execute(httpget);
			  if (webResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {     // 返回 200 表示成功
				  HttpGet httpget2 = new HttpGet(StringUtils.substringBefore(StringUtils.substringAfterLast(EntityUtils.toString(webResponse.getEntity(), "utf-8"), "如：<a href=\""), "http://www.baidu.com/")+polymerizationUrl);//创建Http请求实例，URL 如：https://cd.lianjia.com/
				  // 模拟浏览器，避免被服务器拒绝，返回返回403 forbidden的错误信息
				  httpget2.setHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.84 Safari/537.36");
				  
				  CloseableHttpClient httpclient2 = HttpClients.createDefault();
				  pzResponse = httpclient2.execute(httpget2);
				  if (pzResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
					  html = StringUtils.substringBefore("weixin"+ StringUtils.substringAfterLast(EntityUtils.toString(pzResponse.getEntity(), "utf-8"), "weixin"), "\"");
					  System.out.println(html);
				  }
			  }
		  } catch (IOException e) {
			  e.printStackTrace();
		  } finally {
	        if (webResponse != null) {
	            try {
	            	webResponse.close();
	            	if (webResponse != null) {
	                    try {
	                        pzResponse.close();
	                    } catch (IOException e) {
	                        e.printStackTrace();
	                    }
	                }  
	            } catch (IOException e) {
	                e.printStackTrace();
	            }
	        }
	    }
		return html;
	  }
}
