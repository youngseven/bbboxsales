package com.sales.util;


/*
 --------------------------------------------**********--------------------------------------------

 该算法于1977年由美国麻省理工学院MIT(Massachusetts Institute of Technology)的Ronal Rivest，Adi Shamir和Len Adleman三位年轻教授提出，并以三人的姓氏Rivest，Shamir和Adlernan命名为RSA算法，是一个支持变长密钥的公共密钥算法，需要加密的文件块的长度也是可变的!

 所谓RSA加密算法，是世界上第一个非对称加密算法，也是数论的第一个实际应用。它的算法如下：

 1.找两个非常大的质数p和q（通常p和q都有155十进制位或都有512十进制位）并计算n=pq，k=(p-1)(q-1)。

 2.将明文编码成整数M，保证M不小于0但是小于n。

 3.任取一个整数e，保证e和k互质，而且e不小于0但是小于k。加密钥匙（称作公钥）是(e, n)。

 4.找到一个整数d，使得ed除以k的余数是1（只要e和n满足上面条件，d肯定存在）。解密钥匙（称作密钥）是(d, n)。

 加密过程： 加密后的编码C等于M的e次方除以n所得的余数。

 解密过程： 解密后的编码N等于C的d次方除以n所得的余数。

 只要e、d和n满足上面给定的条件。M等于N。

 --------------------------------------------**********--------------------------------------------
 */

import org.apache.commons.lang3.ArrayUtils;

import javax.crypto.Cipher;
import java.math.BigInteger;
import java.security.*;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.HashMap;
import java.util.Map;

public class RSA {
	/** 指定key的大小 */
	private static int KEYSIZE = 1024;
	private static final String CHAR_ENCODING = "UTF-8";
	private static final String AES_ALGORITHM = "AES/ECB/PKCS5Padding";
	private static final String RSA_ALGORITHM = "RSA/ECB/PKCS1Padding";
	/**
	 * 生成密钥对
	 */
	public static Map<String, String> generateKeyPair() throws Exception {
		/** RSA算法要求有一个可信任的随机数源 */
		SecureRandom sr = new SecureRandom();
		/** 为RSA算法创建一个KeyPairGenerator对象 */
		KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA");
		/** 利用上面的随机数据源初始化这个KeyPairGenerator对象 */
		kpg.initialize(KEYSIZE, sr);
		/** 生成密匙对 */
		KeyPair kp = kpg.generateKeyPair();
		/** 得到公钥 */
		Key publicKey = kp.getPublic();
		byte[] publicKeyBytes = publicKey.getEncoded();
		String pub = new String(Base64.encodeBase64(publicKeyBytes),
				CHAR_ENCODING);
		/** 得到私钥 */
		Key privateKey = kp.getPrivate();
		byte[] privateKeyBytes = privateKey.getEncoded();
		String pri = new String(Base64.encodeBase64(privateKeyBytes),
				CHAR_ENCODING);

		Map<String, String> map = new HashMap<String, String>();
		map.put("publicKey", pub);
		map.put("privateKey", pri);
		RSAPublicKey rsp = (RSAPublicKey) kp.getPublic();
		BigInteger bint = rsp.getModulus();
		byte[] b = bint.toByteArray();
		byte[] deBase64Value = Base64.encodeBase64(b);
		String retValue = new String(deBase64Value);
		map.put("modulus", retValue);
		return map;
	}

	/**
	 * 加密方法 source： 源数据
	 */
	public static String encrypt(String source, String publicKey)
			throws Exception {
		Key key = getPublicKey(publicKey);
		StringBuilder sb = new StringBuilder();
		
		/** 得到Cipher对象来实现对源数据的RSA加密 */
		Cipher cipher = Cipher.getInstance(RSA_ALGORITHM);
		cipher.init(Cipher.ENCRYPT_MODE, key);
		byte[] b = source.getBytes();
		
		for (int i = 0; i < b.length; i += 64) {
			/** 执行加密操作 */
			byte[] doFinal = cipher.doFinal(ArrayUtils.subarray(b, i, i + 64));
			sb.append(new String(Base64.encodeBase64(doFinal), CHAR_ENCODING));
		}

		return sb.toString();
	}
	public static String encrypt1(String source, PublicKey publicKey)
			throws Exception {
		StringBuilder sb = new StringBuilder();
		
		/** 得到Cipher对象来实现对源数据的RSA加密 */
		Cipher cipher = Cipher.getInstance(RSA_ALGORITHM);
		cipher.init(Cipher.ENCRYPT_MODE, publicKey);
		byte[] b = source.getBytes();
		
		for (int i = 0; i < b.length; i += 64) {
			/** 执行加密操作 */
			byte[] doFinal = cipher.doFinal(ArrayUtils.subarray(b, i, i + 64));
			sb.append(new String(Base64.encodeBase64(doFinal), CHAR_ENCODING));
		}
		
		return sb.toString();
	}

	/**
	 * 解密算法 cryptograph:密文
	 */
	public static String decrypt(String cryptograph, String privateKey)
			throws Exception {
		Key key = getPrivateKey(privateKey);
		/** 得到Cipher对象对已用公钥加密的数据进行RSA解密 */
		Cipher cipher = Cipher.getInstance(RSA_ALGORITHM);
		cipher.init(Cipher.DECRYPT_MODE, key);
		byte[] b1 = Base64.decodeBase64(cryptograph.getBytes());
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < b1.length; i += 128) {
			/** 执行解密操作 */
			byte[] doFinal = cipher.doFinal(ArrayUtils.subarray(b1, i, i + 128));
			sb.append(new String(doFinal));
		}
		return sb.toString();
	}

	/**
	 * 得到公钥
	 * 
	 * @param key
	 *            密钥字符串（经过base64编码）
	 * @throws Exception
	 */
	public static PublicKey getPublicKey(String key) throws Exception {
		X509EncodedKeySpec keySpec = new X509EncodedKeySpec(
				Base64.decodeBase64(key.getBytes()));
		KeyFactory keyFactory = KeyFactory.getInstance("RSA");
		PublicKey publicKey = keyFactory.generatePublic(keySpec);
		return publicKey;
	}

	/**
	 * 得到私钥
	 * 
	 * @param key
	 *            密钥字符串（经过base64编码）
	 * @throws Exception
	 */
	public static PrivateKey getPrivateKey(String key) throws Exception {
		PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(
				Base64.decodeBase64(key.getBytes()));
		KeyFactory keyFactory = KeyFactory.getInstance("RSA");
		PrivateKey privateKey = keyFactory.generatePrivate(keySpec);
		return privateKey;
	}

	public static String sign(String content, String privateKey) {
		String charset = CHAR_ENCODING;
		try {
			PKCS8EncodedKeySpec priPKCS8 = new PKCS8EncodedKeySpec(
					Base64.decodeBase64(privateKey.getBytes()));
			KeyFactory keyf = KeyFactory.getInstance("RSA");
			PrivateKey priKey = keyf.generatePrivate(priPKCS8);

			Signature signature = Signature.getInstance("SHA1WithRSA");

			signature.initSign(priKey);
			signature.update(content.getBytes(charset));

			byte[] signed = signature.sign();

			return new String(Base64.encodeBase64(signed));
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}
	
	public static boolean checkSign(String content, String sign, String publicKey)
	{
		try 
		{
			KeyFactory keyFactory = KeyFactory.getInstance("RSA");
	        byte[] encodedKey = Base64.decode2(publicKey);
	        PublicKey pubKey = keyFactory.generatePublic(new X509EncodedKeySpec(encodedKey));

		
			Signature
			signature = Signature
			.getInstance("SHA1WithRSA");
		
			signature.initVerify(pubKey);
			signature.update(content.getBytes("utf-8") );
		
			boolean bverify = signature.verify( Base64.decode2(sign) );
			return bverify;
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return false;
	}
	
	public static void main(String[] args) {
		//String publicKey ="MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCow1OlQfI2iyD7oIYf74VWTE0KeAWMq3dYMKOLUr0qvqPmH4ftwRBizkZUty5rmAk1FIBzavbAb/mm7YeVMSWzKK0Q51ZNLmBe/Ci8rDXO4VxbCFXXYcoz2RgfTyGPuTOuXXKQ4TNyZjC/wO12jOL0qf7tE04uTiz5vUfMqw8b6wIDAQAB";
		//String privateKey = "MIICeAIBADANBgkqhkiG9w0BAQEFAASCAmIwggJeAgEAAoGBAKjDU6VB8jaLIPughh/vhVZMTQp4BYyrd1gwo4tSvSq+o+Yfh+3BEGLORlS3LmuYCTUUgHNq9sBv+abth5UxJbMorRDnVk0uYF78KLysNc7hXFsIVddhyjPZGB9PIY+5M65dcpDhM3JmML/A7XaM4vSp/u0TTi5OLPm9R8yrDxvrAgMBAAECgYEAibUg+vdt0o4CsvDQoRyMe9e1AbbgKGH992tw+A3iqA95pBzUiPACE+3nu9bhvRAJa1O4YPXGNU7pswmdmeLJtCz4ug8CD450KdMgtyTu4KSK3H9wq7A77EEVbAORApCDYk2SNSkDrTIbRcXV9yJSgMYF3tiENoWs44TWHEFKrhECQQDaeowU34neR5MHHTIv7x74yNkw7CRNI79YAyT9zGDf0/S//HMcZLPQgzc/VQptNkGitl4dF7PmK60z0lXr1MVzAkEAxb8FLNzvqvA0nIsVHe1N6hnS1nu/Db1PqaKTOjrleYMsqLQv6mbJZR65ktNQPz4VT1rhG0xYcACamgE9+YtxqQJBAMNxa0zk6A2Lk9CCYroUvG0sJ3cnWBYVy38tWPMd2O+CSelRKZyVzXHTYCHsrmYiajbn02tNH+bg3ZGFMM3GSlcCQB3Bvv+fR5mLh/8+lrDoMt9Vf3qzKG7i3AgE5O7QShPc/21roAI/8XJ8CyTjE6JQr1g/rrwy4uQH0zv1j+PrbfECQQCBJHFT7PX3ExV4h+gbQRd3lPfOIeb/DfETe2xabVmnSV31q6weyhgC2+m2b/KHk/1GZ6Giu7feOEBgB3niwB84";
		String bankCardNo = "bankCardNo";
//		String userName = requestParams.get("userName");
//		String cvv2 = requestParams.get("cvv2");
//		String validPeriod = requestParams.get("validPeriod");
//		String idCode = requestParams.get("idCode");
//		String phone = requestParams.get("phone");
		
		try {
//			System.out.println("bankCardNo:"+bankCardNo);
//			bankCardNo =   RSA.encrypt(bankCardNo, publicKey);
//			System.out.println("bankCardNo:"+bankCardNo);
//			System.out.println("size:"+bankCardNo.length());
//			bankCardNo =   RSA.decrypt(bankCardNo, privateKey);
//			System.out.println("bankCardNo:"+bankCardNo);
//			bankCardNo ="彭玺12312I(3w23)*^%";
//			System.out.println("bankCardNo:"+bankCardNo);
//			bankCardNo =   RSA.encrypt(bankCardNo, publicKey);
//			System.out.println("bankCardNo:"+bankCardNo);
//			System.out.println("size:"+bankCardNo.length());
//			bankCardNo =   RSA.decrypt(bankCardNo, privateKey);
//			System.out.println("bankCardNo:"+bankCardNo);
			
			//String sing = RSA.sign("12312312", privateKey);
			//boolean f = RSA.checkSign("12312312", sing, publicKey);
			//System.out.println(f);
			
			Map map = generateKeyPair();
			System.out.println(map.get("privateKey"));
			System.out.println(map.get("publicKey"));
			System.out.println(encrypt("泰富恒通","MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCqV01qIIch5j8p40dnp2OYmdMEEoTO09Y8Fam5jGDZu3VaYWr20jJ0tN+J1HSSyFMEqBR8Pr1tN25gCK5sdPngfqoc0D71GENEIzAMQkTKedxN9dyKR3TyLRs0NcPZNV3MRZ0n9+A6EVOk5RuXEDbHHQvVItZe4f3U9s7O//IRSwIDAQAB"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}