package com.sales.util;

import java.sql.*;

public class DBConn {
    static{
        try{
        	Class.forName("com.mysql.jdbc.Driver");
        }catch(Exception ex){
            ex.printStackTrace();
        }

    }

    public static Connection getConn(){
        try{
        	Connection conn=DriverManager.getConnection("jdbc:mysql://localhost/alipayqrcode?useUnicode=true&characterEncoding=utf-8","root","4e2ad73b0c6");
        	//Connection conn=DriverManager.getConnection("jdbc:mysql://172.28.9.70/pmo?useUnicode=true&characterEncoding=utf-8","root","root");
            return conn;
        }catch(Exception ex){
            ex.printStackTrace();
            return null;
        }
    }
    public static void close(Connection conn,Statement st,ResultSet rs){
    	if(rs!=null){
            try{
                rs.close();
            }catch(SQLException ex){
            }
       }
       if(st!=null){
           try {
               st.close();
           }catch(Exception ex){
           }
       }
       if(conn!=null){
           try{
               conn.close();
           }catch(Exception ex){
           }
       }
    }

}
