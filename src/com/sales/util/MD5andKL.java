package com.sales.util;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;

public class MD5andKL {
	public static String md5(String inStr, String charset) {
		MessageDigest md5 = null;
		try {
			md5 = MessageDigest.getInstance("MD5");
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}

		byte[] md5Bytes;
		StringBuffer hexValue = new StringBuffer();
		try {
			md5Bytes = md5.digest(inStr.getBytes(charset));
			for (int i = 0; i < md5Bytes.length; i++) {
				int val = ((int) md5Bytes[i]) & 0xff;
				if (val < 16)
					hexValue.append("0");
				hexValue.append(Integer.toHexString(val));
			}
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return hexValue.toString();
	}

	public static String md5UpperCase(String inStr, String charset) {
		return md5(inStr, charset).toUpperCase();
	}

	// 可�?�的加密算法
	public static String kl(String inStr) {
		// String s = new String(inStr);
		char[] a = inStr.toCharArray();
		for (int i = 0; i < a.length; i++) {
			a[i] = (char) (a[i] ^ 't');
		}
		String s = new String(a);
		return s;
	}

	// 加密后解�?
	public static String jm(String inStr) {
		char[] a = inStr.toCharArray();
		for (int i = 0; i < a.length; i++) {
			a[i] = (char) (a[i] ^ 't');
		}
		String k = new String(a);
		return k;
	}

}
