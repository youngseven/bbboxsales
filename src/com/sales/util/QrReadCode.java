package com.sales.util;

import com.google.zxing.*;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.HybridBinarizer;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;

public class QrReadCode {
	public String qrurl(String num,String route){
		String url = null;
		try {
			MultiFormatReader reader=new MultiFormatReader();
			//File f=new File(msg);
			File f=new File("E:/tools/apache-tomcat-8.0.29/webapps/aliPayServer/upload/imgs/"+num+".jpg");
			//File f=new File(route+num+".jpg");
			BufferedImage image=ImageIO.read(f);
			BinaryBitmap bb=new BinaryBitmap(new HybridBinarizer(new BufferedImageLuminanceSource(image)));
			HashMap map =new HashMap();
			map.put(EncodeHintType.CHARACTER_SET, "utf-8");
			Result result = reader.decode(bb,map);
			System.out.println("返回内容"+result.toString());
			System.out.println("编码格式"+result.getBarcodeFormat());
			System.out.println("显示内容"+result.getText());
			url=result.toString();
			return url;
		} catch (NotFoundException e) {
			e.printStackTrace();
			return "a";
		} catch (IOException e) {
			e.printStackTrace();
			return "b";
		}
		
	}
	public static void main(String[] args) {
		try {
			MultiFormatReader reader=new MultiFormatReader();
			int num=0;
			File f=new File("E:/"+num+".jpg");
			BufferedImage image=ImageIO.read(f);
			BinaryBitmap bb=new BinaryBitmap(new HybridBinarizer(new BufferedImageLuminanceSource(image)));
			HashMap map =new HashMap();
			map.put(EncodeHintType.CHARACTER_SET, "utf-8");
			Result result = reader.decode(bb,map);
			System.out.println("返回内容"+result.toString());
			System.out.println("编码格式"+result.getBarcodeFormat());
			System.out.println("显示内容"+result.getText());
		} catch (NotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
