package com.sales.model;

public class RechargeOrder {
    private Integer oid;

    private String orderno;

    private String sysorderno;

    private String merchantno;

    private String salesno;

    private String curcode;

    private String ordertime;

    private String sysordertime;

    private String confirmtime;

    private String jumptime;

    private String waittime;

    private String endtime;

    private String supplementtime;

    private String productname;

    private Integer paychanneltype;

    private Integer orderamount;

    private String version;

    private String bgurl;

    private Integer qrid;

    private Integer raid;

    private String displayname;

    private String cardnum;

    private Integer saleamount;

    private Double srate;

    private Double rate;

    private Integer receivableamount;

    private Integer shandlingfee;

    private Integer handlingfee;

    private Integer income;

    private Integer actualamount;

    private String paymenturl;

    private String remarks;

    private String aliuid;

    private Integer ordertype;

    private String dealmsg;

    private Integer sort;

    private Integer callback;

    public Integer getOid() {
        return oid;
    }

    public void setOid(Integer oid) {
        this.oid = oid;
    }

    public String getOrderno() {
        return orderno;
    }

    public void setOrderno(String orderno) {
        this.orderno = orderno == null ? null : orderno.trim();
    }

    public String getSysorderno() {
        return sysorderno;
    }

    public void setSysorderno(String sysorderno) {
        this.sysorderno = sysorderno == null ? null : sysorderno.trim();
    }

    public String getMerchantno() {
        return merchantno;
    }

    public void setMerchantno(String merchantno) {
        this.merchantno = merchantno == null ? null : merchantno.trim();
    }

    public String getSalesno() {
        return salesno;
    }

    public void setSalesno(String salesno) {
        this.salesno = salesno == null ? null : salesno.trim();
    }

    public String getCurcode() {
        return curcode;
    }

    public void setCurcode(String curcode) {
        this.curcode = curcode == null ? null : curcode.trim();
    }

    public String getOrdertime() {
        return ordertime;
    }

    public void setOrdertime(String ordertime) {
        this.ordertime = ordertime == null ? null : ordertime.trim();
    }

    public String getSysordertime() {
        return sysordertime;
    }

    public void setSysordertime(String sysordertime) {
        this.sysordertime = sysordertime == null ? null : sysordertime.trim();
    }

    public String getConfirmtime() {
        return confirmtime;
    }

    public void setConfirmtime(String confirmtime) {
        this.confirmtime = confirmtime == null ? null : confirmtime.trim();
    }

    public String getJumptime() {
        return jumptime;
    }

    public void setJumptime(String jumptime) {
        this.jumptime = jumptime == null ? null : jumptime.trim();
    }

    public String getWaittime() {
        return waittime;
    }

    public void setWaittime(String waittime) {
        this.waittime = waittime == null ? null : waittime.trim();
    }

    public String getEndtime() {
        return endtime;
    }

    public void setEndtime(String endtime) {
        this.endtime = endtime == null ? null : endtime.trim();
    }

    public String getSupplementtime() {
        return supplementtime;
    }

    public void setSupplementtime(String supplementtime) {
        this.supplementtime = supplementtime == null ? null : supplementtime.trim();
    }

    public String getProductname() {
        return productname;
    }

    public void setProductname(String productname) {
        this.productname = productname == null ? null : productname.trim();
    }

    public Integer getPaychanneltype() {
        return paychanneltype;
    }

    public void setPaychanneltype(Integer paychanneltype) {
        this.paychanneltype = paychanneltype;
    }

    public Integer getOrderamount() {
        return orderamount;
    }

    public void setOrderamount(Integer orderamount) {
        this.orderamount = orderamount;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version == null ? null : version.trim();
    }

    public String getBgurl() {
        return bgurl;
    }

    public void setBgurl(String bgurl) {
        this.bgurl = bgurl == null ? null : bgurl.trim();
    }

    public Integer getQrid() {
        return qrid;
    }

    public void setQrid(Integer qrid) {
        this.qrid = qrid;
    }

    public Integer getRaid() {
        return raid;
    }

    public void setRaid(Integer raid) {
        this.raid = raid;
    }

    public String getDisplayname() {
        return displayname;
    }

    public void setDisplayname(String displayname) {
        this.displayname = displayname == null ? null : displayname.trim();
    }

    public String getCardnum() {
        return cardnum;
    }

    public void setCardnum(String cardnum) {
        this.cardnum = cardnum == null ? null : cardnum.trim();
    }

    public Integer getSaleamount() {
        return saleamount;
    }

    public void setSaleamount(Integer saleamount) {
        this.saleamount = saleamount;
    }

    public Double getSrate() {
        return srate;
    }

    public void setSrate(Double srate) {
        this.srate = srate;
    }

    public Double getRate() {
        return rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }

    public Integer getReceivableamount() {
        return receivableamount;
    }

    public void setReceivableamount(Integer receivableamount) {
        this.receivableamount = receivableamount;
    }

    public Integer getShandlingfee() {
        return shandlingfee;
    }

    public void setShandlingfee(Integer shandlingfee) {
        this.shandlingfee = shandlingfee;
    }

    public Integer getHandlingfee() {
        return handlingfee;
    }

    public void setHandlingfee(Integer handlingfee) {
        this.handlingfee = handlingfee;
    }

    public Integer getIncome() {
        return income;
    }

    public void setIncome(Integer income) {
        this.income = income;
    }

    public Integer getActualamount() {
        return actualamount;
    }

    public void setActualamount(Integer actualamount) {
        this.actualamount = actualamount;
    }

    public String getPaymenturl() {
        return paymenturl;
    }

    public void setPaymenturl(String paymenturl) {
        this.paymenturl = paymenturl == null ? null : paymenturl.trim();
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks == null ? null : remarks.trim();
    }

    public String getAliuid() {
        return aliuid;
    }

    public void setAliuid(String aliuid) {
        this.aliuid = aliuid == null ? null : aliuid.trim();
    }

    public Integer getOrdertype() {
        return ordertype;
    }

    public void setOrdertype(Integer ordertype) {
        this.ordertype = ordertype;
    }

    public String getDealmsg() {
        return dealmsg;
    }

    public void setDealmsg(String dealmsg) {
        this.dealmsg = dealmsg == null ? null : dealmsg.trim();
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Integer getCallback() {
        return callback;
    }

    public void setCallback(Integer callback) {
        this.callback = callback;
    }
}