package com.sales.model;

public class LogTable {
    private Integer logid;

    private String username;

    private String actiontime;

    private String actionevent;

    public Integer getLogid() {
        return logid;
    }

    public void setLogid(Integer logid) {
        this.logid = logid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username == null ? null : username.trim();
    }

    public String getActiontime() {
        return actiontime;
    }

    public void setActiontime(String actiontime) {
        this.actiontime = actiontime == null ? null : actiontime.trim();
    }

    public String getActionevent() {
        return actionevent;
    }

    public void setActionevent(String actionevent) {
        this.actionevent = actionevent == null ? null : actionevent.trim();
    }
}