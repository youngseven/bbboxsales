package com.sales.model;

public class Merchant {
    private Integer mid;

    private Integer said;

    private String salesno;

    private String merchantno;

    private String merchantnname;

    private String industry;

    private String contacts;

    private Integer state;

    private String privatekey;

    private String publickey;

    private String token;

    private Integer merchanttype;

    private String regtime;

    private Integer balance;

    private Integer avbalance;

    private Integer lockbalance;

    private Integer unbalance;

    private Integer mquota;

    private Integer deposit;

    public Integer getMid() {
        return mid;
    }

    public void setMid(Integer mid) {
        this.mid = mid;
    }

    public Integer getSaid() {
        return said;
    }

    public void setSaid(Integer said) {
        this.said = said;
    }

    public String getSalesno() {
        return salesno;
    }

    public void setSalesno(String salesno) {
        this.salesno = salesno == null ? null : salesno.trim();
    }

    public String getMerchantno() {
        return merchantno;
    }

    public void setMerchantno(String merchantno) {
        this.merchantno = merchantno == null ? null : merchantno.trim();
    }

    public String getMerchantnname() {
        return merchantnname;
    }

    public void setMerchantnname(String merchantnname) {
        this.merchantnname = merchantnname == null ? null : merchantnname.trim();
    }

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry == null ? null : industry.trim();
    }

    public String getContacts() {
        return contacts;
    }

    public void setContacts(String contacts) {
        this.contacts = contacts == null ? null : contacts.trim();
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getPrivatekey() {
        return privatekey;
    }

    public void setPrivatekey(String privatekey) {
        this.privatekey = privatekey == null ? null : privatekey.trim();
    }

    public String getPublickey() {
        return publickey;
    }

    public void setPublickey(String publickey) {
        this.publickey = publickey == null ? null : publickey.trim();
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token == null ? null : token.trim();
    }

    public Integer getMerchanttype() {
        return merchanttype;
    }

    public void setMerchanttype(Integer merchanttype) {
        this.merchanttype = merchanttype;
    }

    public String getRegtime() {
        return regtime;
    }

    public void setRegtime(String regtime) {
        this.regtime = regtime == null ? null : regtime.trim();
    }

    public Integer getBalance() {
        return balance;
    }

    public void setBalance(Integer balance) {
        this.balance = balance;
    }

    public Integer getAvbalance() {
        return avbalance;
    }

    public void setAvbalance(Integer avbalance) {
        this.avbalance = avbalance;
    }

    public Integer getLockbalance() {
        return lockbalance;
    }

    public void setLockbalance(Integer lockbalance) {
        this.lockbalance = lockbalance;
    }

    public Integer getUnbalance() {
        return unbalance;
    }

    public void setUnbalance(Integer unbalance) {
        this.unbalance = unbalance;
    }

    public Integer getMquota() {
        return mquota;
    }

    public void setMquota(Integer mquota) {
        this.mquota = mquota;
    }

    public Integer getDeposit() {
        return deposit;
    }

    public void setDeposit(Integer deposit) {
        this.deposit = deposit;
    }
}