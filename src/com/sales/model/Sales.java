package com.sales.model;

public class Sales {
    private Integer said;

    private String salesno;

    private String salesname;

    private String sloginname;

    private String spassword;

    private String regtime;

    private Integer balance;

    private Integer avbalance;

    private Integer lockbalance;

    private Integer unbalance;

    private Integer mquota;

    private Integer deposit;

    public Integer getSaid() {
        return said;
    }

    public void setSaid(Integer said) {
        this.said = said;
    }

    public String getSalesno() {
        return salesno;
    }

    public void setSalesno(String salesno) {
        this.salesno = salesno == null ? null : salesno.trim();
    }

    public String getSalesname() {
        return salesname;
    }

    public void setSalesname(String salesname) {
        this.salesname = salesname == null ? null : salesname.trim();
    }

    public String getSloginname() {
        return sloginname;
    }

    public void setSloginname(String sloginname) {
        this.sloginname = sloginname == null ? null : sloginname.trim();
    }

    public String getSpassword() {
        return spassword;
    }

    public void setSpassword(String spassword) {
        this.spassword = spassword == null ? null : spassword.trim();
    }

    public String getRegtime() {
        return regtime;
    }

    public void setRegtime(String regtime) {
        this.regtime = regtime == null ? null : regtime.trim();
    }

    public Integer getBalance() {
        return balance;
    }

    public void setBalance(Integer balance) {
        this.balance = balance;
    }

    public Integer getAvbalance() {
        return avbalance;
    }

    public void setAvbalance(Integer avbalance) {
        this.avbalance = avbalance;
    }

    public Integer getLockbalance() {
        return lockbalance;
    }

    public void setLockbalance(Integer lockbalance) {
        this.lockbalance = lockbalance;
    }

    public Integer getUnbalance() {
        return unbalance;
    }

    public void setUnbalance(Integer unbalance) {
        this.unbalance = unbalance;
    }

    public Integer getMquota() {
        return mquota;
    }

    public void setMquota(Integer mquota) {
        this.mquota = mquota;
    }

    public Integer getDeposit() {
        return deposit;
    }

    public void setDeposit(Integer deposit) {
        this.deposit = deposit;
    }
}