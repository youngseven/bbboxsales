package com.sales.model;

public class InterFace {
    private Integer sid;

    private Integer said;

    private String salesno;

    private Integer mid;

    private String merchantno;

    private Integer pid;

    private Double rate;

    private Double srate;

    private Integer state;

    public Integer getSid() {
        return sid;
    }

    public void setSid(Integer sid) {
        this.sid = sid;
    }

    public Integer getSaid() {
        return said;
    }

    public void setSaid(Integer said) {
        this.said = said;
    }

    public String getSalesno() {
        return salesno;
    }

    public void setSalesno(String salesno) {
        this.salesno = salesno == null ? null : salesno.trim();
    }

    public Integer getMid() {
        return mid;
    }

    public void setMid(Integer mid) {
        this.mid = mid;
    }

    public String getMerchantno() {
        return merchantno;
    }

    public void setMerchantno(String merchantno) {
        this.merchantno = merchantno == null ? null : merchantno.trim();
    }

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public Double getRate() {
        return rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }

    public Double getSrate() {
        return srate;
    }

    public void setSrate(Double srate) {
        this.srate = srate;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }
}