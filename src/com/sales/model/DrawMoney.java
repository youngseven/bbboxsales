package com.sales.model;

public class DrawMoney {
    private Integer dmid;

    private String drawmoneyno;

    private String salesno;

    private String salesname;

    private String turninbank;

    private String turnincardnumber;

    private String turninname;

    private Integer applymoney;

    private Integer actualmoney;

    private Double rate;

    private Integer servicecharge;

    private String turnoutbank;

    private String turnoutbcardnumber;

    private String turnoutname;

    private String applytime;

    private String examinetime;

    private String endtime;

    private String applynname;

    private String examinename;

    private String endname;

    private Integer state;

    private Integer actualamount;

    public Integer getDmid() {
        return dmid;
    }

    public void setDmid(Integer dmid) {
        this.dmid = dmid;
    }

    public String getDrawmoneyno() {
        return drawmoneyno;
    }

    public void setDrawmoneyno(String drawmoneyno) {
        this.drawmoneyno = drawmoneyno == null ? null : drawmoneyno.trim();
    }

    public String getSalesno() {
        return salesno;
    }

    public void setSalesno(String salesno) {
        this.salesno = salesno == null ? null : salesno.trim();
    }

    public String getSalesname() {
        return salesname;
    }

    public void setSalesname(String salesname) {
        this.salesname = salesname == null ? null : salesname.trim();
    }

    public String getTurninbank() {
        return turninbank;
    }

    public void setTurninbank(String turninbank) {
        this.turninbank = turninbank == null ? null : turninbank.trim();
    }

    public String getTurnincardnumber() {
        return turnincardnumber;
    }

    public void setTurnincardnumber(String turnincardnumber) {
        this.turnincardnumber = turnincardnumber == null ? null : turnincardnumber.trim();
    }

    public String getTurninname() {
        return turninname;
    }

    public void setTurninname(String turninname) {
        this.turninname = turninname == null ? null : turninname.trim();
    }

    public Integer getApplymoney() {
        return applymoney;
    }

    public void setApplymoney(Integer applymoney) {
        this.applymoney = applymoney;
    }

    public Integer getActualmoney() {
        return actualmoney;
    }

    public void setActualmoney(Integer actualmoney) {
        this.actualmoney = actualmoney;
    }

    public Double getRate() {
        return rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }

    public Integer getServicecharge() {
        return servicecharge;
    }

    public void setServicecharge(Integer servicecharge) {
        this.servicecharge = servicecharge;
    }

    public String getTurnoutbank() {
        return turnoutbank;
    }

    public void setTurnoutbank(String turnoutbank) {
        this.turnoutbank = turnoutbank == null ? null : turnoutbank.trim();
    }

    public String getTurnoutbcardnumber() {
        return turnoutbcardnumber;
    }

    public void setTurnoutbcardnumber(String turnoutbcardnumber) {
        this.turnoutbcardnumber = turnoutbcardnumber == null ? null : turnoutbcardnumber.trim();
    }

    public String getTurnoutname() {
        return turnoutname;
    }

    public void setTurnoutname(String turnoutname) {
        this.turnoutname = turnoutname == null ? null : turnoutname.trim();
    }

    public String getApplytime() {
        return applytime;
    }

    public void setApplytime(String applytime) {
        this.applytime = applytime == null ? null : applytime.trim();
    }

    public String getExaminetime() {
        return examinetime;
    }

    public void setExaminetime(String examinetime) {
        this.examinetime = examinetime == null ? null : examinetime.trim();
    }

    public String getEndtime() {
        return endtime;
    }

    public void setEndtime(String endtime) {
        this.endtime = endtime == null ? null : endtime.trim();
    }

    public String getApplynname() {
        return applynname;
    }

    public void setApplynname(String applynname) {
        this.applynname = applynname == null ? null : applynname.trim();
    }

    public String getExaminename() {
        return examinename;
    }

    public void setExaminename(String examinename) {
        this.examinename = examinename == null ? null : examinename.trim();
    }

    public String getEndname() {
        return endname;
    }

    public void setEndname(String endname) {
        this.endname = endname == null ? null : endname.trim();
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getActualamount() {
        return actualamount;
    }

    public void setActualamount(Integer actualamount) {
        this.actualamount = actualamount;
    }
}