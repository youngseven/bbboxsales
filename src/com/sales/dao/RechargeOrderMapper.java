package com.sales.dao;

import com.sales.bean.MerchantReport;
import com.sales.model.RechargeOrder;

import java.util.List;

public interface RechargeOrderMapper {
    int deleteByPrimaryKey(Integer oid);

    int insert(RechargeOrder record);

    int insertSelective(RechargeOrder record);

    RechargeOrder selectByPrimaryKey(Integer oid);

    int updateByPrimaryKeySelective(RechargeOrder record);

    int updateByPrimaryKey(RechargeOrder record);




    MerchantReport queryRechargeOrderReport(String begintime, String endtime, String salesno);

    List<RechargeOrder> queryRechargeOrder(String orderno);

    List<RechargeOrder> queryRechargeOrderSys(String sysorderno);

    List<RechargeOrder> queryRechargeOrderFour(String begintime, String endtime, Integer ordertype,String merchantno, String salesno, int startRow);

    List<RechargeOrder> queryRechargeOrderThere2(String begintime, String endtime,String merchantno, String salesno,  int startRow);


    List<MerchantReport> qureyreport(String begintime, String endtime, String salesno);

    int queryRechargeOrderCount(String begintime, String endtime, String salesno);

    List<RechargeOrder> queryRechargeOrderTwo(String begintime, String endtime, String salesno, int startRow);

    int queryRechargeOrderThereCount2(String begintime, String endtime, String merchantno);

    int queryRechargeOrderThereCount(String begintime, String endtime, Integer ordertype, String salesno);

    int queryRechargeOrderFourCount(String begintime, String endtime, Integer ordertype, String merchantno);

    List<RechargeOrder> queryRechargeOrderThere(String begintime, String endtime, String salesno, int ordertype, int startRow);

    List<MerchantReport> merchantReport(String begintime, String endtime, String salesno);

    List<MerchantReport> transferCardReport(String begintime, String endtime, String salesno);

    List<MerchantReport> zfbReport(String begintime, String endtime, String salesno);

    List<MerchantReport> wxReport(String begintime, String endtime, String salesno);

    List<MerchantReport> modeReport(String begintime, String endtime, String salesno);
}