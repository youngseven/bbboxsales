package com.sales.dao;

import com.sales.model.InterFace;

import java.util.List;

public interface InterFaceMapper {
    int deleteByPrimaryKey(Integer sid);

    int insert(InterFace record);

    int insertSelective(InterFace record);

    InterFace selectByPrimaryKey(Integer sid);

    int updateByPrimaryKeySelective(InterFace record);

    int updateByPrimaryKey(InterFace record);




    List<InterFace> queryInterFace(String merchantno);
}