package com.sales.dao;

import com.sales.model.Merchant;

import java.util.List;

public interface MerchantMapper {
    int deleteByPrimaryKey(Integer mid);

    int insert(Merchant record);

    int insertSelective(Merchant record);

    Merchant selectByPrimaryKey(Integer mid);

    int updateByPrimaryKeySelective(Merchant record);

    int updateByPrimaryKey(Merchant record);






    Merchant getMax();

    Merchant detailsmerchant(String merchantno);

    List<Merchant> queryMerchantList(String salesno);
}