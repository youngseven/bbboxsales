package com.sales.dao;

import com.sales.model.Sales;
import com.sales.model.SalesInterFace;

import java.util.List;

public interface SalesInterFaceMapper {
    int deleteByPrimaryKey(Integer sid);

    int insert(SalesInterFace record);

    int insertSelective(SalesInterFace record);

    SalesInterFace selectByPrimaryKey(Integer sid);

    int updateByPrimaryKeySelective(SalesInterFace record);

    int updateByPrimaryKey(SalesInterFace record);

    List<SalesInterFace> querySalesInterFaceList(String salesno);


}