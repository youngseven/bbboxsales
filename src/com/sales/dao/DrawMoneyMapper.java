package com.sales.dao;

import com.sales.model.DrawMoney;

import java.util.List;

public interface DrawMoneyMapper {
    int deleteByPrimaryKey(Integer dmid);

    int insert(DrawMoney record);

    int insertSelective(DrawMoney record);

    DrawMoney selectByPrimaryKey(Integer dmid);

    int updateByPrimaryKeySelective(DrawMoney record);

    int updateByPrimaryKey(DrawMoney record);




    List<DrawMoney> queryRecordListstateTime(int state, String begintime, String endtime, String salesno);

    List<DrawMoney> queryRecordListDrawmoneyno(String drawMoneyno, String salesno);


    List<DrawMoney> queryRecordListTime(String begintime, String endtime, String salesno);


    DrawMoney queryDrawMoney(int dmid);

}