package com.sales.dao;

import com.sales.model.Sales;

public interface SalesMapper {
    int deleteByPrimaryKey(Integer said);

    int insert(Sales record);

    int insertSelective(Sales record);

    Sales selectByPrimaryKey(Integer said);

    int updateByPrimaryKeySelective(Sales record);

    int updateByPrimaryKey(Sales record);



    Sales login(String cloginname, String cpassword);

    int changePwd(String loginname, String oldpwd, String newpwd);

    Sales querySales(String salesno);

    int modifybalance(String salesno, Integer applymoney);
}