package com.sales.dao;

import com.sales.model.LogTable;

public interface LogTableMapper {
    int deleteByPrimaryKey(Integer logid);

    int insert(LogTable record);

    int insertSelective(LogTable record);

    LogTable selectByPrimaryKey(Integer logid);

    int updateByPrimaryKeySelective(LogTable record);

    int updateByPrimaryKey(LogTable record);
}