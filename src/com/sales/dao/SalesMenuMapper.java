package com.sales.dao;

import com.sales.model.SalesMenu;

import java.util.List;

public interface SalesMenuMapper {
    int deleteByPrimaryKey(Integer suid);

    int insert(SalesMenu record);

    int insertSelective(SalesMenu record);

    SalesMenu selectByPrimaryKey(Integer suid);

    int updateByPrimaryKeySelective(SalesMenu record);

    int updateByPrimaryKey(SalesMenu record);

    List<SalesMenu> getmenu2List();

    List<SalesMenu> getmenuList();
}