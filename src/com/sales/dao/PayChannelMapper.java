package com.sales.dao;

import com.sales.model.PayChannel;

import java.util.List;

public interface PayChannelMapper {
    int deleteByPrimaryKey(Integer pid);

    int insert(PayChannel record);

    int insertSelective(PayChannel record);

    PayChannel selectByPrimaryKey(Integer pid);

    int updateByPrimaryKeySelective(PayChannel record);

    int updateByPrimaryKey(PayChannel record);

	List<PayChannel> qureyAll();
}